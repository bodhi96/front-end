export class BetList {
  Rate: number;
  Stake: number;
  IsBack: boolean;
  Runner: string;
  Number: number;
}
export class CRGameCard {
  SpadeCard: [string, string, string, string, string, string];
  ClubCard: [string, string, string, string, string, string];
  DiamondCard: [string, string, string, string, string, string];
  HeartCard: [string, string, string, string, string, string];
}
export class CRGameRate {
  Spade: [string, string];
  Club: [string, string];
  Diamond: [string, string];
  Heart: [string, string];
  Red: [string, string];
  Black: [string, string];
  Odd: [string, string];
  Even: [string, string];
}
export class CRGamePL {
  SpadePL: [number, number];
  ClubPL: [number, number];
  DiamondPL: [number, number];
  HeartPL: [number, number];
  RedPL: [number, number];
  BlackPL: [number, number];
  OddPL: [number, number];
  EvenPL: [number, number];
}
export class GameList {
  BetCount: number;
  Description: string;
  GameCode: string;
  GameID: number | string;
  ImagePath: string;
  IsIframe: boolean;
  IsLive: boolean;
  Name: string;
  Rules: string;
}