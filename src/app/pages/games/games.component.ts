import { Component, OnInit } from '@angular/core';
import { ConstantService } from '../../services/constant.service';
import { GamesService } from '../../services/games.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { CasinoService } from '../../services/casino.service';
declare var $: any;
export enum fiterTypes { All, Live, Virtual, PowerGames };

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {

  IsVerified = false;
  IsCallMade = false;
  ShowGameList = false;

  Token = '';
  Code = '';
  UserName = 'User';
  GameBalance = 0;
  GameLiability = 0;
  GameList: any = [];
  SelectedGame: any = {};
  vacuumeGameList: any[] = [];
  DisplayMessage: any = '';
  selectedfiterType = fiterTypes.All;
  fiterType = fiterTypes;
  tmpGameList: any = [];
  private balanceInterval: any;
  staticImage: any[] = [
    { image: 'assets/images/VB/virtual-1.png' },
    { image: 'assets/images/VB/virtual-2.png' },
    { image: 'assets/images/VB/virtual-3.png' },
    { image: 'assets/images/VB/virtual-4.png' },
    { image: 'assets/images/VB/virtual-5.png' },
    { image: 'assets/images/VB/virtual-6.png' },
    { image: 'assets/images/VB/virtual-7.png' },
    { image: 'assets/images/VB/virtual-8.png' },
    { image: 'assets/images/VB/virtual-9.png' },
    { image: 'assets/images/VB/virtual-10.png' },
  ];

  constructor(
    public constant: ConstantService,
    private gamesService: GamesService,
    private casinoService: CasinoService,
    private toastr: ToastrService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    const LoginLink = localStorage.getItem('LoginLink');
    localStorage.clear();
    localStorage.setItem('LoginLink', LoginLink);
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    const Code = url.searchParams.get('Code') || '';
    if (Token !== '') {
      this.Token = Token;
      localStorage.setItem('AuthToken', this.Token);
      if (Code) { this.Code = Code; }
      if (this.Code) {
        this.SelectedGame = this.constant.GameDetails.find(x => x.GameCode.trim() === this.Code);
        //this.NavigatoToGame();
      } else {
        this.GetVacuumeGameList();
        this.GetUserWiseGames(0);
        //    this.balanceInterval = setInterval(() => { this.GetBalance(); }, 30 * 1000);
      }
    } else {
      this.IsVerified = false;
      this.DisplayMessage = 'Invalid Token. Please Login Again.';
    }
  }

  private async GetUserWiseGames(GameID) {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gamesService.GetUserWiseGames({ GameID: GameID }).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
            this.IsVerified = true;
            this.UserName = data.UserName;
            this.GameBalance = data.Balance;
            this.GameLiability = (Number(data.Liability) < 0) ? Number(data.Liability) * -1 : data.Liability;
            this.GameList = data.UserGames;
            if (!this.GameList) {
              this.GameList = [];
            }

            if (this.vacuumeGameList && this.vacuumeGameList.length > 0) {
              this.GameList.push(
                ...this.vacuumeGameList.map((x, i) => ({
                  GameID: -(i + 1),
                  GameCode: x.game_code,
                  Name: x.name,
                  IsLive: x.IsLive,
                  ImagePath: x.thumbnail,
                  IsIframe: false,
                  IsStop: x.IsStop
                }))
              );
            }
            this.gameFilterList(fiterTypes.All);
            this.constant.GameList = this.GameList;
            if (data.LoginLink) {
              localStorage.setItem('LoginLink', data.LoginLink);
            }
            if (this.Code) {
              this.SelectedGame = this.constant.GameList.find(x => x.GameCode.trim() === this.Code);
              //    this.NavigatoToGame();
            } else {
              this.ShowGameList = true;
            }
          } else {
            this.IsVerified = false;
            this.DisplayMessage = 'Your token is expired. Please login again.';
          }
        } else {
          this.IsVerified = false;
          if (data.Status.returnMessage) {
            this.DisplayMessage = data.Status.returnMessage;
          } else {
            this.DisplayMessage = 'Invalid credentials. Please login again.';
          }
        }
      }, err => {
        this.IsCallMade = false;
        this.IsVerified = false;
        this.DisplayMessage = 'Something went wrong. Please try again.';
      });
    }
  }

  private async GetVacuumeGameList() {
    await this.gamesService.vacuumGameList().subscribe(data => {
      if (data.Status.code === this.constant.HttpResponceCode.Success) {
        this.vacuumeGameList = JSON.parse(data.vacuumGameList).games;
      }
    }, err => {
      this.DisplayMessage = 'Something went wrong. Please try again.';
    });
  }

  private getVacuumGameList() {
    const games = [
      {
        BetCount: 0, Description: 'Fruit Slots', GameCode: 'FSL', GameID: -1,
        ImagePath: 'http://dev.vacuumplay.com/images/games/slots/0/banner.jpg', IsIframe: false,
        IsLive: true, Name: 'Fruit Slots', Rules: ''
      },
      {
        BetCount: 0, Description: 'Animal Slots', GameCode: 'ASL', GameID: -2,
        ImagePath: 'http://dev.vacuumplay.com/images/games/slots/1/banner.jpg', IsIframe: false,
        IsLive: true, Name: 'Animal Slots', Rules: ''
      },
      {
        BetCount: 0, Description: 'Egypt Slots', GameCode: 'ESL', GameID: -3,
        ImagePath: 'http://dev.vacuumplay.com/storage/games/slots/2/banner-1600668381.jpeg', IsIframe: false,
        IsLive: true, Name: 'Egypt Slots', Rules: ''
      }
    ];
    return games;
  }

  //#region GetBalance
  private async GetBalance() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GetBalance().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.Balance != undefined && data.Balance != null) {
            this.GameBalance = Number(data.Balance);
          } else {
            this.toastr.error('Your token is expired. Please login.');
            this.router.navigateByUrl('/lobby?Token=' + this.Token);
            return;
          }
        } else {
          this.toastr.error(data.Status.returnMessage);
          clearInterval(this.balanceInterval);
        }
      }, err => {
        this.IsCallMade = false;
        this.IsVerified = false;
        this.DisplayMessage = 'Something went wrong. Please try again.';
        clearInterval(this.balanceInterval);
      });
    }
  }
  //#endregion

  async OpenGameRule(item) {
    this.SelectedGame = item;
    if (item.GameID < 0) {
      this.vacuumLogin(item.GameCode);
    } else if (this.SelectedGame.IsLive) {
      if (item.BetCount > 0) {
        //   this.NavigatoToGame();
      } else {
        if (this.SelectedGame.Rules) {
          $('#RulesPopup').modal('show');
        } else {
          await this.gamesService.GetUserWiseGames({ GameID: item.GameID }).subscribe(data => {
            this.IsCallMade = false;
            if (data.Status.code === this.constant.HttpResponceCode.Success) {
              if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
                this.SelectedGame = data.UserGames[0];
                const gameIndex = this.constant.GameList.findIndex((e) => e.GameID === this.SelectedGame.GameID);
                let newGameList = [...this.constant.GameList];
                newGameList[gameIndex] = { ...newGameList[gameIndex], Rules: this.SelectedGame.Rules };
                this.GameList = newGameList;
                this.constant.GameList = newGameList;
                this.gameFilterList(fiterTypes.All);
                $('#RulesPopup').modal('show');
              }
            }
          });
        }
      }
    } else {
      this.router.navigate(['/coming-soon']);
    }
  }

  async NavigatoToGame(item?) {

    if (item.GameID && item.GameCode) {
      localStorage.setItem(item.GameCode.trim(), item.GameID);
      this.location.go(location.pathname.replace('lobby', item.GameCode.trim()) + location.search);
      if (!this.IsCallMade) {
        this.IsCallMade = true;
        this.router.navigateByUrl('/' + item.GameCode.trim() + '?Token=' + this.Token);
        // await this.gamesService.GetRefreshToken().subscribe(data => {
        //   this.IsCallMade = false;
        //   if (data.Status.code === this.constant.HttpResponceCode.Success) {
        //   } else {
        //     this.IsVerified = false;
        //     if (data.Status.returnMessage) {
        //       this.DisplayMessage = data.Status.returnMessage;
        //     } else {
        //       this.DisplayMessage = 'Invalid credentials. Please login again.';
        //     }
        //   }
        // }, err => {
        //   this.IsVerified = false;
        //   this.IsCallMade = false;
        // });
      }
    }

    this.constant.beepAudio.load();
    this.constant.noBetAudio.load();
    this.constant.betPlaceAudio.load();
  }


  OpenMarketHistory() {
    window.open(location.origin + '/history?Token=' + this.Token, '_blank');
  }

  OpenRoundHistory() {
    window.open(location.origin + '/round?Token=' + this.Token, '_blank');
  }

  async Logout() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gamesService.LogOut().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          this.IsVerified = false;
          this.GameList = [];
          localStorage.removeItem('AuthToken');
          localStorage.removeItem('Token');
          localStorage.removeItem('GameID');
          this.DisplayMessage = 'Log out successfully';
        } else {
          this.toastr.error(data.Status.returnMessage);
          clearInterval(this.balanceInterval);
        }
      }, err => {
        this.IsCallMade = false;
        clearInterval(this.balanceInterval);
      });
    }
  }

  getImage(i) {
    const index = i % 8;
    return this.staticImage[index].image;
  }

  vacuumLogin(gameCode) {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      const obj = { gameCode }
      this.gamesService.vacuumLogin(obj).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.RedirectUrl) {
            window.open(data.RedirectUrl, '_Self');
          }
        } else {
          this.toastr.error(data.Status.returnMessage);
          clearInterval(this.balanceInterval);
        }
      }, err => { this.IsCallMade = false; });
    }
  }

  gameFilterList(type: fiterTypes) {
    this.selectedfiterType = type;
    this.tmpGameList = [];
    if (this.selectedfiterType === this.fiterType.All) {
      this.tmpGameList = this.GameList;
    }
    else if (this.selectedfiterType === this.fiterType.Live) {
      this.GameList.forEach(element => {
        if (element.GameID > 0 && !element.Name.toLowerCase().includes('virtual')) {
          this.tmpGameList.push(element);
        }
      });
    }
    else if (this.selectedfiterType === this.fiterType.Virtual) {
      this.GameList.forEach(element => {
        if (element.GameID > 0 && element.Name.toLowerCase().includes('virtual')) {
          this.tmpGameList.push(element);
        }
      });
    }
    else if (this.selectedfiterType === this.fiterType.PowerGames) {
      this.GameList.forEach(element => {
        if (element.GameID < 0) {
          this.tmpGameList.push(element);
        }
      });
    }
    else {
      this.tmpGameList = this.GameList;
    }
  }
  get getLeftScroll() {
    const position = $('#scroll-menu').scrollLeft();
    if (position === 0) {
      return true;
    } else {
      return false;
    }
  }
  left() {
    $('#scroll-menu').stop().animate({ scrollLeft: '-=100' }, 800);
    return false;
  }

  right() {
    $('#scroll-menu').stop().animate({ scrollLeft: '+=100' }, 800);
    return false;
  }

}
