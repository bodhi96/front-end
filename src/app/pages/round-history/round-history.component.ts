import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ConstantService } from '../../services/constant.service';
import { CasinoService } from '../../services/casino.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GamesService } from '../../services/games.service';
import { PagerService } from '../../services/pager.service';
declare var $: any;

@Component({
  selector: 'app-round-history',
  templateUrl: './round-history.component.html',
  styleUrls: ['./round-history.component.scss']
})
export class RoundHistoryComponent implements OnInit {

  IsVerified = false;
  IsCallMade = false;

  Token = '';
  UserName = 'User';
  GameBalance = 0;
  GameList: any = [];
  SelectedGame: any = 0;
  RoundSummaryID = 0;
  StartDate: any = '2019-08-05';
  EndDate: any = '2019-08-05';
  MaxDate: any = '2019-08-05';
  Runner: any = {};
  DisplayMessage: any = '';
  videoExt: string;

  RoundHistoryList: any = [];
  RoundHistoryDetails: any = { DisplayID: '', Winner: '', TableCard: [''], PlayerACard: [''], PlayerBCard: [''], PlayerTCard: [''], Player1Card: ['', ''], Player2Card: ['', ''], Player3Card: ['', ''], Player4Card: ['', ''], Player5Card: ['', ''], Player6Card: ['', ''], SpadeCard: ['', '', '', '', '', ''], ClubCard: ['', '', '', '', '', ''], DiamondCard: ['', '', '', '', '', ''], HeartCard: ['', '', '', '', '', ''], PlayerAScore: 0, PlayerBScore: 0, BetList: [], Card: {}, winHand: { Dragon: '', DragonSuite: '', TigerSuite: '', Tiger: '' } };
  abWinnerHand = { Hand: '', JokerSuite: '', WinnerSuite: '' };
  VARWWinningHand = { Hand: '', Suite: '', Seven: '' };
  ARWWinningHand = { Hand: '', Suite: '', Seven: '' };
  JKRWinnerHand = { Number: '', Color: '', CardSuit: '' };
  CardRaceWinninghand = { Hand: '', Color: '' };
  @ViewChild('videoPlayer') videoplayer: ElementRef;

  // For custome pagination start
  pageIndex: any = 0;
  perPageData: any = 10;
  pager: any = {};
  iniflag: any = 1;
  rowCount: any;
  pagedItems: any[];
  SearchText: any = '';
  shortBy = 0;
  // For custome pagination end
  SummaryID: string;
  videoPath: string;
  currentDate: Date = new Date();

  private balanceInterval: any;

  selectedGameCode = '';

  constructor(
    private router: Router,
    public constant: ConstantService,
    private casinoService: CasinoService,
    private gamesService: GamesService,
    private pagerService: PagerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    if (Token !== '') {
      this.Token = Token;
      localStorage.setItem('AuthToken', this.Token);
      const today = new Date().toISOString().substring(0, 10);
      this.StartDate = today;
      this.EndDate = today;
      this.MaxDate = today;
      const gameId = localStorage.getItem('SelectedGame');
      this.SelectedGame = (gameId) ? gameId : 0;
      localStorage.removeItem('SelectedGame');
      this.GetUserWiseGames();
      this.SearchRoundHistory();
      // this.balanceInterval = setInterval(() => { this.GetBalance(); }, 30 * 1000);

    } else {
      this.IsVerified = false;
      this.DisplayMessage = 'Invalid Token. Please Login Again.';
    }
    $('.casinocard').on('load', function () {
      setTimeout(() => {
        $(this.parentElement.parentElement).addClass('flip');
      }, 10);
    });
  }

  private async GetUserWiseGames() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gamesService.GetUserWiseGames().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === 0) {
          if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
            this.IsVerified = true;
            this.UserName = data.UserName;
            this.GameBalance = data.Balance;
            let liveGames = [];
            data.UserGames.forEach(element => {
              if (element.Name.search(/Virtual/i) < 0) {
                liveGames.push(element);
              }
            });
            this.GameList = liveGames;
            if (this.SelectedGame !== 0 && this.SelectedGame !== '0') {
              this.SearchRoundHistory();
            }
          } else {
            this.IsVerified = false;
            this.DisplayMessage = 'Your token is expired. Please login again.';
          }
        } else {
          this.IsVerified = false;
          if (data.Status.returnMessage) {
            this.DisplayMessage = data.Status.returnMessage;
          } else {
            this.DisplayMessage = 'Invalid credentials. Please login again.';
          }
        }
      }, err => {
        this.IsCallMade = false;
        this.IsVerified = false;
        this.DisplayMessage = 'Something went wrong. Please try again.';
      });
    }
  }

  private async GetBalance() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GetBalance().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === 0) {
          if (data.Balance !== undefined && data.Balance != null) {
            this.GameBalance = Number(data.Balance);
          } else {
            this.toastr.error('Your token is expired. Please login.');
            this.router.navigateByUrl('/lobby?Token=' + this.Token);
            return;
          }
        } else {
          this.toastr.error(data.Status.returnMessage);
          clearInterval(this.balanceInterval);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  SearchRoundHistory() {
    // if (this.SelectedGame === 0 || this.SelectedGame === '0') {
    //   this.toastr.error('Please select game');
    //   return;
    // }
    if (this.StartDate === '') {
      this.toastr.error('Please select from date');
      return;
    }
    if (this.EndDate === '') {
      this.toastr.error('Please select to date');
      return;
    }
    if (this.RoundSummaryID < 0 || this.RoundSummaryID === null) {
      this.RoundSummaryID = 0;
    }
    const maxTime = new Date(this.MaxDate).getTime();
    const startTime = new Date(this.StartDate).getTime();
    const endTime = new Date(this.EndDate).getTime();
    if (startTime > maxTime) {
      this.toastr.error('Please select valid from date');
      return;
    } else if (endTime > maxTime) {
      this.toastr.error('Please select valid to date');
      return;
    } else if (startTime > endTime) {
      this.toastr.error('Please select valid from and to date');
      return;
    }
    this.GetRoundHistory(1);
  }

  async GetRoundHistory(item: any) {
    if (item === 1) {
      this.pageIndex = 0;
      this.iniflag = 1;
    }
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GetRoundHistory({
        GameId: this.SelectedGame,
        RoundSummaryID: this.RoundSummaryID,
        StartDate: this.StartDate,
        EndDate: this.EndDate,
        PageIndex: this.pageIndex,
        PageSize: this.perPageData,
      }).subscribe(data => {

        this.IsCallMade = false;
        if (data.Status.code === 0) {
          if (data.RoundHistory) {
            this.RoundHistoryList = data.RoundHistory;
            this.rowCount = data.RowCount;
          }
          if (this.iniflag === 1) {
            this.setPage(1);
            this.iniflag = 0;
          }
        } else {
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  setPage(page: number) {
    if (page < 1) {
      return;
    }
    // get pager object from service
    this.pager = this.pagerService.getPager(this.rowCount, page, this.perPageData);
    if (this.pageIndex === page - 1) {
      return;
    }
    this.pageIndex = page - 1;
    if (this.iniflag === 0) {
      this.GetRoundHistory(0);
    }
  }

  async GetRoundHistoryDetails(item) {
    this.selectedGameCode = item.GameCode;
    this.Runner = this.constant.GameRunner[item.GameCode];
    $('.mhistorycard').removeClass('flip');
    this.RoundHistoryDetails = { DisplayID: '', Winner: '', TableCard: [''], PlayerACard: [''], PlayerBCard: [''], PlayerTCard: [''], Player1Card: ['', ''], Player2Card: ['', ''], Player3Card: ['', ''], Player4Card: ['', ''], Player5Card: ['', ''], Player6Card: ['', ''], SpadeCard: ['', '', '', '', '', ''], ClubCard: ['', '', '', '', '', ''], DiamondCard: ['', '', '', '', '', ''], HeartCard: ['', '', '', '', '', ''], PlayerAScore: 0, PlayerBScore: 0, BetList: [], Card: {}, winHand: { Dragon: '', DragonSuite: '', TigerSuite: '', Tiger: '' } };
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GetMarketHistoryDetails({ RoundSummaryID: item.RoundSummaryID }).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === 0) {
          if (data.RoundData && data.BetList) {
            this.RoundHistoryDetails.BetList = data.BetList;
            this.RoundHistoryDetails.DisplayID = item.Description;
            this.RoundHistoryDetails.Winner = data.RoundData.Winner.trim();
            this.RoundHistoryDetails.WinningHand = data.RoundData.WinningHand ? data.RoundData.WinningHand.trim() : '';
            const cards = JSON.parse(data.RoundData.Cards);
            if (data.RoundData.GameCode.trim() === 'TP' || data.RoundData.GameCode.trim() === 'TP20' || data.RoundData.GameCode.trim() === 'VTP' || data.RoundData.GameCode.trim() === 'VTP20') {
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
              this.RoundHistoryDetails.PlayerBCard = cards.PlayerB.trim().split(' ');
            } else if (data.RoundData.GameCode.trim() === 'BAC' || data.RoundData.GameCode.trim() === 'BAC20' || data.RoundData.GameCode.trim() === 'VBAC' || data.RoundData.GameCode.trim() === 'VBAC20') {
              this.RoundHistoryDetails.PlayerACard = cards.Player.trim().split(' ');
              this.RoundHistoryDetails.PlayerBCard = cards.Banker.trim().split(' ');
              this.RoundHistoryDetails.PlayerAScore = (cards.PlayerScore) ? Number(cards.PlayerScore) : 0;
              this.RoundHistoryDetails.PlayerBScore = (cards.BankerScore) ? Number(cards.BankerScore) : 0;
            } else if (data.RoundData.GameCode.trim() === 'DT' || data.RoundData.GameCode.trim() === 'DT20' || data.RoundData.GameCode.trim() === 'VDT' || data.RoundData.GameCode.trim() === 'VDT20') {
              this.RoundHistoryDetails.PlayerACard = cards.Dragon.trim().split(' ');
              this.RoundHistoryDetails.PlayerBCard = cards.Tiger.trim().split(' ');
              const winHand = JSON.parse(data.RoundData.WinningHand);
              this.RoundHistoryDetails.winHand = { Dragon: winHand.Dragon, DragonSuite: winHand.DragonSuite, TigerSuite: winHand.TigerSuite, Tiger: winHand.Tiger };
            } else if (data.RoundData.GameCode.trim() === 'PK' || data.RoundData.GameCode.trim() === 'PK20' || data.RoundData.GameCode.trim() === 'VPK' || data.RoundData.GameCode.trim() === 'VPK20') {
              this.RoundHistoryDetails.TableCard = cards.Table.trim().split(' ');
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
              this.RoundHistoryDetails.PlayerBCard = cards.PlayerB.trim().split(' ');
            } else if (data.RoundData.GameCode.trim() === 'AB' || data.RoundData.GameCode.trim() === 'AB20' || data.RoundData.GameCode.trim() === 'VAB20') {
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
              this.RoundHistoryDetails.PlayerBCard = cards.PlayerB.trim().split(' ');
              this.RoundHistoryDetails.PlayerTCard = cards.PlayerT.trim().split(' ');
              this.abWinnerHand = { Hand: '', JokerSuite: '', WinnerSuite: '' };
              if (data.RoundData.WinningHand) {
                const winHand = JSON.parse(data.RoundData.WinningHand);
                this.abWinnerHand = { Hand: winHand.Hand, JokerSuite: winHand.JokerSuite, WinnerSuite: winHand.WinnerSuite };
              }
            } else if (data.RoundData.GameCode.trim() === 'WM' || data.RoundData.GameCode.trim() === 'VWM') {
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
            } else if (data.RoundData.GameCode.trim() === 'ARW' || data.RoundData.GameCode.trim() === 'VARW') {
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
              this.ARWWinningHand = { Hand: '', Suite: '', Seven: '' };
              if (data.RoundData.WinningHand) {
                const winHand = JSON.parse(data.RoundData.WinningHand);
                this.ARWWinningHand = { Hand: winHand.Hand, Suite: winHand.Suite, Seven: winHand.Seven };
              }
            } else if (data.RoundData.GameCode.trim() === 'C32' || data.RoundData.GameCode.trim() === 'VC32') {
              cards.Player8.Cards = cards.Player8.Cards.trim().split(' ');
              cards.Player9.Cards = cards.Player9.Cards.trim().split(' ');
              cards.Player10.Cards = cards.Player10.Cards.trim().split(' ');
              cards.Player11.Cards = cards.Player11.Cards.trim().split(' ');
              Object.assign(this.RoundHistoryDetails.Card, cards);
            } else if (data.RoundData.GameCode.trim() === 'UD7' || data.RoundData.GameCode.trim() === 'VUD7') {
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
            } else if (data.RoundData.GameCode.trim() === 'PK6' || data.RoundData.GameCode.trim() === 'PK620') {
              this.RoundHistoryDetails.TableCard = cards.Table.trim().split(' ');
              this.RoundHistoryDetails.Player1Card = cards.Player1.trim().split(' ');
              this.RoundHistoryDetails.Player2Card = cards.Player2.trim().split(' ');
              this.RoundHistoryDetails.Player3Card = cards.Player3.trim().split(' ');
              this.RoundHistoryDetails.Player4Card = cards.Player4.trim().split(' ');
              this.RoundHistoryDetails.Player5Card = cards.Player5.trim().split(' ');
              this.RoundHistoryDetails.Player6Card = cards.Player6.trim().split(' ');
            } else if (data.RoundData.GameCode.trim() === 'V3CJ' || data.RoundData.GameCode.trim() === 'D3CJ') {
              this.RoundHistoryDetails.PlayerACard = cards.PlayerA.trim().split(' ');
            } else if (data.RoundData.GameCode.trim() === 'JKR' || data.RoundData.GameCode.trim() === "VJKR") {
              this.RoundHistoryDetails.PlayerACard = cards.Card.trim().split(' ');
              this.JKRWinnerHand = { Number: '', Color: '', CardSuit: '' };
              if (data.RoundData.WinningHand) {
                const winHand = JSON.parse(data.RoundData.WinningHand);
                this.JKRWinnerHand = { Number: winHand.Number, Color: winHand.Color, CardSuit: winHand.CardSuit };
              }
            } else if (data.RoundData.GameCode.trim() === 'VCR' || data.RoundData.GameCode.trim() === 'CR') {
              this.RoundHistoryDetails.SpadeCard = cards.Spade.trim().split(' ');
              this.RoundHistoryDetails.ClubCard = cards.Club.trim().split(' ');
              this.RoundHistoryDetails.DiamondCard = cards.Diamond.trim().split(' ');
              this.RoundHistoryDetails.HeartCard = cards.Heart.trim().split(' ');
              this.CardRaceWinninghand = { Hand: '', Color: '' };
              if (data.RoundData.WinningHand) {
                const winHand = JSON.parse(data.RoundData.WinningHand);
                this.CardRaceWinninghand = { Hand: winHand.Hand, Color: winHand.Color };
              }
            }
          }
        } else {
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private onError(err) {
    this.IsCallMade = false;
    this.toastr.error('Something went wrong. Please try again.');
    this.router.navigateByUrl('/lobby?Token=' + this.Token);
  }

  OpenGames() {
    this.router.navigateByUrl('/lobby?Token=' + this.Token);
  }

  async Logout() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gamesService.LogOut().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === 0) {
          this.IsVerified = false;
          this.GameList = [];
          localStorage.removeItem('AuthToken');
          localStorage.removeItem('Token');
          localStorage.removeItem('GameID');
          this.DisplayMessage = 'Log out successfully';
        } else {
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.IsCallMade = false;
      });
    }
  }
  // toggleVideo(event: any)
  toggleVideo() {
    this.videoplayer.nativeElement.play();
  }
  openVideo(item) {
    this.checkCondition(item.CreatedDate);
    this.videoPath = null;
    this.videoExt = null;
    setTimeout(() => {
      this.videoPath = item.Path;
      this.SummaryID = item.RoundSummaryID;
      this.videoExt = 'video/' + item.Path.substr(item.Path.lastIndexOf('.') + 1);
    }, 100);
  }
  checkCondition(date) {
    // To set two dates to two variables
    const date1 = new Date(this.currentDate);
    const date2 = new Date(date);
    const fullDay = (1000 * 3600 * 24);
    const dateDiff = (date1.getTime() - date2.getTime());
    return (fullDay - dateDiff);
  }
  JokerNumber(num) {
    if (num == '14') { return 'JOKER'; }
    else if (num == '1') { return 'A'; }
    else if (num == '11') { return 'J'; }
    else if (num == '12') { return 'Q'; }
    else if (num == '13') { return 'K'; }
    else if (num == '0') { return '-'; }
    else { return num; }
  }
}
