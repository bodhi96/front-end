import { Component, OnInit, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ConstantService } from '../../services/constant.service';
import { CasinoService } from '../../services/casino.service';
import { ToastrService } from 'ngx-toastr';
import { SignalRService } from 'src/app/services/signalR.service';
import { GamesService } from 'src/app/services/games.service';
// declare const init_page: any;
declare var $: any;

@Component({
  selector: 'app-virtual-3cards-judgement',
  templateUrl: './virtual-3cards-judgement.component.html',
  styleUrls: ['./virtual-3cards-judgement.component.scss']
})
export class Virtual3CardsJudgementComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region Variables

  // Flag Variable
  IsVerified = false;
  IsCallMade = false;
  IsBetPlaced = true;
  IsCashoutBet = false;
  IsOneClick = false;
  IsEdit = false;
  IsReload = false;

  // Display Hide/Show Variable
  ShowTimer = false;
  ShowBetSlip = false;
  ShowBetList = false;
  ShowCashout = false;
  ShowBetDelayOverlay = false;
  ShowGameStatusOverlay = false;

  // Current Game Variables
  Token = '';
  UserName = 'User';
  DisplayID = '';
  RoundSummaryID = 0;
  GameID = 0;
  GameCode = '';
  GameBalance = 0;
  GameLiability = 0;
  GameDetails: any = {};
  GameCard: any = { PlayerACard: ['', '', ''] };
  GameRate: any = {};
  GameStatus = 0;
  GameWinner = '';
  GameWinningHand = '';
  GameRandomRate: any = {};
  GameChips: any = ['5', '10', '25', '50', '100', '250', '500', '1000', '5000'];
  OneClickChips: any = { Chip1: '', Chip2: '', Chip3: '', DefaultChip: '' };
  DefaultChip = '';
  DisplayMessage = '';
  LoginLink = null;
  selectCardList = { firstCard: '', secCard: '', thirdCard: '' };
  selectedBetType = '';

  // Cashout Variable
  Cashout: any = { PlayerAPL: 0, PlayerBPL: 0, LowestOdd: 0, Runner: '' };
  CashoutResponce: any = {};
  CashoutStake: any;
  CashoutPL: any;

  // Profit|Loss Variable
  PL: any = {};

  PlayerPL = 0;
  PlayerPLB = 0;
  PlayerPLL = 0;
  PlayerACashoutPL = 0;
  PlayerBCashoutPL = 0;

  // Game Bet Variable
  GameBetList: any = [];
  GameBetDetails: any = { RoundSummaryID: 0, Rate: 0, Stake: 0, IsBack: 0, Runner: '', Number: 0 };

  // Game History Variable
  GameHistoryList: any = [];
  GameHistoryDetails: any = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''] };

  // SignalR Hub and Timer Variable
  totalTime = 0;
  countDown = 0;
  private setInterval: any;
  private setTimeout: any;
  private balanceInterval: any;
  // private hubConnection: HubConnection;
  CurrentRunner = 'Single';
  PlayerNum = 0;
  PlayerBet = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
  //#endregion
  PlayerDestroy: boolean = false;

  //#region Constructor, OnInit, AfterViewInit, ResetGame
  constructor(
    private router: Router,
    public constant: ConstantService,
    private casinoService: CasinoService,
    private signalRService: SignalRService,
    private toastr: ToastrService,
    private gameService: GamesService) { }
  ngOnInit(): void {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    if (Token !== '') {
      this.Token = Token;
      localStorage.setItem('AuthToken', Token);
      let path = location.pathname;
      path = path.slice(1);
      this.GameCode = path.trim();
      this.GameID = Number(localStorage.getItem(this.GameCode));
      this.ResetGame();
      this.GetUserWiseGameDetails();
      //  this.balanceInterval = setInterval(() => { this.GetBalance(); }, 30 * 1000);
    } else {
      this.IsVerified = false;
      this.DisplayMessage = 'Invalid credentials. Please login again.';
    }
    this.GameRate['Player'] = ['', ''];
  }

  ngAfterViewInit(): void {
    $('.casinocard').on('load', function () {
      setTimeout(() => {
        $(this.parentElement.parentElement).addClass('flip');
      }, 10);
    });
    $('.cashout').hover(() => {
      $('.cashout-price').show();
    }, () => {
      $('.cashout-price').hide();
    });
    $('.open-bet').on('click', () => {
      $('.betslip').addClass('open');
      // $('.overlay').css('display', 'block');
    });
    $('.cancel-betslip').on('click', () => {
      $('.betslip').removeClass('open');
      setTimeout(() => {
        $('.overlay').css('display', 'none');
      }, 100);
    });
    $('.place-betslip').on('click', () => {
      setTimeout(() => {
        $('.betslip').removeClass('open');
        $('.overlay').css('display', 'none');
      }, this.GameDetails.BetPlaceDelay * 1000);
    });
  }

  ngOnDestroy(): void {
    // if (this.hubConnection) {
    //   this.hubConnection.invoke('LeaveGame', this.GameID.toString());
    // }
    this.signalRService.DisconnectHub(this.GameID);
    clearTimeout(this.setTimeout);
    clearInterval(this.setInterval);
  }

  private ResetGame() {

    this.GameCard = { PlayerACard: ['', '', ''] };

    this.GameBetDetails = {
      GameID: this.GameID,
      RoundSummaryID: this.RoundSummaryID,
      Rate: 0,
      Stake: 0,
      IsBack: 0,
      Runner: '',
    };

    this.countDown = 0;
    this.PlayerPL = 0;
    this.PlayerPLB = 0;
    this.PlayerPLL = 0;

    this.GameBetList = [];
    this.ShowBetList = false;
    this.ShowBetSlip = false;
    this.ShowTimer = false;
    this.ShowCashout = false;
    this.ShowGameStatusOverlay = true;

    $('.livecard').removeClass('flip');
    setTimeout(() => {
      $('.betslip').removeClass('open');
      $('.overlay').css('display', 'none');
    }, 100);
  }

  //#endregion

  //#region Get/Set Data Methods
  private async GetBalance() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GetBalance().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.Balance !== undefined && data.Balance != null) {
            this.GameBalance = Number(data.Balance);
          } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
            this.IsVerified = false;
            this.DisplayMessage = this.constant.ErrorMsg.Invalid;
          } else {
            this.toastr.error('Your token is expired. Please login.');
            this.router.navigateByUrl('/lobby?Token=' + this.Token);
            return;
          }
        } else {
          this.toastr.error(data.Status.returnMessage);
          clearInterval(this.balanceInterval);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private async GetUserWiseGameDetails() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gameService.gameDetail(this.GameCode).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          // this.StartHub();
          this.GameID = data?.RoundData?.GameID || data.GamesDetails.GameID;
          localStorage.setItem('GameID', JSON.stringify(this.GameID));
          this.signalRService.ConnectHub(this.GameID, this.SetRoundData.bind(this));
          if (data.UserName) {
            this.IsVerified = true;
            this.UserName = data.UserName;
          } else {
            this.toastr.error('You\'ve been idle for too long.');
            this.router.navigateByUrl('/lobby?Token=' + this.Token);
            return;
          }
          if (data.GamesDetails && data.RoundData) {
            this.GameDetails = data.GamesDetails;
            this.GameLiability = (Number(this.GameDetails.Liability) < 0) ? Number(this.GameDetails.Liability) * -1 : this.GameDetails.Liability;
            if (this.GameDetails.LoginLink) { this.LoginLink = this.GameDetails.LoginLink; }
            if (this.GameDetails.News) {
              this.GameDetails.News = this.GameDetails.News.split('|');
            }
            // init_page(this.GameDetails.StreamingUrl);
            if (this.GameDetails.ChipsSetting) {
              this.constant.GameChips = JSON.parse(this.GameDetails.ChipsSetting);
            } else {
              this.constant.GameChips = this.constant.defaultChipsSetting;
            }
            this.DefaultChip = this.GameDetails.DefaultChip;
            this.RoundSummaryID = data.RoundData.RoundSummaryID;
            this.DisplayID = this.GameDetails.GameCode.trim() + '-' + this.RoundSummaryID;
            if (data.BetList) {
              this.GameBetList = [];
              this.GameBetList = data.BetList;
              this.PlayerPLB = 0;
              this.PlayerPLL = 0;
              this.GameBetList.forEach(element => {
                if (element.IsBack) {
                  this.PlayerPLB += element.PL;
                } else {
                  this.PlayerPLL += element.PL;
                }
              });
            }

            if (this.GameDetails.GameRates && this.GameDetails.GameRates !== '') {
              const rates = JSON.parse(this.GameDetails.GameRates);
              this.GameRate['Player'] = ['', ''];
              this.GameRate['Player'] = [rates['PlayerRates'][0].Lay, rates['PlayerRates'][0].Back];
            }
            this.IsCashoutBet = true;
            this.SetRoundData(data.RoundData);
            this.GetRoundSummaryHistory();
          } else {
            this.IsVerified = false;
            this.DisplayMessage = 'Currently game is in sleep mode.';
            return;
          }
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.IsVerified = false;
          if (data.Status.returnMessage) {
            this.DisplayMessage = data.Status.returnMessage;
          } else {
            this.DisplayMessage = 'Invalid credentials. Please login again.';
          }
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private async GetRoundSummaryHistory() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gameService.GetRoundSummaryHistory(this.GameID).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.IsStop) {
            this.IsVerified = false;
            this.DisplayMessage = 'Currently game is in sleep mode.';
          }
          if (data.RoundHistory) {
            this.GameHistoryList = data.RoundHistory;
          }
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  SetHistoryCards(item) {
    $('.historycard').removeClass('flip');
    this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''] };
    if (item.Cards) {
      setTimeout(() => {
        const cards = JSON.parse(item.Cards);
        Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
        // item.Winner ? item.Winner.trim() : '';
        this.GameHistoryDetails.Winner = this.gameResult(item.Winner);
        this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
        this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
      }, 100);
    }
  }
  //#endregion

  private SetRoundData(data: any) {
    if (data.GameID === this.GameID) {

      if (!this.IsBetPlaced) {
        this.toastr.error('Betting closed. Your bet has not been placed.');
        clearTimeout(this.setTimeout);
        this.IsBetPlaced = true;
        this.ShowBetDelayOverlay = false;
      }
      if (data.Cards && data.Cards !== '') {
        const cards = JSON.parse(data.Cards);
        this.GameCard = { PlayerACard: ['', '', ''] };
        Object.assign(this.GameCard.PlayerACard, cards.PlayerA.trim().split(' '));
      }
      this.GameStatus = Number(data.Status);
      if (this.GameStatus === 0) {
        this.RoundSummaryID = data.RoundSummaryID;
        this.DisplayID = this.GameDetails.GameCode.trim() + '-' + this.RoundSummaryID;
        this.ResetGame();
      } else if (this.GameStatus === 1) {

        if (this.RoundSummaryID !== data.RoundSummaryID) {
          location.reload();
        }
        this.ShowGameStatusOverlay = false;
        this.IsCashoutBet = false;
        this.totalTime = (this.GameCard.PlayerACard[0] !== '') ? this.GameDetails.TimePerCardSec : this.GameDetails.GameStartSec;
        if (data.TimeRemainSec) {
          const sec = (this.GameCard.PlayerACard[0] !== '') ? this.GameDetails.TimePerCardSec : this.GameDetails.GameStartSec;
          const diff = sec - data.TimeRemainSec;
          this.countDown = (diff < 0) ? 0 : diff;
        } else {
          this.countDown = (this.GameCard.PlayerACard[0] !== '') ? this.GameDetails.TimePerCardSec : this.GameDetails.GameStartSec;
        }
        this.StartTimer();
      } else {

        if (this.RoundSummaryID !== data.RoundSummaryID) {
          location.reload();
        }
        this.countDown = 0;
        this.ShowTimer = false;
        this.ShowBetSlip = false;
        this.ShowCashout = false;
        this.ShowGameStatusOverlay = true;
        setTimeout(() => {
          $('.betslip').removeClass('open');
          $('.overlay').css('display', 'none');
        }, 100);
      }
      if (this.GameBetList.length > 0) {
        this.ShowBetList = true;
        if (!this.IsCashoutBet && this.GameStatus === 1) {
          //this.GetCashOut();
        }
      }
      this.GameWinner = (data.Winner) ? data.Winner : '';
      this.GameWinningHand = (data.WinningHand) ? data.WinningHand : '';
      if (this.GameWinner !== '') {
        this.ShowCashout = false;
        if (this.GameHistoryList.filter(res => res.RoundSummaryID === data.RoundSummaryID).length === 0) {
          this.GameHistoryList.unshift(data);
          if (this.GameHistoryList.length > 10) {
            this.GameHistoryList.pop();
          }
        }
        this.CheckGameStatus();
      }
    }
  }

  private StartTimer() {
    if (this.ShowTimer) {
      this.ShowTimer = false;
      clearInterval(this.setInterval);
    }
    this.ShowTimer = true;
    this.TimerInterval();
  }

  private TimerInterval() {
    this.setInterval = setInterval(() => {
      this.countDown = Number(--this.countDown);
      if (this.countDown <= 0) {
        this.countDown = 0;
        this.GameStatus = (!this.GameWinner) ? 2 : this.GameStatus;
        this.ShowTimer = false;
        this.ShowBetSlip = false;
        this.ShowCashout = false;
        this.ShowGameStatusOverlay = true;
        if (!this.IsBetPlaced) {
          this.IsBetPlaced = true;
          this.ShowBetDelayOverlay = false;
          clearTimeout(this.setTimeout);
          this.toastr.error('Betting closed. Your bet has not been placed.');
        }
        this.ClearBet();
        clearInterval(this.setInterval);
      }
    }, 1000);
  }
  //#endregion

  //#region Add Bet,Validate Bet, Calculate Profit Loss
  ChangeRunner(runner) {
    this.CurrentRunner = runner;
    this.ShowBetSlip = false;
    this.ClearBet();
  }

  AddBet(player, num, isBack) {
    let number = null;
    number = '2' + this.CardStringToNumber(this.selectCardList.firstCard) + this.CardStringToNumber(this.selectCardList.secCard) + this.CardStringToNumber(this.selectCardList.thirdCard);
    this.PlayerNum = Number(number);
    // this.GameRate[player][isBack] !== '' && this.GameStatus === 1
    if((this.selectCardList.firstCard != this.selectCardList.secCard) || (this.selectCardList.firstCard != this.selectCardList.thirdCard) || (this.selectCardList.thirdCard != this.selectCardList.secCard)){
      if (this.GameRate[player][isBack] !== '' && this.GameStatus === 1) {
        this.GameBetDetails.Runner = player;
        this.GameBetDetails.Number = this.PlayerNum;
        this.GameBetDetails.IsBack = isBack;
        this.GameBetDetails.Rate = Number(this.GameRate[player][isBack]);
        this.ShowBetSlip = true;
        this.IsCashoutBet = false;
        // this.IsOneClick
        if (this.IsOneClick && this.selectCardList.firstCard && this.selectCardList.secCard && this.selectCardList.thirdCard) {
          this.GameBetDetails.Stake = this.DefaultChip;
          this.ValidateBet();
        } else {
          this.CalculatePL(1);
        }
      } else {
        $('.betslip').removeClass('open');
        setTimeout(() => {
          $('.overlay').css('display', 'none');
        }, 100);
        this.toastr.error('Betting closed. You can\'t bet right now.');
        this.selectCardList = { firstCard: '', secCard: '', thirdCard: '' };
        this.selectedBetType = null;
      }
    }
  }
  onCalculatePL(event) {
    this.CalculatePL(event.isCheck, event.CashOutBet);
  }
  AddStake(stake) {
    if (this.GameBetDetails.Rate && this.GameBetDetails.Rate !== '-' && this.GameStatus === 1) {
      this.GameBetDetails.Stake += +stake;
      this.CalculatePL(1);
    } else {
      this.toastr.error('You can not bet right now.');
    }
  }

  gameKeyBoardClick(key) {
    if (this.GameBetDetails.Rate && this.GameBetDetails.Rate !== '-' && this.GameStatus === 1) {
      if (!this.GameBetDetails.Stake) {
        this.GameBetDetails.Stake = 0;
      }
      if (this.GameBetDetails.Stake.toString().length < 6 || key === '<') {
        if (key === '<') {
          this.GameBetDetails.Stake = Number(this.GameBetDetails.Stake.toString().slice(0, -1));
          this.CalculatePL(1);
        } else {
          key = (this.GameBetDetails.Stake.toString().length === 5 && key === '00' ? '0' : key);
          this.GameBetDetails.Stake = Number(this.GameBetDetails.Stake.toString() + key);
        }
        this.CalculatePL(1);
      }
    } else {
      this.toastr.error('You can not bet right now.');
    }
  }

  CalculatePL(isCheck: number, CashOutBet: boolean = false) {
    // if (this.GameBalance <= 0) {
    //   this.toastr.error('You have insufficient balance.');
    //   return;
    // }
    if (isCheck === 1 && !this.IsOneClick) {
      this.GameBetDetails.Stake = Math.round(this.GameBetDetails.Stake);
      if (this.GameBetDetails.Stake) {
        if (!CashOutBet && (this.GameDetails.MinStakePerBet !== -1 && this.GameBetDetails.Stake < this.GameDetails.MinStakePerBet)) {
          this.GameBetDetails.Stake = this.GameDetails.MinStakePerBet;
          this.toastr.error('Your stake is below minimum stake ' + this.GameDetails.MinStakePerBet);
        }
        if (!CashOutBet && (this.GameDetails.MaxStakePerBet !== -1 && this.GameBetDetails.Stake > this.GameDetails.MaxStakePerBet)) {
          this.GameBetDetails.Stake = this.GameDetails.MaxStakePerBet;
          this.toastr.error('Your stake is above maximum stake ' + this.GameDetails.MaxStakePerBet);
        }
      }
    }
    // this.PlayerPL = (this.GameBetDetails.Stake * this.GameBetDetails.Rate);// - this.GameBetDetails.Stake;
    this.PlayerPL = (this.GameBetDetails.Stake * this.GameBetDetails.Rate) - this.GameBetDetails.Stake;
  }

  private CalculateProfitLoss(runner: string): number {
    let pl = 0;
    if (this.GameBetDetails.IsBack && this.GameBetDetails.Runner === runner) {
      pl = ((this.GameBetDetails.Stake * this.GameBetDetails.Rate)); //- this.GameBetDetails.Stake);
    } else if (this.GameBetDetails.IsBack && this.GameBetDetails.Runner !== runner) {
      pl = (-1 * this.GameBetDetails.Stake);
    } else if (!this.GameBetDetails.IsBack && this.GameBetDetails.Runner === runner) {
      pl = (-1 * ((this.GameBetDetails.Stake * this.GameBetDetails.Rate))); // - this.GameBetDetails.Stake));
    } else if (!this.GameBetDetails.IsBack && this.GameBetDetails.Runner !== runner) {
      pl = this.GameBetDetails.Stake;
    }
    return pl;
  }

  PlaceCashOutBet() {
    this.ClearBet();
    this.ShowBetSlip = true;
    this.IsCashoutBet = true;
    Object.assign(this.GameBetDetails, this.CashoutResponce);
    this.ValidateBet(true);
  }

  PlaceEnterBet(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      this.ValidateBet();
    }
    if (event.keyCode === 46) {
      event.preventDefault();
    }
  }

  ValidateBet(CashOutBet: boolean = false) {
    // if (this.GameBalance <= 0) {
    //   this.toastr.error('You have insufficient balance.');
    // } else
    if (!this.constant.IsAllowBet()) {
      this.toastr.warning('You are not allow to place bet, Contact your up line.');
      // this.ShowBetDelayOverlay || this.GameStatus !== 1
    } else if (this.ShowBetDelayOverlay || this.GameStatus !== 1) {
      this.toastr.error('Betting closed. You can\'t bet right now.');
    } else if (!this.GameBetDetails.Rate || this.GameBetDetails.Rate === '-') {
      this.toastr.error('You can not bet right now.');
    } else if (this.GameDetails.MaxRateLimit !== -1 && this.GameBetDetails.Rate > this.GameDetails.MaxRateLimit) {
      this.toastr.error('You are not allowed to play with betting rate ' + this.GameBetDetails.Rate);
    } else if (!this.GameBetDetails.Stake) {
      this.toastr.error('Please enter valid stack');
    } else if (!CashOutBet && (this.GameDetails.MinStakePerBet !== -1 && this.GameBetDetails.Stake < this.GameDetails.MinStakePerBet)) {
      this.toastr.error('Your stake is below minimum stake ' + this.GameDetails.MinStakePerBet);
      this.GameBetDetails.Stake = this.GameDetails.MinStakePerBet;
      this.CalculatePL(1);
    } else if (!CashOutBet && (this.GameDetails.MaxStakePerBet !== -1 && this.GameBetDetails.Stake > this.GameDetails.MaxStakePerBet)) {
      this.toastr.error('Your stake is above maximum stake ' + this.GameDetails.MaxStakePerBet);
      this.GameBetDetails.Stake = this.GameDetails.MaxStakePerBet;
      this.CalculatePL(1);
    } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL.PlayerAPL + this.PL.PlayerATempPL) > this.GameDetails.MaxProfit)) {
      this.toastr.error('Your profit is above your limits.');
    } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL.PlayerBPL + this.PL.PlayerBTempPL) > this.GameDetails.MaxProfit)) {
      this.toastr.error('Your profit is above your limits.');
    } else {
      this.CalculatePL(1, CashOutBet);
      this.ShowBetDelayOverlay = true;
      this.ShowCashout = false;
      this.IsBetPlaced = false;
      this.setTimeout = setTimeout(() => {
        // this.GameStatus === 1
        if (this.GameStatus === 1) {
          this.PlaceBet(CashOutBet);
        }
        clearTimeout(this.setTimeout);
      }, this.GameDetails.BetPlaceDelay * 1000);
      return;
    }
  }

  private async PlaceBet(CashOutBet: boolean = false) {
    // !this.IsCallMade && !this.IsBetPlaced
    if (!this.IsCallMade && !this.IsBetPlaced) {
      this.GameBetDetails.Runner = this.GameBetDetails.Runner;
      this.GameBetDetails.GameID = this.GameID;
      this.GameBetDetails.RoundSummaryID = this.RoundSummaryID;
      this.GameBetDetails.IsCashOutBet = CashOutBet;
      this.IsCallMade = true;
      await this.casinoService.AddUpdateBet(this.GameBetDetails).subscribe(data => {
        this.IsBetPlaced = true;
        this.IsCallMade = false;
        this.ShowBetSlip = false;
        this.ShowBetDelayOverlay = false;
        this.ClearBet();
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (this.constant.IsTabActive && this.constant.IsSound) {
            this.constant.betPlaceAudio.play();
          }
          if (data.Balance !== undefined && data.Balance != null) { this.GameBalance = Number(data.Balance); }
          if (data.Liability !== undefined && data.Liability != null) { this.GameLiability = (Number(data.Liability) < 0) ? Number(data.Liability) * -1 : Number(data.Liability); }
          // if (data.PlayerAPL) {
          //   this.PL.PlayerAPL = Number(data.PlayerAPL);
          // }
          if (data.BetList) {
            this.GameBetList = [];
            this.GameBetList = data.BetList;
            this.GameBetList.forEach(element => {
              if (element.IsBack) {
                this.PlayerPLB += element.PL;
              } else {
                this.PlayerPLL += element.PL;
              }
            });
            if (this.GameBetList.length > 0) {
              this.ShowBetList = true;
              if (!this.IsCashoutBet && this.GameStatus === 1) {
                //this.GetCashOut();
              }
            }
          }
          this.toastr.success('Bet placed successfully.');
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          if (data.Status.code === 1) {
            this.toastr.warning(data.Status.returnMessage);
          } else {
            this.toastr.error(data.Status.returnMessage);
          }
        }
      }, err => {
        this.IsCallMade = false;
        this.IsBetPlaced = true;
        this.ShowBetDelayOverlay = false;
        this.ShowGameStatusOverlay = false;
        this.toastr.error('Something went wrong. Please try again.');
      });
    }
  }

  ClearBet() {
    this.selectCardList = { firstCard: '', secCard: '', thirdCard: '' };
    this.selectedBetType = null;
    this.GameBetDetails = {
      GameID: this.GameID,
      RoundSummaryID: this.RoundSummaryID,
      Rate: 0,
      Stake: 0,
      IsBack: 0,
      Runner: '',
    };
    this.PlayerNum = null;
    this.CalculatePL(0);
    $('.betslip').removeClass('open');
    $('.overlay').css('display', 'none');
  }
  //#endregion

  //#region Cashout Calculation
  async GetCashOut() {
    this.PlayerACashoutPL = 0;
    this.PlayerBCashoutPL = 0;
  }



  CalculateSize(ltp, p1, p2) {
    return (p2 - p1) / ltp;
  }
  CalculateP1After(size, p1, ltp) {
    return p1 + size * (ltp - 1);
  }
  //#endregion

  //#region One Click Bet
  EditOneClickChips(flag: number = 0) {
    if (flag === 0) { this.IsEdit = !this.IsEdit; }
    this.OneClickChips.Chip1 = this.GameDetails.Chip1;
    this.OneClickChips.Chip2 = this.GameDetails.Chip2;
    this.OneClickChips.Chip3 = this.GameDetails.Chip3;
    this.OneClickChips.DefaultChip = this.DefaultChip;
    if (flag === 1) { this.AddUpdateOneClickChips(); }
  }

  async AddUpdateOneClickChips() {
    if (this.OneClickChips.Chip1 === ''
      || this.OneClickChips.Chip2 === ''
      || this.OneClickChips.Chip3 === ''
      || this.OneClickChips.DefaultChip === '') {
      this.toastr.error('Please enter valid chip values.');
      return;
    }

    if (!this.constant.CheckNumberFormat(this.OneClickChips.Chip1)
      || !this.constant.CheckNumberFormat(this.OneClickChips.Chip2)
      || !this.constant.CheckNumberFormat(this.OneClickChips.Chip3)
    ) {
      this.toastr.error('Please enter valid chip values.');
      return;
    }
    if (this.OneClickChips.Chip1 === this.OneClickChips.Chip2
      || this.OneClickChips.Chip2 === this.OneClickChips.Chip3
      || this.OneClickChips.Chip3 === this.OneClickChips.Chip1) {
      this.toastr.error('Please enter different chip values.');
      return;
    }
    if (this.GameDetails.MaxStakePerBet !== -1 &&
      (this.OneClickChips.Chip1 > this.GameDetails.MaxStakePerBet
        || this.OneClickChips.Chip2 > this.GameDetails.MaxStakePerBet
        || this.OneClickChips.Chip3 > this.GameDetails.MaxStakePerBet)) {
      this.toastr.error('Please enter chip values within your stack limit.');
      return;
    }
    if ((this.OneClickChips.DefaultChip !== this.OneClickChips.Chip1)
      && (this.OneClickChips.DefaultChip !== this.OneClickChips.Chip2)
      && (this.OneClickChips.DefaultChip !== this.OneClickChips.Chip3)) {
      this.OneClickChips.DefaultChip = this.OneClickChips.Chip1;
      this.DefaultChip = this.OneClickChips.Chip1;
    }

    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.AddUpdateOneClickChips(this.OneClickChips).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          this.GameDetails.Chip1 = this.OneClickChips.Chip1;
          this.GameDetails.Chip2 = this.OneClickChips.Chip2;
          this.GameDetails.Chip3 = this.OneClickChips.Chip3;
          this.toastr.success('Chip set successfully');
          this.IsEdit = false;
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.toastr.error(data.Status.returnMessage);
          this.router.navigateByUrl('/lobby?Token=' + this.Token);
        }
      }, err => {
        this.onError(err);
      });
    }
  }
  //#endregion

  //#region Other Methods
  async CheckGameStatus() {
    await this.casinoService.CheckGameStatus().subscribe(data => {
      if (data.Status.code === this.constant.HttpResponceCode.Success) {
        if (data.IsStop) {
          this.IsVerified = false;
          if (data.Message !== '') {
            this.DisplayMessage = data.Message;
          } else {
            this.DisplayMessage = 'Currently game is in sleep mode.';
          }
        }
      } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
        this.IsVerified = false;
        this.DisplayMessage = this.constant.ErrorMsg.Invalid;
      } else {
        this.toastr.error(data.Status.returnMessage);
        this.router.navigateByUrl('/lobby?Token=' + this.Token);
      }
    }, err => {
      this.onError(err);
    });
  }

  async Logout() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GameLogOut().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          const url = new URL(window.location.href);
          const Token = url.searchParams.get('Token');
          localStorage.removeItem('Token');
          localStorage.removeItem('GameID');
          this.router.navigateByUrl('/lobby?Token=' + Token);
        } else {
          this.toastr.error(data.Status.returnMessage);
          this.router.navigateByUrl('/lobby?Token=' + this.Token);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private onError(err) {
    this.IsCallMade = false;
    this.toastr.error('Something went wrong. Please try again.');
    this.router.navigateByUrl('/lobby?Token=' + this.Token);
  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: FocusEvent): void {
    this.constant.IsTabActive = true;
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: FocusEvent): void {
    this.constant.IsTabActive = false;
  }
  oneClick(event) {
    this.IsOneClick = event;
  }

  stringToNumber(strNumber) {
    return +strNumber;
  }
  gameResult(value) {
    let gameResult = [" ", " ", " ", " ", " "];
    // const gameResult = JSON.parse(value);
    Object.assign(gameResult, value.trim().split(' '));
    const fistCard = this.setResult(gameResult[1]);
    const secCard = this.setResult(gameResult[2]);
    const thirdCard = this.setResult(gameResult[3]);

    return fistCard + ' ' + secCard + ' ' + thirdCard;
  }
  setResult(value) {
    if (value > 10) {
      if (Number(value) === 11) { return "J"; }
      else if (Number(value) === 12) { return "Q"; }
      else if (Number(value) === 13) { return "K"; }
      else { return null; }
    } else if (Number(value) === 1) {
      return "A";
    } else {
      return value;
    }
  }
  selectedCard(type, num) {
    if (this.selectedBetType == type && (this.selectCardList.secCard == num || this.selectCardList.thirdCard == num || this.selectCardList.firstCard == num)) {
      return true;
    } else {
      return false;
    }
    // if (this.selectedBetType === '') {
    //   this.selectedBetType = type;
    //   this.selectCardList.push = num;
    // } else if (this.selectedBetType === type) {
    //   if (this.selectedBetType.length < 3){
    //     this.selectCardList.push = num;
    //   }else{
    //     // this.toastr.warning('Max 3 Cards Select');
    //     return false;
    //   }
    // }else{
    //   this.selectedBetType = type;
    //   this.selectCardList = [];
    //   this.selectCardList.push = num;
    // }
    // console.log(this.selectedBetType);
    // console.log(this.selectCardList);

  }
  selectCard(type, num) {
    this.selectedBetType = type;
    if (this.selectCardList.secCard != num && this.selectCardList.thirdCard != num && this.selectCardList.firstCard != num) {
      if (!this.selectCardList.firstCard) {
        this.selectCardList.firstCard = num;
      } else if (!this.selectCardList.secCard) {
        this.selectCardList.secCard = num;
      } else if (!this.selectCardList.thirdCard) {
        this.selectCardList.thirdCard = num;
      } else {
        this.toastr.warning('Max 3 Cards Select');
      }
    } else {
      if (this.selectCardList.firstCard == num) {
        this.selectCardList.firstCard = '';
      } else if (this.selectCardList.secCard == num) {
        this.selectCardList.secCard = '';
      } else if (this.selectCardList.thirdCard == num) {
        this.selectCardList.thirdCard = '';
      }
    }
    let isBack = 0;
    if (this.selectedBetType == 'Yes') {
      isBack = 1;
    } else {
      isBack = 0;
    }
    
    // number = this.selectCardList.firstCard + this.selectCardList.secCard + this.selectCardList.thirdCard;
    // console.log(number);
    this.AddBet('Player', 0, isBack)
    // console.log(Number(number));
  }
  CardStringToNumber(card) {
    if (card == 'A') {
      return '01';
    } else if (card == 'J') {
      return '11';
    } else if (card == 'Q') {
      return '12';
    } else if (card == 'K') {
      return '13';
    } else if (card == '10') {
      return card;
    } else if (card) {
      return '0' + card;
    } else {
      return '';
    }

  }
  PlayerDes(event) {
    this.PlayerDestroy = event;
  }
  //#endregion
}
