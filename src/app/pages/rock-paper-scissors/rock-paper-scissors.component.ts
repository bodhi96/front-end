import { Component, OnInit, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ConstantService } from '../../services/constant.service';
import { CasinoService } from '../../services/casino.service';
import { ToastrService } from 'ngx-toastr';
import { SignalRService } from 'src/app/services/signalR.service';
import { GamesService } from 'src/app/services/games.service';
// declare const init_page: any;
declare var $: any;

@Component({
  selector: 'app-rock-paper-scissors',
  templateUrl: './rock-paper-scissors.component.html',
  styleUrls: ['./rock-paper-scissors.component.scss']
})
export class RockPaperScissorsComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region Variables

  // Flag Variable
  IsVerified = false;
  IsCallMade = false;
  IsBetPlaced = true;
  IsCashoutBet = false;
  IsOneClick = false;
  IsEdit = false;

  // Display Hide/Show Variable
  ShowTimer = false;
  ShowBetSlip = false;
  ShowBetList = false;
  ShowCashout = false;
  ShowBetDelayOverlay = false;
  ShowGameStatusOverlay = false;

  // Current Game Variables
  Token = '';
  UserName = 'User';
  DisplayID = '';
  RoundSummaryID = 0;
  GameID = 0;
  GameCode = '';
  GameBalance = 0;
  GameLiability = 0;
  GameDetails: any = {};
  GameStreamingUrl: any = '';
  GameCard: any = { SilverCard: [''], GoldCard: [''] };
  GameRate: any = {
    SilverRock: ['', ''],
    SilverPaper: ['', ''],
    SilverScissors: ['', ''],
    GoldRock: ['', ''],
    GoldPaper: ['', ''],
    GoldScissors: ['', ''],
  };
  GameStatus = 0;
  GameWinner: any = { Silver: '', Gold: '' };
  GameWinningHand = '';
  GameChips: any = ['5', '10', '25', '50', '100', '250', '500', '1000', '5000'];
  OneClickChips: any = { Chip1: '', Chip2: '', Chip3: '', DefaultChip: '' };
  DefaultChip = '';
  DisplayMessage = '';
  LoginLink = null;
  PlayerDestroy: boolean = false;

  // Cashout Variable
  Cashout: any = { PlayerAPL: 0, PlayerBPL: 0, LowestOdd: 0, Runner: '' };
  CashoutResponce: any = {};
  CashoutStake: any;
  CashoutPL: any;

  // Profit|Loss Variable
  PlayerPL = 0;
  PL: any = {};
  // Game Bet Variable
  GameBetList: any = [];
  GameBetDetails: any = { RoundSummaryID: 0, Rate: 0, Stake: 0, IsBack: 0, Runner: '', };

  // Game History Variable
  GameHistoryList: any = [];
  GameHistoryDetails: any = { DisplayID: '', Winner: { Silver: '', Gold: '' }, SilverCard: [''], GoldCard: [''], WinningHand: '' };

  // SignalR Hub and Timer Variable
  totalTime = 0;
  countDown = 0;
  SilverBet: boolean = false;
  GoldBet: boolean = false;
  private setInterval: any;
  private setTimeout: any;
  private balanceInterval: any;

  CashoutDetails: any = {
    Runner1PL: 0, // Runner 1 Profit Loss
    Runner2PL: 0, // Runner 2 Profit Loss
    Runner1BRate: 0, // Runner 1 Back Rate
    Runner2BRate: 0, // Runner 2 Back Rate
    Runner1LRate: 0, // Runner 1 Lay Rate
    Runner2LRate: 0, // Runner 2 Lay Rate
    Runner1Title: 'PlayerA', // Get Runner1 Title
    Runner2Title: 'PlayerB', // Get Runner2 Title
    LTP: 0, // LTP (Last Traded Price)
    PLRunner1: '', // Runner 1 Profit Loss -Update Every Call
    PLRunner2: '', // Runner 2 Profit Loss -Update Every Call
    PLCashout: '', // Cashout Profit Loss
    Runner1BNewRate: '', // Runner 1 Back Rate
    Runner2BNewRate: '', // Runner 2 Back Rate
    Runner1LNewRate: '', // Runner 1 Lay Rate
    Runner2LNewRate: '' // Runner 2 Lay Rate
  };
  //#endregion

  //#region Constructor, OnInit, AfterViewInit, ResetGame
  constructor(
    private router: Router,
    public constant: ConstantService,
    private casinoService: CasinoService,
    private signalRService: SignalRService,
    private toastr: ToastrService,
    private gameService: GamesService) { }

  ngOnInit(): void {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    if (Token !== '') {
      this.Token = Token;
      localStorage.setItem('AuthToken', Token);
      let path = location.pathname;
      path = path.slice(1);
      this.GameCode = path.trim();
      this.GameID = Number(localStorage.getItem(this.GameCode));
      this.ResetGame();
      this.GetUserWiseGameDetails();
      //    this.balanceInterval = setInterval(() => { this.GetBalance(); }, 30 * 1000);
    } else {
      this.IsVerified = false;
      this.DisplayMessage = 'Invalid credentials. Please login again.';
    }
  }

  ngAfterViewInit(): void {
    $('.casinocard').on('load', function () {
      setTimeout(() => {
        $(this.parentElement.parentElement).addClass('flip');
      }, 10);
    });
    $('.cashout').hover(() => {
      $('.cashout-price').show();
    }, () => {
      $('.cashout-price').hide();
    });
    $('.open-bet').on('click', () => {
      $('.betslip').addClass('open');
      // $('.overlay').css('display', 'block');
    });
    $('.cancel-betslip').on('click', () => {
      $('.betslip').removeClass('open');
      setTimeout(() => {
        $('.overlay').css('display', 'none');
      }, 100);
    });
    $('.place-betslip').on('click', () => {
      setTimeout(() => {
        $('.betslip').removeClass('open');
        $('.overlay').css('display', 'none');
      }, this.GameDetails.BetPlaceDelay * 1000);
    });
  }

  ngOnDestroy(): void {
    this.signalRService.DisconnectHub(this.GameID);
    clearTimeout(this.setTimeout);
    clearInterval(this.setInterval);
  }

  private ResetGame() {

    this.GameCard = { SilverCard: [''], GoldCard: [''] };
    this.GameBetDetails = {
      GameID: this.GameID,
      RoundSummaryID: this.RoundSummaryID,
      Rate: 0,
      Stake: 0,
      IsBack: 0,
      Runner: '',
    };
    this.SilverBet = false;
    this.GoldBet = false;
    this.countDown = 0;
    this.PlayerPL = 0;
    Object.keys(this.constant.GameRunner.RPSR.Silver).map((key) => {
      this.PL[key + 'PL'] = 0;
    });
    Object.keys(this.constant.GameRunner.RPSR.Gold).map((key) => {
      this.PL[key + 'PL'] = 0;
    });
    this.GameBetList = [];
    this.ShowBetList = false;
    this.ShowBetSlip = false;
    this.ShowTimer = false;
    this.ShowCashout = false;
    this.ShowGameStatusOverlay = true;

    $('.livecard').removeClass('flip');
    setTimeout(() => {
      $('.betslip').removeClass('open');
      $('.overlay').css('display', 'none');
    }, 100);
  }

  //#endregion

  //#region Get/Set Data Methods
  private async GetBalance() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GetBalance().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.Balance !== undefined && data.Balance != null) {
            this.GameBalance = Number(data.Balance);
          } else {
            this.toastr.error('Your token is expired. Please login.');
            this.router.navigateByUrl('/lobby?Token=' + this.Token);
            return;
          }
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.toastr.error(data.Status.returnMessage);
          clearInterval(this.balanceInterval);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private async GetUserWiseGameDetails() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gameService.gameDetail(this.GameCode).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          // this.StartHub();
          this.GameID = data?.RoundData?.GameID || data.GamesDetails.GameID;
          localStorage.setItem('GameID', JSON.stringify(this.GameID));
          this.signalRService.ConnectHub(this.GameID, this.SetRoundData.bind(this));
          if (data.UserName) {
            this.IsVerified = true;
            this.UserName = data.UserName;
          } else {
            this.toastr.error('You\'ve been idle for too long.');
            this.router.navigateByUrl('/lobby?Token=' + this.Token);
            return;
          }
          if (data.GamesDetails && data.RoundData) {
            this.GameDetails = data.GamesDetails;
            this.GameLiability = (Number(this.GameDetails.Liability) < 0) ? Number(this.GameDetails.Liability) * -1 : this.GameDetails.Liability;
            if (this.GameDetails.LoginLink) { this.LoginLink = this.GameDetails.LoginLink; }
            if (this.GameDetails.News) {
              this.GameDetails.News = this.GameDetails.News.split('|');
            }
            // init_page(this.GameDetails.StreamingUrl);
            if (this.GameDetails.ChipsSetting) {
              this.constant.GameChips = JSON.parse(this.GameDetails.ChipsSetting);
            } else {
              this.constant.GameChips = this.constant.defaultChipsSetting;
            }
            this.DefaultChip = this.GameDetails.DefaultChip;
            this.RoundSummaryID = data.RoundData.RoundSummaryID;
            this.DisplayID = this.GameDetails.GameCode.trim() + '-' + this.RoundSummaryID;
            if (data.BetList) {
              this.GameBetList = [];
              this.GameBetList = data.BetList;
              if (this.GameBetList.length > 0) {
                this.GameBetList.forEach(element => {
                  if (element.Runner == "SilverRock" || element.Runner == "SilverPaper" || element.Runner == "SilverScissors") {
                    this.SilverBet = true;
                  }
                  if (element.Runner == "GoldRock" || element.Runner == "GoldPaper" || element.Runner == "GoldScissors") {
                    this.GoldBet = true;
                  }
                });
              }
              Object.keys(this.constant.GameRunner.RPSR.Silver).map((key) => {
                this.PL[key + 'PL'] = (data[key + 'PL']) ? Number(data[key + 'PL']) : 0;
              });
              Object.keys(this.constant.GameRunner.RPSR.Gold).map((key) => {
                this.PL[key + 'PL'] = (data[key + 'PL']) ? Number(data[key + 'PL']) : 0;
              });
            }
            if (this.GameDetails.GameRates) {
              const rates = JSON.parse(this.GameDetails.GameRates);
              this.GameRate = {
                SilverRock: ['', ''],
                SilverPaper: ['', ''],
                SilverScissors: ['', ''],
                GoldRock: ['', ''],
                GoldPaper: ['', ''],
                GoldScissors: ['', ''],
              };
              this.GameRate['SilverRock'] = [rates['SilverRockRates'][0].Lay, rates['SilverRockRates'][0].Back];
              this.GameRate['SilverPaper'] = [rates['SilverPaperRates'][0].Lay, rates['SilverPaperRates'][0].Back];
              this.GameRate['SilverScissors'] = [rates['SilverScissorsRates'][0].Lay, rates['SilverScissorsRates'][0].Back];
              this.GameRate['GoldRock'] = [rates['GoldRockRates'][0].Lay, rates['GoldRockRates'][0].Back];
              this.GameRate['GoldPaper'] = [rates['GoldPaperRates'][0].Lay, rates['GoldPaperRates'][0].Back];
              this.GameRate['GoldScissors'] = [rates['GoldScissorsRates'][0].Lay, rates['GoldScissorsRates'][0].Back];

            }
            this.IsCashoutBet = true;
            this.SetRoundData(data.RoundData);
            this.GetRoundSummaryHistory();
          } else {
            this.IsVerified = false;
            this.DisplayMessage = 'Currently game is in sleep mode.';
            return;
          }
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.IsVerified = false;
          if (data.Status.returnMessage) {
            this.DisplayMessage = data.Status.returnMessage;
          } else {
            this.DisplayMessage = 'Invalid credentials. Please login again.';
          }
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private async GetRoundSummaryHistory() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gameService.GetRoundSummaryHistory(this.GameID).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.IsStop) {
            this.IsVerified = false;
            this.DisplayMessage = 'Currently game is in sleep mode.';
          }
          if (data.RoundHistory) {
            this.GameHistoryList = data.RoundHistory;
          }
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  SetHistoryCards(item) {
    $('.historycard').removeClass('flip');
    this.GameHistoryDetails = { DisplayID: '', Winner: { Silver: '', Gold: '' }, SilverCard: [''], GoldCard: [''], WinningHand: '' };
    if (item.Cards) {
      setTimeout(() => {
        const cards = item.Cards ? JSON.parse(item.Cards) : '';
        const winner = item.Winner ? JSON.parse(item.Winner) : '';
        if (winner) {
          this.GameHistoryDetails.Winner.Silver = winner.Silver;
          this.GameHistoryDetails.Winner.Gold = winner.Gold;
          // console.log(this.GameHistoryDetails.Winner.Silver, this.GameHistoryDetails.Winner.Gold);
        }
        if (cards) {
          Object.assign(this.GameHistoryDetails.SilverCard, String(cards.Silver || '').trim().split(' '));
          Object.assign(this.GameHistoryDetails.GoldCard, String(cards.Gold || '').trim().split(' '));
          this.GameHistoryDetails.DisplayID = String(this.GameDetails.GameCode || '').trim() + '-' + item.RoundSummaryID;
        }
      }, 100);
    }
  }
  //#endregion

  //#region SignalR Hub , SetRoundData, Timer
  private SetRoundData(data: any) {
    if (data.GameID === this.GameID) {

      if (!this.IsBetPlaced) {
        this.toastr.error('Betting closed. Your bet has not been placed.');
        clearTimeout(this.setTimeout);
        this.IsBetPlaced = true;
        this.ShowBetDelayOverlay = false;
      }
      if (data.Cards && data.Cards !== '') {
        const cards = JSON.parse(data.Cards);
        this.GameCard = { SilverCard: [''], GoldCard: [''] };
        Object.assign(this.GameCard.SilverCard, cards.Silver ? cards.Silver.trim().split(' ') : '');
        Object.assign(this.GameCard.GoldCard, cards.Gold ? cards.Gold.trim().split(' ') : '');
      }
      this.GameStatus = Number(data.Status);
      if (this.GameStatus === 0) {
        this.RoundSummaryID = data.RoundSummaryID;
        this.DisplayID = this.GameDetails.GameCode.trim() + '-' + this.RoundSummaryID;
        this.ResetGame();
      } else if (this.GameStatus === 1) {

        if (this.RoundSummaryID !== data.RoundSummaryID) {
          location.reload();
        }
        this.ShowGameStatusOverlay = false;
        this.IsCashoutBet = false;
        this.totalTime = (this.GameCard.SilverCard[0] !== '') ? this.GameDetails.TimePerCardSec : this.GameDetails.GameStartSec;
        if (data.TimeRemainSec) {
          const sec = (this.GameCard.SilverCard[0] !== '') ? this.GameDetails.TimePerCardSec : this.GameDetails.GameStartSec;
          const diff = sec - data.TimeRemainSec;
          this.countDown = (diff < 0) ? 0 : diff;
        } else {
          this.countDown = (this.GameCard.SilverCard[0] !== '') ? this.GameDetails.TimePerCardSec : this.GameDetails.GameStartSec;
        }
        this.StartTimer();
      } else {

        if (this.RoundSummaryID !== data.RoundSummaryID) {
          location.reload();
        }
        this.countDown = 0;
        this.ShowTimer = false;
        this.ShowBetSlip = false;
        this.ShowCashout = false;
        this.ShowGameStatusOverlay = true;
        setTimeout(() => {
          $('.betslip').removeClass('open');
          $('.overlay').css('display', 'none');
        }, 100);
      }
      if (this.GameBetList.length > 0) {
        this.ShowBetList = true;
        // if (!this.IsCashoutBet && this.GameStatus === 1) {
        //   this.GetCashOut();
        // }
      }
      // this.GameRandomRate = { '7Up': ['', ''], '7': ['', ''], '7Down': ['', ''], 'Odd': ['', ''], 'Even': ['', ''] };
      // this.GameRandomRate['7Up'] = [this.constant.GenerateRandom, this.constant.GenerateRandom];
      // this.GameRandomRate['7'] = [this.constant.GenerateRandom, this.constant.GenerateRandom];
      // this.GameRandomRate['7Down'] = [this.constant.GenerateRandom, this.constant.GenerateRandom];
      // this.GameRandomRate['Odd'] = [this.constant.GenerateRandom, this.constant.GenerateRandom];
      // this.GameRandomRate['Even'] = [this.constant.GenerateRandom, this.constant.GenerateRandom];
      const winner = data.Winner ? JSON.parse(data.Winner) : '';
      this.GameWinner = { Silver: '', Gold: '' };
      if (winner) {
        this.ShowCashout = false;
        this.GameWinner.Silver = winner.Silver;
        this.GameWinner.Gold = winner.Gold;
        if (this.GameHistoryList.filter(res => res.RoundSummaryID === data.RoundSummaryID).length === 0) {
          this.GameHistoryList.unshift(data);
          if (this.GameHistoryList.length > 10) {
            this.GameHistoryList.pop();
          }
        }
        this.CheckGameStatus();
      }
    }
  }

  private StartTimer() {
    if (this.ShowTimer) {
      this.ShowTimer = false;
      clearInterval(this.setInterval);
    }
    this.ShowTimer = true;
    this.TimerInterval();
  }

  private TimerInterval() {
    this.setInterval = setInterval(() => {
      this.countDown = Number(--this.countDown);
      if (this.countDown <= 0) {
        this.countDown = 0;
        this.GameStatus = (!this.GameWinner) ? 2 : this.GameStatus;
        this.ShowTimer = false;
        this.ShowBetSlip = false;
        this.ShowCashout = false;
        this.ShowGameStatusOverlay = true;
        if (!this.IsBetPlaced) {
          this.IsBetPlaced = true;
          this.ShowBetDelayOverlay = false;
          clearTimeout(this.setTimeout);
          this.toastr.error('Betting closed. Your bet has not been placed.');
        }
        this.ClearBet();
        clearInterval(this.setInterval);
      }
    }, 1000);
  }
  //#endregion

  //#region Add Bet,Validate Bet, Calculate Profit Loss
  AddBet(player, isBack) {
    if (this.GameRate[player][isBack] !== '' && this.GameStatus === 1) {
      this.GameBetDetails.Runner = player;
      this.GameBetDetails.IsBack = isBack;
      this.GameBetDetails.Rate = this.GameRate[player][isBack];
      this.ShowBetSlip = true;
      this.IsCashoutBet = false;
      if (this.IsOneClick) {
        this.GameBetDetails.Stake = this.DefaultChip;
        this.ValidateBet();
      } else {
        this.CalculatePL(1);
      }
    } else {
      this.toastr.error('Betting closed. You can\'t bet right now.');
    }
  }
  onCalculatePL(event) {
    this.CalculatePL(event.isCheck, event.CashOutBet);
  }
  AddStake(stake) {
    if (this.GameBetDetails.Rate && this.GameBetDetails.Rate !== '-' && this.GameStatus === 1) {
      this.GameBetDetails.Stake += +stake;
      this.CalculatePL(1);
    } else {
      this.toastr.error('You can not bet right now.');
    }
  }

  gameKeyBoardClick(key) {
    if (this.GameBetDetails.Rate && this.GameBetDetails.Rate !== '-' && this.GameStatus === 1) {
      if (!this.GameBetDetails.Stake) {
        this.GameBetDetails.Stake = 0;
      }
      if (this.GameBetDetails.Stake.toString().length < 6 || key === '<') {
        if (key === '<') {
          this.GameBetDetails.Stake = Number(this.GameBetDetails.Stake.toString().slice(0, -1));
          this.CalculatePL(1);
        } else {
          key = (this.GameBetDetails.Stake.toString().length === 5 && key === '00' ? '0' : key);
          this.GameBetDetails.Stake = Number(this.GameBetDetails.Stake.toString() + key);
        }
        this.CalculatePL(1);
      }
    } else {
      this.toastr.error('You can not bet right now.');
    }
  }

  CalculatePL(isCheck: any, CashOutBet: boolean = false) {
    // if (this.GameBalance <= 0) {
    //   this.toastr.error('You have insufficient balance.');
    //   return;
    // }
    if (isCheck === 1 && !this.IsOneClick) {
      if (this.GameBetDetails.Stake) {
        if (!CashOutBet && (this.GameDetails.MinStakePerBet !== -1 && this.GameBetDetails.Stake < this.GameDetails.MinStakePerBet)) {
          this.GameBetDetails.Stake = this.GameDetails.MinStakePerBet;
          this.toastr.error('Your stake is below minimum stake ' + this.GameDetails.MinStakePerBet);
        }
        if (!CashOutBet && (this.GameDetails.MaxStakePerBet !== -1 && this.GameBetDetails.Stake > this.GameDetails.MaxStakePerBet)) {
          this.GameBetDetails.Stake = this.GameDetails.MaxStakePerBet;
          this.toastr.error('Your stake is above maximum stake ' + this.GameDetails.MaxStakePerBet);
        }
      }
    }
    this.PlayerPL = (this.GameBetDetails.Stake * this.GameBetDetails.Rate) - this.GameBetDetails.Stake;
    // this.TempPL['7UpTempPL'] = Number(this.CalculateProfitLoss('7Up'));
    // this.TempPL['7TempPL'] = Number(this.CalculateProfitLoss('7'));
    // this.TempPL['7DownTempPL'] = Number(this.CalculateProfitLoss('7Down'));
    // this.TempPL['OddTempPL'] = Number(this.CalculateProfitLoss('Odd'));
    // this.TempPL['EvenTempPL'] = Number(this.CalculateProfitLoss('Even'));
  }

  private CalculateProfitLoss(runner: string): number {
    let pl = 0;
    if (this.GameBetDetails.IsBack && this.GameBetDetails.Runner === runner) {
      pl = ((this.GameBetDetails.Stake * this.GameBetDetails.Rate) - this.GameBetDetails.Stake);
    } else if (this.GameBetDetails.IsBack && this.GameBetDetails.Runner !== runner) {
      pl = (-1 * this.GameBetDetails.Stake);
    } else if (!this.GameBetDetails.IsBack && this.GameBetDetails.Runner === runner) {
      pl = (-1 * ((this.GameBetDetails.Stake * this.GameBetDetails.Rate) - this.GameBetDetails.Stake));
    } else if (!this.GameBetDetails.IsBack && this.GameBetDetails.Runner !== runner) {
      pl = this.GameBetDetails.Stake;
    }
    return pl;
  }

  PlaceCashOutBet() {
    this.ClearBet();
    this.ShowBetSlip = true;
    this.IsCashoutBet = true;
    Object.assign(this.GameBetDetails, this.CashoutResponce);
    this.ValidateBet(true);
  }

  PlaceEnterBet(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      this.ValidateBet();
    }
  }

  ValidateBet(CashOutBet: boolean = false) {
    // if (this.GameBalance <= 0) {
    //   this.toastr.error('You have insufficient balance.');
    // } else
    if (!this.constant.IsAllowBet()) {
      this.toastr.error('You are not allow to place bet, Contact your up line.');
    } else if (this.ShowBetDelayOverlay || this.GameStatus !== 1) {
      this.toastr.error('Betting closed. You can\'t bet right now.');
    } else if (!this.GameBetDetails.Rate || this.GameBetDetails.Rate === '-') {
      this.toastr.error('You can not bet right now.');
    } else if (this.GameDetails.MaxRateLimit !== -1 && this.GameBetDetails.Rate > this.GameDetails.MaxRateLimit) {
      this.toastr.error('You are not allowed to play with betting rate ' + this.GameBetDetails.Rate);
    } else if (!this.GameBetDetails.Stake) {
      this.toastr.error('Please enter valid stack');
    } else if (!CashOutBet && (this.GameDetails.MinStakePerBet !== -1 && this.GameBetDetails.Stake < this.GameDetails.MinStakePerBet)) {
      this.toastr.error('Your stake is below minimum stake ' + this.GameDetails.MinStakePerBet);
      this.GameBetDetails.Stake = this.GameDetails.MinStakePerBet;
      this.CalculatePL(1);
    } else if (!CashOutBet && (this.GameDetails.MaxStakePerBet !== -1 && this.GameBetDetails.Stake > this.GameDetails.MaxStakePerBet)) {
      this.toastr.error('Your stake is above maximum stake ' + this.GameDetails.MaxStakePerBet);
      this.GameBetDetails.Stake = this.GameDetails.MaxStakePerBet;
      this.CalculatePL(1);
      // } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL['7UpPL'] + this.TempPL['7UpTempPL']) > this.GameDetails.MaxProfit)) {
      //   this.toastr.error('Your profit is above your limits.');
      // } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL['7PL'] + this.TempPL['7TempPL']) > this.GameDetails.MaxProfit)) {
      //   this.toastr.error('Your profit is above your limits.');
      // } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL['7DownPL'] + this.TempPL['7DownTempPL']) > this.GameDetails.MaxProfit)) {
      //   this.toastr.error('Your profit is above your limits.');
      // } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL['OddPL'] + this.TempPL['OddTempPL']) > this.GameDetails.MaxProfit)) {
      //   this.toastr.error('Your profit is above your limits.');
      // } else if (this.GameDetails.MaxProfit !== -1 && ((this.PL['EvenPL'] + this.TempPL['EvenTempPL']) > this.GameDetails.MaxProfit)) {
      //   this.toastr.error('Your profit is above your limits.');
    } else {
      this.CalculatePL(1, CashOutBet);
      this.ShowBetDelayOverlay = true;
      this.ShowCashout = false;
      this.IsBetPlaced = false;
      this.setTimeout = setTimeout(() => {
        if (this.GameStatus === 1) {
          this.PlaceBet(CashOutBet);
        }
        clearTimeout(this.setTimeout);
      }, this.GameDetails.BetPlaceDelay * 1000);
      return;
    }
  }

  private async PlaceBet(CashOutBet: boolean = false) {
    if (!this.IsCallMade && !this.IsBetPlaced) {
      let bet = true;
      if ((this.GameBetDetails.Runner == 'SilverRock' || this.GameBetDetails.Runner == 'SilverPaper' || this.GameBetDetails.Runner == 'SilverScissors') && this.SilverBet) {
        bet = false;
      }
      if ((this.GameBetDetails.Runner == 'GoldRock' || this.GameBetDetails.Runner == 'GoldPaper' || this.GameBetDetails.Runner == 'GoldScissors') && this.GoldBet) {
        bet = false;
      }
      if (bet) {
        this.GameBetDetails.GameID = this.GameID;
        this.GameBetDetails.RoundSummaryID = this.RoundSummaryID;
        this.GameBetDetails.IsCashOutBet = CashOutBet;
        this.IsCallMade = true;
        await this.casinoService.AddUpdateBet(this.GameBetDetails).subscribe(data => {
          this.IsBetPlaced = true;
          this.IsCallMade = false;
          this.ShowBetSlip = false;
          this.ShowBetDelayOverlay = false;
          this.ClearBet();
          if (data.Status.code === this.constant.HttpResponceCode.Success) {
            if (this.constant.IsTabActive && this.constant.IsSound) {
              this.constant.betPlaceAudio.play();
            }
            if (data.Balance !== undefined && data.Balance != null) { this.GameBalance = Number(data.Balance); }
            if (data.Liability !== undefined && data.Liability != null) { this.GameLiability = (Number(data.Liability) < 0) ? Number(data.Liability) * -1 : Number(data.Liability); }
            Object.keys(this.constant.GameRunner.RPSR.Silver).map((key) => {
              this.PL[key + 'PL'] = (data[key + 'PL']) ? Number(data[key + 'PL']) : 0;
            });
            Object.keys(this.constant.GameRunner.RPSR.Gold).map((key) => {
              this.PL[key + 'PL'] = (data[key + 'PL']) ? Number(data[key + 'PL']) : 0;
            });
            if (data.BetList) {
              this.GameBetList = [];
              this.GameBetList = data.BetList;
              if (this.GameBetList.length > 0) {
                this.ShowBetList = true;
                this.GameBetList.forEach(element => {
                  if (element.Runner == "SilverRock" || element.Runner == "SilverPaper" || element.Runner == "SilverScissors") {
                    this.SilverBet = true;
                  }
                  if (element.Runner == "GoldRock" || element.Runner == "GoldPaper" || element.Runner == "GoldScissors") {
                    this.GoldBet = true;
                  }
                });
                // if (!this.IsCashoutBet && this.GameStatus === 1) {
                //   //this.GetCashOut();
                // }
              }
            }
            this.toastr.success('Bet placed successfully.');
          } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
            this.IsVerified = false;
            this.DisplayMessage = this.constant.ErrorMsg.Invalid;
          } else {
            if (data.Status.code === 1) {
              this.toastr.warning(data.Status.returnMessage);
            } else {
              this.toastr.error(data.Status.returnMessage);
            }
          }
        }, err => {
          this.IsCallMade = false;
          this.IsBetPlaced = true;
          this.ShowBetDelayOverlay = false;
          this.ShowGameStatusOverlay = false;
          this.toastr.error('Something went wrong. Please try again.');
        });
      }
    }
  }

  ClearBet() {
    this.GameBetDetails = {
      GameID: this.GameID,
      RoundSummaryID: this.RoundSummaryID,
      Rate: 0,
      Stake: 0,
      IsBack: 0,
      Runner: '',
    };
    this.CalculatePL(0);
    $('.betslip').removeClass('open');
    $('.overlay').css('display', 'none');
  }
  //#endregion

  //#region Cashout Calculation
  // async GetCashOut() {
  //   this.PlayerACashoutPL = 0;
  //   this.PlayerBCashoutPL = 0;
  // }

  getCashoutDetails(ltp) {
    let vRunnerTitle = '';
    if (this.CashoutDetails.Runner2BRate > this.CashoutDetails.Runner1BRate) {
      this.CashoutDetails.PLRunner1 = (this.CashoutDetails.Runner1PL === 0) ? 0 : this.CashoutDetails.Runner1PL;
      this.CashoutDetails.PLRunner2 = (this.CashoutDetails.Runner2PL === 0) ? 0 : this.CashoutDetails.Runner2PL;
      vRunnerTitle = this.CashoutDetails.Runner1Title;

      this.CashoutDetails.Runner1BNewRate = this.CashoutDetails.Runner1BRate;
      this.CashoutDetails.Runner2BNewRate = this.CashoutDetails.Runner2BRate;
      this.CashoutDetails.Runner1LNewRate = this.CashoutDetails.Runner1LRate;
      this.CashoutDetails.Runner2LNewRate = this.CashoutDetails.Runner2LRate;

    } else {
      this.CashoutDetails.PLRunner2 = (this.CashoutDetails.Runner1PL === 0) ? 0 : this.CashoutDetails.Runner1PL;
      this.CashoutDetails.PLRunner1 = (this.CashoutDetails.Runner2PL === 0) ? 0 : this.CashoutDetails.Runner2PL;
      vRunnerTitle = this.CashoutDetails.Runner2Title;

      this.CashoutDetails.Runner1BNewRate = this.CashoutDetails.Runner2BRate;
      this.CashoutDetails.Runner2BNewRate = this.CashoutDetails.Runner1BRate;
      this.CashoutDetails.Runner1LNewRate = this.CashoutDetails.Runner2LRate;
      this.CashoutDetails.Runner2LNewRate = this.CashoutDetails.Runner1LRate;
    }
    const odd = ltp;
    const plp1 = this.CashoutDetails.PLRunner1;
    const plp2 = this.CashoutDetails.PLRunner2;
    const size = this.CalculateSize(odd, plp1, plp2); // Stake
    const p1AfterCashout = this.CalculateP1After(size, plp1, odd); // P1
    const betType = size < 0 ? 0 : 1;
    let vrate = 0;
    if (this.CashoutDetails.Runner2LNewRate > this.CashoutDetails.Runner1LNewRate) {
      if (size > 0) {
        vrate = this.CashoutDetails.Runner1BNewRate;
      } else {
        vrate = this.CashoutDetails.Runner1LNewRate;
      }
    } else {
      if (size > 0) {
        vrate = this.CashoutDetails.Runner2BNewRate;
      } else {
        vrate = this.CashoutDetails.Runner2LNewRate;
      }
    }
    this.CashoutDetails.PLCashout = p1AfterCashout.toFixed(2);

    const vCashout = Number(p1AfterCashout) - Math.min(Number(this.CashoutDetails.Runner2PL), Number(this.CashoutDetails.Runner1PL));
    const vStake = (Number(size) > 0) ? Number(size).toFixed(2) : (Number(size) * -1).toFixed(2);
    this.CashoutStake = (Number(vCashout) > 0) ? Number(vCashout).toFixed(2) : (Number(vCashout) * -1).toFixed(2);
    this.CashoutPL = p1AfterCashout.toFixed(2);

    this.CashoutResponce = {};
    this.CashoutResponce.Stake = Math.round(Number(vStake));
    this.CashoutResponce.Runner = vRunnerTitle;
    this.CashoutResponce.IsBack = betType;
    this.CashoutResponce.Rate = Number(vrate);
    if (vCashout > 1) {
      this.ShowCashout = true;
    }
  }
  CalculateSize(ltp, p1, p2) {
    return (p2 - p1) / ltp;
  }
  CalculateP1After(size, p1, ltp) {
    return p1 + size * (ltp - 1);
  }
  //#endregion

  //#region One Click Bet
  EditOneClickChips(flag: number = 0) {
    if (flag === 0) { this.IsEdit = !this.IsEdit; }
    this.OneClickChips.Chip1 = this.GameDetails.Chip1;
    this.OneClickChips.Chip2 = this.GameDetails.Chip2;
    this.OneClickChips.Chip3 = this.GameDetails.Chip3;
    this.OneClickChips.DefaultChip = this.DefaultChip;
    if (flag === 1) { this.AddUpdateOneClickChips(); }
  }

  async AddUpdateOneClickChips() {
    if (this.OneClickChips.Chip1 === ''
      || this.OneClickChips.Chip2 === ''
      || this.OneClickChips.Chip3 === ''
      || this.OneClickChips.DefaultChip === '') {
      this.toastr.error('Please enter valid chip values.');
      return;
    }

    if (!this.constant.CheckNumberFormat(this.OneClickChips.Chip1)
      || !this.constant.CheckNumberFormat(this.OneClickChips.Chip2)
      || !this.constant.CheckNumberFormat(this.OneClickChips.Chip3)
    ) {
      this.toastr.error('Please enter valid chip values.');
      return;
    }
    if (this.OneClickChips.Chip1 === this.OneClickChips.Chip2
      || this.OneClickChips.Chip2 === this.OneClickChips.Chip3
      || this.OneClickChips.Chip3 === this.OneClickChips.Chip1) {
      this.toastr.error('Please enter different chip values.');
      return;
    }
    if (this.GameDetails.MaxStakePerBet !== -1 &&
      (this.OneClickChips.Chip1 > this.GameDetails.MaxStakePerBet
        || this.OneClickChips.Chip2 > this.GameDetails.MaxStakePerBet
        || this.OneClickChips.Chip3 > this.GameDetails.MaxStakePerBet)) {
      this.toastr.error('Please enter chip values within your stack limit.');
      return;
    }
    if ((this.OneClickChips.DefaultChip !== this.OneClickChips.Chip1)
      && (this.OneClickChips.DefaultChip !== this.OneClickChips.Chip2)
      && (this.OneClickChips.DefaultChip !== this.OneClickChips.Chip3)) {
      this.OneClickChips.DefaultChip = this.OneClickChips.Chip1;
      this.DefaultChip = this.OneClickChips.Chip1;
    }

    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.AddUpdateOneClickChips(this.OneClickChips).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          this.GameDetails.Chip1 = this.OneClickChips.Chip1;
          this.GameDetails.Chip2 = this.OneClickChips.Chip2;
          this.GameDetails.Chip3 = this.OneClickChips.Chip3;
          this.toastr.success('Chip set successfully');
          this.IsEdit = false;
        } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
          this.IsVerified = false;
          this.DisplayMessage = this.constant.ErrorMsg.Invalid;
        } else {
          this.toastr.error(data.Status.returnMessage);
          this.router.navigateByUrl('/lobby?Token=' + this.Token);
        }
      }, err => {
        this.onError(err);
      });
    }
  }
  //#endregion

  //#region Other Methods
  async CheckGameStatus() {
    await this.casinoService.CheckGameStatus().subscribe(data => {
      if (data.Status.code === this.constant.HttpResponceCode.Success) {
        if (data.IsStop) {
          this.IsVerified = false;
          if (data.Message !== '') {
            this.DisplayMessage = data.Message;
          } else {
            this.DisplayMessage = 'Currently game is in sleep mode.';
          }
        }
      } else if (data.Status.code === this.constant.HttpResponceCode.Invalid) {
        this.IsVerified = false;
        this.DisplayMessage = this.constant.ErrorMsg.Invalid;
      } else {
        this.toastr.error(data.Status.returnMessage);
        this.router.navigateByUrl('/lobby?Token=' + this.Token);
      }
    }, err => {
      this.onError(err);
    });
  }

  async Logout() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.casinoService.GameLogOut().subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          const url = new URL(window.location.href);
          const Token = url.searchParams.get('Token');
          localStorage.removeItem('Token');
          localStorage.removeItem('GameID');
          this.router.navigateByUrl('/lobby?Token=' + Token);
        } else {
          this.toastr.error(data.Status.returnMessage);
          this.router.navigateByUrl('/lobby?Token=' + this.Token);
        }
      }, err => {
        this.onError(err);
      });
    }
  }

  private onError(err) {
    this.IsCallMade = false;
    this.toastr.error('Something went wrong. Please try again.');
    this.router.navigateByUrl('/lobby?Token=' + this.Token);
  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: FocusEvent): void {
    this.constant.IsTabActive = true;
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: FocusEvent): void {
    this.constant.IsTabActive = false;
  }
  oneClick(event) {
    this.IsOneClick = event;
  }
  PlayerDes(event) {
    this.PlayerDestroy = event;
  }
  MinStake(GameDetails) {
    return Math.ceil(GameDetails?.MinStakePerBet);
  }
  MaxStake(GameDetails) {
    if (GameDetails.MaxStakePerBet == -1) {
      return 'No limit';
    } else {
      return Math.trunc(GameDetails.MaxStakePerBet);
    }
  }
  MaxProfit(GameDetails) {
    if (GameDetails.MaxProfit == -1) {
      return 'No limit';
    } else {
      return Math.trunc(GameDetails.MaxProfit);
    }
  }
  RPSWinnerResult(value) {
    const Winner = JSON.parse(value);
    return Winner.Silver.slice(0, 1) + '-' + Winner.Gold.slice(0, 1);
  }
  //#endregion
}
