import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualBlackJackClassicComponent } from './virtual-black-jack-classic.component';

describe('VirtualBlackJackClassicComponent', () => {
  let component: VirtualBlackJackClassicComponent;
  let fixture: ComponentFixture<VirtualBlackJackClassicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VirtualBlackJackClassicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualBlackJackClassicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
