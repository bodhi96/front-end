import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  constructor(private http: HttpClient) { }

  GetUserWiseGames(paramval?: any) {
    return this.http.post<any>('/api/Game/GetUserWiseGames', JSON.stringify(paramval));
  }

  GetRefreshToken(paramval?: any) {
    return this.http.post<any>('/api/Game/GetRefreshToken', JSON.stringify(paramval));
  }

  vacuumLogin(paramval) {
    return this.http.post<any>('/api/User/VacuumPlayLogin', paramval);
  }

  vacuumGameList() {
    return this.http.get<any>('/api/User/VacuumPlayGameList');
  }

  LogOut(paramval?: any) {
    return this.http.post<any>('/api/Game/LogOut', JSON.stringify(paramval));
  }
  gameDetail(gameCode) {
    return this.http.get<any>(`/api/Game/GetGameDetail/${gameCode}`);
  }
  GetRoundSummaryHistory(gameId) {
    return this.http.get<any>(`/api/Game/GetRoundSummaryHistory/${gameId}`);
  }
  GetGameRule(GameId) {
    return this.http.get<any>(`/api/Game/GetGameRoule/${GameId}`);
  }
}
