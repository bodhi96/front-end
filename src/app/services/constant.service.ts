import { Injectable } from '@angular/core';
import { BetList } from '../models/common.model';
import { GamesService } from './games.service';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  gameRule: any;
  constructor(private gameService: GamesService) { }

  GameKeyBoard: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '00', '<'];

  HttpResponceCode = {
    Success: 0,
    Warning: 1,
    Error: 2,
    Info: 3,
    Invalid: 401
  };

  ErrorMsg = {
    Invalid: 'You\'ve been idle for too long.'
  };

  GameName: any = {
    TP: 'Live Teen Patti',
    BAC: 'Live Baccarat',
    DT: 'Live Dragon Tiger',
    PK: 'Live Poker',
    AB: 'Live Andar Bahar',
    WM: 'Live Worli Matka',
    ARW: 'Live Akbar Romeo Walter',
    TP20: 'Live Teen Patti 20-20',
    PK20: 'Live Poker 20-20',
    DT20: 'Live Dragon Tiger 20-20',
    C32: 'Live 32 Cards',
    UD7: 'Live 7Up 7Down',
    PK6: 'Live Poker 6 Player',
    AB20: 'Live Andar Bahar 20-20',
    PK620: 'Live Poker 6 Player 20-20',
    BAC20: 'Live Baccarat 20-20',
    VTP: 'RNG Teen Patti',
    VDT: 'RNG Dragon Tiger',
    VUD7: 'RNG 7Up 7Down',
    VPK: 'RNG Poker',
    VWM: 'RNG Worli Matka',
    VC32: 'RNG 32 Cards',
    VTP20: 'RNG Teen Patti 20-20',
    VBAC: 'RNG Baccarat',
    VAB20: 'RNG Andar Bahar 20-20',
    VARW: 'RNG Akbar Romeo Walter',
    V3CJ: 'RNG 3 Cards Judgement',
    VBAC20: 'RNG Baccarat 2020',
    VDT20: 'RNG Dragon Tiger 2020',
    VPK20: 'RNG Poker 2020',
    V3TP: 'RNG 3 Player Teen Patti',
    VBJ: 'RNG Black Jack',
    D3CJ: 'Live 3 Cards Judgement',
    D3TP: 'Live 3 Player Teen Patti',
    BJ: 'Live Black Jack',
    VPK6: 'RNG Poker 6 Player',
    VPK620: 'RNG Poker 6 Player 20-20',
    VJKR: 'RNG Joker',
    JKR: 'Live Joker',
    VCR: 'RNG Card Race',
    CR: 'Live Card Race',
    VBJC: 'RNG Black Jack Classic',
    RPS: 'Rock Paper Scissors',
    VRPS: 'RNG Rock Paper Scissors',
    BJC: 'Black Jack Classic',
    KF: 'Kabutar Fuddi',
    VKF: 'RNG Kabutar Fuddi',
  };

  GameDetails: any = [
    { GameID: 1, GameCode: 'TP', Name: 'Teen Patti' },
    { GameID: 2, GameCode: 'BAC', Name: 'Baccarat' },
    { GameID: 3, GameCode: 'DT', Name: 'Dragon Tiger' },
    { GameID: 4, GameCode: 'PK', Name: 'Poker' },
    { GameID: 5, GameCode: 'AB', Name: 'Andar Bahar' },
    { GameID: 6, GameCode: 'WM', Name: 'Worli Matka' },
    { GameID: 7, GameCode: 'ARW', Name: 'Akbar Romeo Walter' },
    { GameID: 8, GameCode: 'TP20', Name: 'Teen Patti 20-20' },
    { GameID: 9, GameCode: 'PK20', Name: 'Poker 20-20' },
    { GameID: 10, GameCode: 'C32', Name: '32 Cards' },
    { GameID: 12, GameCode: 'PK6', Name: 'Poker 6 Player ' },
    { GameID: 11, GameCode: 'UD7', Name: '7Up 7Down' },
    { GameID: 13, GameCode: 'DT20', Name: 'Dragon Tiger 20-20' },
    { GameID: 14, GameCode: 'AB20', Name: 'Andar Bahar 20-20' },
    { GameID: 15, GameCode: 'PK620', Name: 'Poker 6 Player 20-20' },
    { GameID: 16, GameCode: 'BAC20', Name: 'Baccarat 20-20' },
    { GameID: 17, GameCode: 'VTP', Name: 'RNG Teen Patti' },
    { GameID: 18, GameCode: 'VDT', Name: 'RNG Dragon Tiger' },
    { GameID: 19, GameCode: 'VUD7', Name: 'RNG 7Up 7Down' },
    { GameID: 20, GameCode: 'VPK', Name: 'RNG Poker' },
    { GameID: 21, GameCode: 'VWM', Name: 'RNG Worli Matka' },
    { GameID: 22, GameCode: 'VC32', Name: 'RNG 32 Cards' },
    { GameID: 23, GameCode: 'VTP20', Name: 'RNG Teen Patti 20-20' },
    { GameID: 24, GameCode: 'VBAC', Name: 'RNG Baccarat' },
    { GameID: 25, GameCode: 'VAB20', Name: 'RNG Andar Bahar 20-20' },
    { GameID: 26, GameCode: 'VARW', Name: 'RNG Akbar Romeo Walter' },
    { GameID: 27, GameCode: 'V3CJ', Name: 'RNG 3 Cards Judgement' },
    { GameID: 28, GameCode: 'VBAC20', Name: 'RNG Baccarat 20-20' },
    { GameID: 29, GameCode: 'VDT20', Name: 'RNG Dragon Tiger 20-20' },
    { GameID: 30, GameCode: 'VPK20', Name: 'RNG Poker 20-20' },
    { GameID: 31, GameCode: 'V3TP', Name: 'RNG 3 Player Teen Patti' },
    { GameID: 32, GameCode: 'VBJ', Name: 'RNG Black Jack' },
    { GameID: 33, GameCode: 'D3CJ', Name: '3 Cards Judgement' },
    { GameID: 34, GameCode: 'D3TP', Name: '3 Player Teen Patti' },
    { GameID: 35, GameCode: 'BJ', Name: 'Black Jack' },
    { GameID: 36, GameCode: 'VPK6', Name: 'RNG Poker 6 Player' },
    { GameID: 37, GameCode: 'VPK620', Name: 'RNG Poker 6 Player 20-20' },
    { GameID: 38, GameCode: 'VJKR', Name: 'RNG Joker' },
    { GameID: 39, GameCode: 'JKR', Name: 'Live Joker' },
    { GameID: 40, GameCode: 'VCR', Name: 'RNG Card Race' },
    { GameID: 41, GameCode: 'CR', Name: 'Live Card Race' },
    { GameID: 42, GameCode: 'VBJC', Name: 'RNG Black Jack Classic' },
    { GameID: 43, GameCode: 'RPS', Name: 'Rock Paper Scissors' },
    { GameID: 44, GameCode: 'VRPS', Name: 'RNG Rock Paper Scissors' },
    { GameID: 45, GameCode: 'BJC', Name: 'Black Jack Classic' },
    { GameID: 46, GameCode: 'KF', Name: 'Kabutar Fuddi' },
    { GameID: 47, GameCode: 'VKF', Name: 'RNG Kabutar Fuddi' },

  ];

  GameStatus: any = {
    0: 'New Game Starting',
    1: 'Rates',
    2: 'New Card Opening',
    3: 'Winner',
    4: 'Game Over',
    5: 'Cancelled',
  };

  GameRunner: any = {
    TP: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Color: 'Color',
      Sequence: 'Sequence',
      PureSequence: 'PureSequence',
      Trio: 'Trio'
    },
    TPUI: {
      Tab1: {
        PlayerA: 'Player A',
        PlayerB: 'Player B'
      },
      Tab2: {
        // HighCard: 'HighCard',
        Pair: 'Pair',
        Color: 'Color',
        Sequence: 'Sequence',
        PureSequence: 'PureSequence',
        Trio: 'Trio'
      }
    },
    BAC: {
      PlayerA: 'Player',
      PlayerB: 'Banker',
      PlayerT: 'Tie',
      Score1T4: 'Score 1-4',
      Score5T6: 'Score 5-6',
      Score7: 'Score 7',
      Score8: 'Score 8',
      Score9: 'Score 9',
    },
    BACUI: {
      Score1T4: 'Score 1-4',
      Score5T6: 'Score 5-6',
      Score7: 'Score 7',
      Score8: 'Score 8',
      Score9: 'Score 9',
    },
    BAC20: {
      PlayerA: 'Player',
      PlayerB: 'Banker',
      PlayerT: 'Tie',
      Score1T4: 'Score 1-4',
      Score5T6: 'Score 5-6',
      Score7: 'Score 7',
      Score8: 'Score 8',
      Score9: 'Score 9',
    },
    DT: {
      PlayerA: 'Dragon',
      PlayerB: 'Tiger',
      PlayerT: 'Tie',
      DragonOdd: 'Dragon Odd',
      DragonEven: 'Dragon Even',
      DragonSuiteRed: 'Dragon Suite Red',
      DragonSuiteBlack: 'Dragon Suite Black',
      TigerOdd: 'Tiger Odd',
      TigerEven: 'Tiger Even',
      TigerSuiteRed: 'Tiger Suite Red',
      TigerSuiteBlack: 'Tiger Suite Black',
    },

    DT20: {
      PlayerA: 'Dragon',
      PlayerB: 'Tiger',
      PlayerT: 'Tie',
      DragonOdd: 'Dragon Odd',
      DragonEven: 'Dragon Even',
      DragonSuiteRed: 'Dragon Suite Red',
      DragonSuiteBlack: 'Dragon Suite Black',
      TigerOdd: 'Tiger Odd',
      TigerEven: 'Tiger Even',
      TigerSuiteRed: 'Tiger Suite Red',
      TigerSuiteBlack: 'Tiger Suite Black',
    },

    DTUI: {
      Tab1: {
        PlayerA: 'Dragon',
        PlayerB: 'Tiger',
        PlayerT: 'Tie',
      },
      Tab2Dragon: {
        DragonOdd: 'Odd',
        DragonEven: 'Even',
        DragonSuiteRed: 'Suite Red',
        DragonSuiteBlack: 'Suite Black',
      },
      Tab2Tiger: {
        TigerOdd: 'Odd',
        TigerEven: 'Even',
        TigerSuiteRed: 'Suite Red',
        TigerSuiteBlack: 'Suite Black',
      }
    },

    PK: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      FourOfAKind: 'FourOfAKind',
      Flush: 'Flush',
      FullHouse: 'FullHouse',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Straight: 'Straight',
      StraightFlush: 'StraightFlush',
      ThreeOfAKind: 'ThreeOfAKind',
      TwoPair: 'TwoPair',
    },
    PKUI: {
      FourOfAKind: 'FourOfAKind',
      Flush: 'Flush',
      FullHouse: 'FullHouse',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Straight: 'Straight',
      StraightFlush: 'StraightFlush',
      ThreeOfAKind: 'ThreeOfAKind',
      TwoPair: 'TwoPair',
    },

    AB: {
      PlayerA: 'Andar',
      PlayerB: 'Bahar',
      '7Up': '7 UP (8,9,10,J,Q,K)',
      '7': '7',
      '7Down': '7 Down (A,2,3,4,5,6)',
      JokerSuiteRed: 'Joker Red',
      JokerSuiteBlack: 'Joker Black',
      WinnerSuiteRed: 'Winner Red',
      WinnerSuiteBlack: 'Winner Black',
    },
    AB20: {
      PlayerA: 'Andar',
      PlayerB: 'Bahar',
      '7Up': '7 UP (8,9,10,J,Q,K)',
      '7': '7',
      '7Down': '7 Down (A,2,3,4,5,6)',
      JokerSuiteRed: 'Joker Red',
      JokerSuiteBlack: 'Joker Black',
      WinnerSuiteRed: 'Winner Red',
      WinnerSuiteBlack: 'Winner Black',
    },
    ABUI: {
      NotJoker:
      {
        PlayerA: 'Andar',
        PlayerB: 'Bahar'
      },
      Joker: {
        '7Up': '7 UP (8,9,10,J,Q,K)',
        '7': '7',
        '7Down': '7 Down (A,2,3,4,5,6)',
      },
      Joker20: {
        '7Up': '7 UP',
        '7': '7',
        '7Down': '7 Down',
      },
      Tab2Joker: {
        JokerSuiteRed: 'Joker Red',
        JokerSuiteBlack: 'Joker Black',
      },
      Tab2Winner: {
        WinnerSuiteRed: 'Winner Red',
        WinnerSuiteBlack: 'Winner Black',
      }
    },

    WM: {
      Single: 'Single',
      SP: 'SP',
      DP: 'DP',
      TP: 'TP',
    },

    ARW: {
      Akbar: 'Akbar',
      Romeo: 'Romeo',
      Walter: 'Walter',
      WinnerEven: 'Even',
      WinnerOdd: 'Odd',
      WinnerRedSuite: 'Red Suite',
      WinnerBlackSuite: 'Black Suite',
      Below7: 'Below 7',
      '7': '7',
      Above7: 'Above 7',
    },
    ARWR: {
      WinnerEven: 'Even',
      WinnerOdd: 'Odd',
      WinnerRedSuite: 'Red Suite',
      WinnerBlackSuite: 'Black Suite',
    },
    ARWR7: {
      Below7: 'Below 7',
      '7': '7',
      Above7: 'Above 7',
    },
    TP20: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Color: 'Color',
      Sequence: 'Sequence',
      PureSequence: 'PureSequence',
      Trio: 'Trio'
    },
    TPUI20: {
      Tab1: {
        PlayerA: 'Player A',
        PlayerB: 'Player B'
      },
      Tab2: {
        // HighCard: 'HighCard',
        Pair: 'Pair',
        Color: 'Color',
        Sequence: 'Sequence',
        PureSequence: 'PureSequence',
        Trio: 'Trio'
      }
    },

    PK20: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      FourOfAKind: 'FourOfAKind',
      Flush: 'Flush',
      FullHouse: 'FullHouse',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Straight: 'Straight',
      StraightFlush: 'StraightFlush',
      ThreeOfAKind: 'ThreeOfAKind',
      TwoPair: 'TwoPair',
    },

    C32: {
      Player8: 'Player 8',
      Player9: 'Player 9',
      Player10: 'Player 10',
      Player11: 'Player 11',
      WinnerOdd: 'Odd',
      WinnerEven: 'Even',
    },
    C32UI: {
      Player8: 'Player 8',
      Player9: 'Player 9',
      Player10: 'Player 10',
      Player11: 'Player 11',
    },
    C32R: {
      WinnerOdd: 'Odd',
      WinnerEven: 'Even',
    },
    UD7: {
      '7Up': '7 UP (8,9,10,J,Q,K)',
      '7': '7',
      '7Down': '7 Down (A,2,3,4,5,6)',
      Odd: 'Odd',
      Even: 'Even',
    },
    UD7UI: {
      Tab1: {
        '7Up': '7 UP (8,9,10,J,Q,K)',
        '7': '7',
        '7Down': '7 Down (A,2,3,4,5,6)',
      },
      Tab2: {
        Odd: 'Odd',
        Even: 'Even',
      }
    },
    PK6: {
      Player1: 'Player 1',
      Player2: 'Player 2',
      Player3: 'Player 3',
      Player4: 'Player 4',
      Player5: 'Player 5',
      Player6: 'Player 6',
    },
    PK620: {
      Player1: 'Player 1',
      Player2: 'Player 2',
      Player3: 'Player 3',
      Player4: 'Player 4',
      Player5: 'Player 5',
      Player6: 'Player 6',
    },
    VTP: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Color: 'Color',
      Sequence: 'Sequence',
      PureSequence: 'PureSequence',
      Trio: 'Trio'
    },
    VTPUI: {
      Tab1: {
        PlayerA: 'Player A',
        PlayerB: 'Player B'
      },
      Tab2: {
        // HighCard: 'HighCard',
        Pair: 'Pair',
        Color: 'Color',
        Sequence: 'Sequence',
        PureSequence: 'PureSequence',
        Trio: 'Trio'
      }
    },
    VDT: {
      PlayerA: 'Dragon',
      PlayerB: 'Tiger',
      PlayerT: 'Tie',
      DragonOdd: 'Dragon Odd',
      DragonEven: 'Dragon Even',
      DragonSuiteRed: 'Dragon Suite Red',
      DragonSuiteBlack: 'Dragon Suite Black',
      TigerOdd: 'Tiger Odd',
      TigerEven: 'Tiger Even',
      TigerSuiteRed: 'Tiger Suite Red',
      TigerSuiteBlack: 'Tiger Suite Black',
    },
    VUD7: {
      '7Up': '7 UP (8,9,10,J,Q,K)',
      '7': '7',
      '7Down': '7 Down (A,2,3,4,5,6)',
      Odd: 'Odd',
      Even: 'Even',
    },
    VUD7UI: {
      Tab1: {
        '7Up': '7 UP (8,9,10,J,Q,K)',
        '7': '7',
        '7Down': '7 Down (A,2,3,4,5,6)',
      },
      Tab2: {
        Odd: 'Odd',
        Even: 'Even',
      }
    },
    VPK: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      FourOfAKind: 'FourOfAKind',
      Flush: 'Flush',
      FullHouse: 'FullHouse',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Straight: 'Straight',
      StraightFlush: 'StraightFlush',
      ThreeOfAKind: 'ThreeOfAKind',
      TwoPair: 'TwoPair',
    },
    VWM: {
      Single: 'Single',
      SP: 'SP',
      DP: 'DP',
      TP: 'TP',
    },
    VC32: {
      Player8: 'Player 8',
      Player9: 'Player 9',
      Player10: 'Player 10',
      Player11: 'Player 11',
      WinnerOdd: 'Odd',
      WinnerEven: 'Even',
    },
    VTP20: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Color: 'Color',
      Sequence: 'Sequence',
      PureSequence: 'PureSequence',
      Trio: 'Trio'
    },
    VTPUI20: {
      Tab1: {
        PlayerA: 'Player A',
        PlayerB: 'Player B'
      },
      Tab2: {
        // HighCard: 'HighCard',
        Pair: 'Pair',
        Color: 'Color',
        Sequence: 'Sequence',
        PureSequence: 'PureSequence',
        Trio: 'Trio'
      }
    },
    VBAC: {
      PlayerA: 'Player',
      PlayerB: 'Banker',
      PlayerT: 'Tie',
      Score1T4: 'Score 1-4',
      Score5T6: 'Score 5-6',
      Score7: 'Score 7',
      Score8: 'Score 8',
      Score9: 'Score 9',
    },
    VAB20: {
      PlayerA: 'Andar',
      PlayerB: 'Bahar',
      '7Up': '7 UP (8,9,10,J,Q,K)',
      '7': '7',
      '7Down': '7 Down (A,2,3,4,5,6)',
      JokerSuiteRed: 'Joker Red',
      JokerSuiteBlack: 'Joker Black',
      WinnerSuiteRed: 'Winner Red',
      WinnerSuiteBlack: 'Winner Black',
    },
    VARW: {
      Akbar: 'Akbar',
      Romeo: 'Romeo',
      Walter: 'Walter',
      WinnerEven: 'Even',
      WinnerOdd: 'Odd',
      WinnerRedSuite: 'Red Suite',
      WinnerBlackSuite: 'Black Suite',
      Below7: 'Below 7',
      '7': '7',
      Above7: 'Above 7',
    },
    V3CJ: {
      Player: 'Player'
    },
    VBAC20: {
      PlayerA: 'Player',
      PlayerB: 'Banker',
      PlayerT: 'Tie',
      Score1T4: 'Score 1-4',
      Score5T6: 'Score 5-6',
      Score7: 'Score 7',
      Score8: 'Score 8',
      Score9: 'Score 9',
    },
    VDT20: {
      PlayerA: 'Dragon',
      PlayerB: 'Tiger',
      PlayerT: 'Tie',
      DragonOdd: 'Dragon Odd',
      DragonEven: 'Dragon Even',
      DragonSuiteRed: 'Dragon Suite Red',
      DragonSuiteBlack: 'Dragon Suite Black',
      TigerOdd: 'Tiger Odd',
      TigerEven: 'Tiger Even',
      TigerSuiteRed: 'Tiger Suite Red',
      TigerSuiteBlack: 'Tiger Suite Black',
    },
    VPK20: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      FourOfAKind: 'FourOfAKind',
      Flush: 'Flush',
      FullHouse: 'FullHouse',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Straight: 'Straight',
      StraightFlush: 'StraightFlush',
      ThreeOfAKind: 'ThreeOfAKind',
      TwoPair: 'TwoPair',
    },
    V3TP: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      PlayerC: 'Player C',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Color: 'Color',
      Sequence: 'Sequence',
      PureSequence: 'PureSequence',
      Trio: 'Trio'
    },
    V3TPUI: {
      Tab1: {
        PlayerA: 'Player A',
        PlayerB: 'Player B',
        PlayerC: 'Player C',
      },
      Tab2: {
        // HighCard: 'HighCard',
        Pair: 'Pair',
        Color: 'Color',
        Sequence: 'Sequence',
        PureSequence: 'PureSequence',
        Trio: 'Trio'
      }
    },
    VBJ: {
      PlayerA: 'PlayerA',
      PlayerB: 'PlayerB',
      PlayerC: 'PlayerC',
      PlayerD: 'PlayerD',
      Dealer: 'Dealer',
    },
    D3CJ: {
      Player: 'Player'
    },
    D3TP: {
      PlayerA: 'Player A',
      PlayerB: 'Player B',
      PlayerC: 'Player C',
      // HighCard: 'HighCard',
      Pair: 'Pair',
      Color: 'Color',
      Sequence: 'Sequence',
      PureSequence: 'PureSequence',
      Trio: 'Trio'
    },
    D3TPUI: {
      Tab1: {
        PlayerA: 'Player A',
        PlayerB: 'Player B',
        PlayerC: 'Player C',
      },
      Tab2: {
        // HighCard: 'HighCard',
        Pair: 'Pair',
        Color: 'Color',
        Sequence: 'Sequence',
        PureSequence: 'PureSequence',
        Trio: 'Trio'
      }
    },
    BJ: {
      PlayerA: 'PlayerA',
      PlayerB: 'PlayerB',
      PlayerC: 'PlayerC',
      PlayerD: 'PlayerD',
      Dealer: 'Dealer',
    },
    VPK6: {
      Player1: 'Player 1',
      Player2: 'Player 2',
      Player3: 'Player 3',
      Player4: 'Player 4',
      Player5: 'Player 5',
      Player6: 'Player 6',
    },
    VPK620: {
      Player1: 'Player 1',
      Player2: 'Player 2',
      Player3: 'Player 3',
      Player4: 'Player 4',
      Player5: 'Player 5',
      Player6: 'Player 6',
    },
    VJKR: {
      Player: 'Player',
      NotNumber: 'Not Number',
      Number: 'Number',
      Red: 'Red',
      Black: 'Black',
      Spade: 'Spade',
      Club: 'Club',
      Diamond: 'Diamond',
      Heart: 'Heart',
    },
    VJKRUI: {
      Player1: 'A',
      Player2: '2',
      Player3: '3',
      Player4: '4',
      Player5: '5',
      Player6: '6',
      Player7: '7',
      Player8: '8',
      Player9: '9',
      Player10: '10',
      Player11: 'J',
      Player12: 'Q',
      Player13: 'K',
      Player14: 'JOKER',
    },
    VJKRR: {
      NotNumber: 'Not Number',
      Number: 'Number',
      Red: 'Red',
      Black: 'Black',
      Spade: 'Spade',
      Club: 'Club',
      Diamond: 'Diamond',
      Heart: 'Heart',
    },
    JKR: {
      Player: 'Player',
      NotNumber: 'Not Number',
      Number: 'Number',
      Red: 'Red',
      Black: 'Black',
      Spade: 'Spade',
      Club: 'Club',
      Diamond: 'Diamond',
      Heart: 'Heart',
    },
    JKRUI: {
      Player1: 'A',
      Player2: '2',
      Player3: '3',
      Player4: '4',
      Player5: '5',
      Player6: '6',
      Player7: '7',
      Player8: '8',
      Player9: '9',
      Player10: '10',
      Player11: 'J',
      Player12: 'Q',
      Player13: 'K',
      Player14: 'JOKER',
    },
    JKRR: {
      NotNumber: 'Not Number',
      Number: 'Number',
      Red: 'Red',
      Black: 'Black',
      Spade: 'Spade',
      Diamond: 'Diamond',
      Heart: 'Heart',
      Club: 'Club',
    },
    VCR: {
      Spade: "Spade",
      Club: "Club",
      Diamond: "Diamond",
      Heart: "Heart",
      Red: "Red",
      Black: "Black",
      Odd: "Odd",
      Even: "Even"
    },
    VCRMR: {
      Spade: "Spade",
      Club: "Club",
      Diamond: "Diamond",
      Heart: "Heart"
    },
    VCRSR: {
      Red: "Red",
      Black: "Black",
      Odd: "Odd",
      Even: "Even"
    },
    CR: {
      Spade: "Spade",
      Club: "Club",
      Diamond: "Diamond",
      Heart: "Heart",
      Red: "Red",
      Black: "Black",
      Odd: "Odd",
      Even: "Even"
    },
    CRMR: {
      Spade: "Spade",
      Club: "Club",
      Diamond: "Diamond",
      Heart: "Heart"
    },
    CRSR: {
      Red: "Red",
      Black: "Black",
      Odd: "Odd",
      Even: "Even"
    },
    VBJC: {
      Player: 'Player',
      Dealer: 'Dealer',
    },
    RPS: {
      SilverRock: "Rock (Silver)",
      SilverPaper: "Paper (Silver)",
      SilverScissors: "Scissors (Silver)",
      GoldRock: "Rock (Gold)",
      GoldPaper: "Paper (Gold)",
      GoldScissors: "Scissors (Gold)",
    },
    RPSR: {
      Silver: {
        SilverRock: "Rock",
        SilverPaper: "Paper",
        SilverScissors: "Scissors",
      },
      Gold: {
        GoldRock: "Rock",
        GoldPaper: "Paper",
        GoldScissors: "Scissors",
      }
    },
    VRPS: {
      SilverRock: "Rock (Silver)",
      SilverPaper: "Paper (Silver)",
      SilverScissors: "Scissors (Silver)",
      GoldRock: "Rock (Gold)",
      GoldPaper: "Paper (Gold)",
      GoldScissors: "Scissors (Gold)",
    },
    VRPSR: {
      Silver: {
        SilverRock: "Rock",
        SilverPaper: "Paper",
        SilverScissors: "Scissors",
      },
      Gold: {
        GoldRock: "Rock",
        GoldPaper: "Paper",
        GoldScissors: "Scissors",
      }
    },
    BJC: {
      Player: 'Player',
      Dealer: 'Dealer',
    },
    KF: {
      Umbrella: 'Umbrella',
      Ball: 'Ball',
      Sun: 'Sun',
      Lamp: 'Lamp',
      Cow: 'Cow',
      Bucket: 'Bucket',
      Kite: 'Kite',
      SpinningTop: 'Spinning Top',
      Rose: 'Rose',
      Butterfly: 'Butterfly',
      Pigeon: 'Pigeon',
      Rabbit: 'Rabbit'
    },
    VKF: {
      Umbrella: 'Umbrella',
      Ball: 'Ball',
      Sun: 'Sun',
      Lamp: 'Lamp',
      Cow: 'Cow',
      Bucket: 'Bucket',
      Kite: 'Kite',
      SpinningTop: 'Spinning Top',
      Rose: 'Rose',
      Butterfly: 'Butterfly',
      Pigeon: 'Pigeon',
      Rabbit: 'Rabbit'
    },
  };
// assets/images/ajax-loader.gif
  bgColor = ['dark-blue', 'dark-green', 'dark-red', 'dark-gray', 'dark-orange-cap', 'dark-blue', 'dark-green', 'dark-red', 'dark-gray','dark-blue', 'dark-green', 'dark-red', 'dark-gray', 'dark-orange-cap', 'dark-blue', 'dark-green', 'dark-red', 'dark-gray'];
  KFIcon = [
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif',
    'assets/images/ajax-loader.gif'
    ];

  GameChips: any;
  chipsSetting: any;
  defaultChipsSetting = [
    // { name: '5', value: '5' },
    // { name: '10', value: '10' },
    // { name: '25', value: '25' },
    // { name: '50', value: '50' },
    // { name: '100', value: '100' },
    // { name: '250', value: '250' },
    { name: '500', value: '500' },
    { name: '1000', value: '1000' },
    { name: '5000', value: '5000' },
    { name: '10K', value: '10000' },
    { name: '50K', value: '50000' },
    { name: '100K', value: '100000' },
    // { name: '500K', value: '500000' },
    // { name: '5M', value: '5000000' },
  ];

  IsSound = false;
  IsTabActive = true;

  beepAudio = new Audio('assets/audio/beep.mp3');
  noBetAudio = new Audio('assets/audio/nobet.mp3');
  betPlaceAudio = new Audio('assets/audio/betplace.mp3');

  CardImages: any = {
    '': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'undefined': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    //'Back': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAYAAADkUTU1AAAGEUlEQVR4Xu1cXUwcVRg9dxa1Ysu/SLEW2WUXKg/W6KMv1hejPpiYVDEoVYk2WiWV1tjYqFH6k1poNLZRgpZoYw2JT5qYmNQ++eBTTWotsixCKARp+SutpoWZa757Z2Zn6bI/zO7qDvcmZGcnc+d+5zvnO/fOnbAMAGo2beH06fU2cv4nxlYLWItMBdjrso4yzACsgkpWkl49kvY6UhOfkrTXiVYMK4Y9lgElaY8RegMcxbBi2GMZUJL2GKHKtJSklaQ9lgElaY8RqlxaSVpJ2mMZUJK2CD23MxyfW8bANAZoAHzWMQMYnaPvEJ8Ne+9ypY3+D0YBzsENAAYXf9z8lN8hv/P474cajwTjjr8sw4YBnG9fAprePwlQEpw49pnHdiJkMhhjqN9z54pA9x8Yk0CWAtNjQQvA+o2AlwNLwSSUdNW6BZxqHY4G7QRoATcB20kQwGVi6t+sThsw4Rw4NG6ySwxKhjkBW8q0MwHmSA2dQfho/GVa0houXqOLcSgQDibGJ5mR0uSxeU6qDP0HxqPsM4ZQe1VaoAc6J8Btdjka9lQLMZG4xJ9GUciKosTK89y+Zu6aL+F4SQFTb1HPFruibjVHDdOxlLGmMWx+Zz3O7P/Llr3/tTtQkDgGO8DCmzl+PTRp12tg53pEPhyLqd0Ypk3W6ZzGDWzqCiVNbkqA6S6/745IYJZROT6ddR3aVY3wRxMxJhbcUZk0ELpg4BMCGzWp2rYqDHWORw3LAmjWrlXDBLjxcCClMVIGLEC/NSRNiorEUcN2/ZpJCL1RhYGPJ203P/LdbTj2Q2HCgE69P42NFbrtvqHXKzHQNSHZddaqODakWZm1fc9Bf0pgk5pWvLtQ3VLdJGuDxyapsqT0NYbgS+UJu4S7p2xTuvI3cN+u25MNITwklVicN0qLYepIg/S/NyLreKmsLcf2MZCMw59esmtZK2DwbyuNCyJyfMZ2YWI0uL1CytvBonBOi2ndENc/sH8Dri6kaBDmyGkDpn66AYT3jToAaxKYkLucp7kPeHhvGU4fnLNdu25bSVzAg72zNru1z5fh9L5p1JTrtpwlUCljS+JnRm5CU096M8CKJG1F3LX1Ih7dfA2M6tlpZIJleS74cjkGv5iJWawEmotiQEdOXI4xpboXSxH+bMoEZ9aqAyjXDeHIobc3JpV8vAtWxHCikcJHL8a4eV1rGSJfWiwD/qYisQqldn0RGO0jwCQbjkBLMQZ7pqPMkrxfTV7L6SDPOOB3n5pH80Mm8xpDTUspLvTNweDRZah/61oR41DfFdtpF3WOuuZi/Hl8BjBkjQ5c0PBYR1k6eJJem3HANCJJ0qppmsYCLSWIfD1vTmkMW3bciuZHFvHC4wtSurqBwDNFiPTORqch3RDmlemWFcC0Moz0TMva9jH0j/nQdrQQP3b9E613sSyVzrvhybX4vmMe994dNapAa5kt/UyCzgpgCjDSMwVOi10TdODZYgx9M287tgBhPgz4n16HyFdzcmrSORg3EGhNPG+vNAlZA0wBkQHRqsx6hCSHFqAt1+IcAiw5tWPqIaPLVssq4FsKOM51z9rTkp9YPnnZXh/RU1FtUxGGT8zZDwj1rSXQjXTXT6mnJ6uAbZatzQKNIfBcsZAvNZI5TVnOnYxssutq4ZF6Tk1pi4dahsPfrsHZYR9+PluA3t1X8WDjor27kW2wOQP8S+cUyousp3gGWk0J9j+fsXYW8NuID090FKeTxxVdm3VJW1GJuVlsTzCxQyH3LQiv3MahZWguWs4A09o/0n1JYnK4NIGt216RlTk3XgJzBliswGidLQCboXDCzlH3Smo7IplQQE4BU8C0E+IETDsbuWw5B3x/7XWcbJsRGBvbK7GgZ2/O/c8lbQXwR+eEOKxPcws3E0rIOcOZCNrNPRRgN9nLh76K4XxgyU2MimE32cuHvorhfGDJTYyKYTfZy4e+iuF8YMlNjIphN9nLh76K4XxgyU2MimE32cuHvorhfGDJTYyKYTfZy4e+iuF8YMlNjIphN9n7X/el99D0btr6TTx6MU3/mOH1piStGPZYBkTRrpbftqQf8vwXmssHoA8bAY0AAAAASUVORK5CYII=',
    'Back': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',

    'JK': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAxCAMAAABj7DDGAAACWFBMVEX3+fn19fXz8/Tt7Ovw8fH////59/j6+/v6/f77Ji77NT39nKD9+Pj9sbT7LDT+8/P/5uf9qq0AZpH8d3z7QEf+3N38Zmzynyz8Ymf8S1H8WF78Iirs9fiuztz0KTD/7e394OH+t7r9iI3qnC/s8PLD2uLb3Nr5r7I/V3eCRF38UVipjUrdNEDbLDnsMTTY6fDj6+7q5+CAscb8u71FiaNrmKEKZ4sYa4X7bHFReW5rSmalUGDg5Ojd4OLM2N22ytb50dL6zM6quMKPrLpworhXk6uliZkXcpivrpMcYIRaVnJ5Vm+yWmZ7R2HjSFOXP1PZQk/FP0zSOUa4NEPEM0LOMkDOlzzfmjX34+XT3uDgztPVxs7Bzs3Lz8rKxMmbuMXFy8OSscOAq7q3v7bRxaynoKuNp6W3laNXf5phj5f8j5Mmc5K+h5Dqho0NYYr7f4QpXH87dXmHY3n4cnjba3WoX3GEWGzis2twgmbqWGGQRFuullrHTVqIhliXiFOlO0/oQ0y1OkzuO0TqpUPGlD7kNz7eXTfegTbj8PT58PH06eu21OCtxtLxzNDW18+2wM2YvMyKuczh28v9xsn7wcTkusDIrrnlrLTApLDmoqmvtKifrqSDnZrZkZr7lZmbiZOhpY5Zh45GcI6YeY2kdYjWuYZ1bYQpbH7Kcn2QcHVvXXWHj27pYmtbbmrGWmh+SlSqcE2nRUvcdDfjkDPj2NvFuMPYtLzXqbHunKI4fJpkdZBJgYq5qYR9j4BXZH/rdn48ZnSZVmqTh2nSbFqMXlb0TlbFdT/YmT3iAxnlAAAABXRSTlP7/fr+rGSPfHEAAAQtSURBVDjLhdWFd9pAHAfwQHvkEkKABAgthRYoDIrU3V1Xl7nW3b2rt3N3d3d3l39rl9D20bG9fUPI4+5DuPsd74L5+4nFYpEI841IJEbxx1CvZCU4jktWRyTGhHZ08hcBeHuIPmPocrwrQ6LUJMjwTPY4VLIZmSzL9kmlLNslk+M4EkCqMyppkqaNa3TSEFXAujU6mqYTtDqaoKUQxyAEUkKmJY/J6AQlIW0lpHAN0ZWRodaQRhkZSEEMAl4EBsjVpFZJsKqwdUhIIaQ0ZIg8IEwOMLBKkLQS8gLH/yUIOhoAGRGgMck1ZLSM1FA+QsdSSNCkVq7hx2sEfwqVSqVGwhSioDR0gopUe4k9grho0kmRkAKAxqEwEUv3aEVCFZJJa9BsM+kwORIUxQsjYeLFMZZUhZiIwADdRVQxSktEywiVNrBVSyrUZCA/FykdYASKPpJkg6NVSqBUmaLRaEiWDVNQCWEhSASrgwH6WbWaAnKFHFCKYLkCJThYQQF0IvGfeAuIDvRaDrXBz1t4uaCIlIhQCQAHqi79Tew+lDdQkvyl91rToaw7W3wFln69ssZuK+zsLPz84MN64COwrIeGqm6GiU+ccIwVpAJfkf5QV13IxA3XcnXvrTjwFeF5BreNidvFceaC0L/OVl+iG2WYYY7jfm0GPgKGRkZmVxpsTHwtx9WN7YjKyYnah3kJv/qh2dmvzupOJpEzj7U0F09WVZUV5VpWRORQrcNR56gsY5gJ8703xS7C4HYbDJNXLUsCuzFxq7uwZ1eFHY1zuG2UMCwsPnu2uOCebA71iKP3bNOumqSeZDuTf67NTrifmjlz7Vzd4nzRfo+ofzCdZEuqvtswmq+H6eWGpxw3lxgf3+NYWLtDEHjBo6SYw0feXsdO7kcLt/GbmZu7lZ+Tro/9uSwul31MQwWLsgQBqLcEHZ01JzJbcQAsj4u2CgIUPLnJ1xDT60GQJWofPuSIi+EbNj2+muoR2570RghF3Y6B3fpwcCORyU4JAmDn92zKIyJKBvmVDno11UEBDMLYuKL7J9LWg/o7KUAQ4tSSmxG8ODk1snfDCxie313t6ss6CyIbJB7RPnhpk1DejU2nrw1uCM9LstXU9GddpiS320WCeL6Fp2DPif7kvKbTR5rLk3tLS1/mnsP3jj9K8Vp9eGZmPtl5u32g3LW2YurTutydAKRa4bKAaHn750sHxp1Od4W9zMWmbW/ZtvL/gADiKNb7MzPjxWvv2ounf1w4kBt/Fnq+i4QAJHjaBc2I01VZ4SwtP3V4e8t5q9CNDjR9KOyz1saYho7XHSNnNp7a2nZlM+65A4ogEBBJNr2LzYnJ3hIVE3tlm58ECgG4sOPighCJIq2N53c2xm4+eFCCe0XY1xFA4d/RTr/cLVkKhjpWxffZ4C9GEfk+YFCTmI//b8WI5btMb7PnAAAAAElFTkSuQmCC',
    'JKB': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAxCAMAAABj7DDGAAAA/FBMVEX39/jz8/P19fb5+fnw8fHx8vLw8PHv7/Du7+////8AAAD9/v73+Pj7+/z29vf5+fn6+vr09PX8/f0GBgYLCwuQkJAiIiIWFhZ9fX0/Pz8PDw/Nzc06Ozvr6+ubm5syMjLw8PDPz8+UlJRVVVVSUlIRERGXl5iJiory8vLk5OTb29vY2dnV1dVnZ2dBQUEtLS0oKCjt7e7o6enh4eGdnZ26u7uqqqqAgIB4eHhiYmJMTExDQ0MfHx8ZGRnm5ubS0tLFxcW2t7d0dHQ2NzfJycnAwMCxsbGurq6jo6OhoaGgoKCLi4ttbW1bW1ze3t6mp6eGhoZGRkazs7OBgYHld7qaAAAACXRSTlP5+vr596ysrKwlEFl2AAACyklEQVQ4y72V15aiQBBAW3c2dEEjUQQMKOiYcw5jmpxn9///ZauREXX2+Lj3oQ92XcqqPlpNfl3EYrF4nCDfIggSj2Pk4ieJKYokIgmOLMvBkxgiSUqMSLiDu8wtMznBn/ENDPBFlFVZlIgsq2/eTdlozkrS2h/Khj8YeJ7np1K+91LoMJkwRsfwaICuadkCjPLmvFYC0KC5BWTEGFFVmoJC2r4rwIcBqWf4I5Tg5WawsvRsQd+6AlEFNIxcspvX05cwNdtVWoIUFRTLrmeS7QwlQmhcBYYGlwxzjCWpbNnL0KCHBsAbpVlINhvdpv5Y0i2FEhoa3dDwRDQ0PV2ZYUKtTyPjyuHGoli8RmPi1MtNmJm2szNGkbFpwBiNFKVS0843IBsY6jMUcuZyrVmX8D7Q2t0+Vqq42Asm48adb5tOA1pJ2OCJuWl47MMivW2k7bqjtzJobLRkn1anuu1VHswnapiT/sI0i/60XXOb7SUavVUPa3GvnTLt1DJCufbaqdWq1UqlmhAqVd7tHkHgCz2FhCFcI8I3IoNz6JzmOIYL5w3kPxm9fv68wV4g1z1niOs5tCpnjLyn+8bqzLcM56ANzlV6awKY9X8ZiUyH8S5aAFDMnxjsvvG7MG21/HuXvgOiDU+M6gLmlsYjvmMBZyJgR5KwN7qWtikPk4C0beAsnPr4w5oMO5Tt6ujduFiiDRHbwNesJ89I7HtRG/AFUwezFnW7KsIx+nN9ZE8zkaHMjoVillFpmTk8sdSx8fT1TO+0Q2HmfjWco0Ie6OcvnggcFSdRLwkR89f9f4QbLKCTgwhLEUK4ocoyH7ai0SruStGKuexeUHEWosGnryhWrm8fSoXS/drJqAgPI4QhGFcQMSHjBxVhB+BMZjyFEiDxVCgijMk7yG7QK5+ggTVx+D6HxBXpk/0dEQaDGyBGvgf3C0KO4Dsx5OLHXwDGc5qBpHlWAAAAAElFTkSuQmCC',
    'JKR': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAxCAMAAABj7DDGAAABTVBMVEX39/jy8/P19fb5+fnw8fLw8PHx8vL////7Ji79/f77JCz9///4+Pn19fb39/j6+vr7/Pz5+fr7Iir8/f38S1L7NT38Zmz7Lzf5/Pz7KTH9naH8QEf7OD/7NDv/+vr8Ymj8V1389/j98fL83d7+1tj7///99PT/7+/95uf82tz+t7r8tLf7srX7qa39iI36dnv8UVj7ICj7+vv+5OX8urz+sLP7r7P6nKD7Q0r7O0L7KzP5/f3//Pz/9/j/6+z+3d750dL8wMP5rrH7rK/8mZ38gYX8eH38bnP7am/8YGf8W2H7LDT59/f84eP+4eL8vcD7oKT9en/8cnf8XWP3+/z/9PX98vP77/D/6On45uj9zM/5y839xsj8rrH9oaX8lJj8j5T9jJH8TlX7SE777e79q676pqr6pKf8X2X7PkX/5eb+z9H7qKv9l5v7PUR3nRo1AAAAB3RSTlP6+vr596ystdDdEgAAAyJJREFUOMutlVeT2jAURgUptiWwwd3YwNKXEnpoS12Wvr1lW7amt///GEmQGJIMTzkzFjP3Hj5fazwyePHc4XA4nQDzzAZgnE7cef4CONxuN8+7XC4vQRBcC3i6uN0OgLukrO/u6gIGF/k5Lq+g614XD/Bv76i721fHGeE619P7ue51Lpc7rlZzw0ElLgig6IntiOX+zK9p6UqhGjGfLrdFQ5uNJ4UGaxxKHiBJMR+bCVo3FWPUh9V79nBrGw663WTQn84oJyEJSBzysW835HBECb5ih+bjJTaqsS1Btfbicv6cA9yyYSjGR32rAnc8QmhuxBHgEONjX//KgLODWCzDyuOHsOovp/1qAAGGWTHEIwllWM0fDKtQa2gphvltvEkS4+lHPYIyMBsphdTGyKx3Vo3JN3iXFXew4UMooFr7WXaRcc+mp+betaa+godXWj6cxpMGWqpVSsEsMW5ylhnJwg0Z3m2L1dCELadhbXLyEFT2O8rGGTa+a3IKbR4r1vBd2XwV63/Olmvm5/pwmC+1xvl9bLSjbQahUDIZ4uKlcy602Y5vYhKJiyaTuAjQORCiC8dx9CIwNsTgOIasNvMWso0FaNG3A2xj1WLWGTR7nUH5P0bpbXS90TwW8+11RqBnFuSLNUb0WBvdXq25S6omzm7WTXpgQWh1/mUEEokAeQpZZKES/cNovh70blX5cfQ6zuxAlmWN1B/Gfr1gTmciFI1xdCpiQzwioS3baE8b2bNUDf8bPvlJBqxHOw+Tk6N0mGnO5/hydYZHJE2IL6Js1ESMceIb3QbsZxmQLoWYdKlrBatjG19xyDJQGyTv/GrCNlqfxBXB/77JtJKJ5R27XzEM3997WjaWBPHTOVo2ECGqwKWICuKYeQtwC2KbNduA5h4iLz3CKzE8RQ/mTb4AfwmFaYibQw1JoGft7qGsNOhWNJTH978FCUiSVPTylMvuwfaHjx8qveg7XKVtDMD5Au+m8Pzp6amu61u6ZwkgCEWBpwqFnvACplgU5gBy4ruIYSvUEHCZwAMnSacGT6BVL+3Oq07wkn5fMGAFUnHQ78tPu3SMG7fcxnoAAAAASUVORK5CYII=',

    '2S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAulBMVEX+/v7////+/f3q6urr6+v///8AAADDw8T7+/v29vbk5OTS0tLPz9BISEgfHx8NDQ4GBgf39/fv7u/X19impqY+Pj8YGBkVFRX9/f3h4eHe3t7d3d3c3NvV1dXCwsK8vL21tbWZmJmDg4RkY2Q4ODgnJygiIiIQEBH4+Pjy8vLMy8y/v7+fn5+RkJGIiIgtLS7p6enGxsaxsbGvr7Curq6rqquSkpKLi4t7e3x4eHhzcnNcXF1TU1RCQkMOWWCJAAAABXRSTlPy9fGXnUWfCdoAAAFDSURBVDjLpdXHcoMwFEBRHEc3EDDgArj3XtN7/v+3Yg+JM1LgscjdvM0ZLfQ0I6t8YUldlC1VlFVSWi2vq7RKlnbIYArM99oRmhiS9p4nViHf3eeIMYSN1sCHKEfMYXQcfYibmaIG1NIZOJmiWu92TrMLia0JsxFMlSh82IviBVhJog2MlSQO4CtJ7ABPEjfAREkigoUtiQYwVIJwQqgoSXxArCTxAIyd61N2tpgBCQC08t7HuU628AuFd3VuqYnM/iVqReI2GciiHRDUJVFfAIGbL+5CAHjME6/89JYtKvz22TSECSD6K0bo7UzRjNFb2IaoY+YaoofZ2hB2QFqFtLBqCNUAgEl7AgBbZYgjOUDyrKb2Uwyzbead9jxH9RmqpdcTtl8hkre/BvqSqM4A39FFSRMb13U3mihZqqj0BxK6/AICKSs4CLCDmwAAAABJRU5ErkJggg==',
    '3S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAyVBMVEX+/v7////+/f3q6ur////s6+vp6en///8AAADS0tL29vb8/Pze3t5ISEkwMDErKywXFxgGBgf4+Pjm5ubi4uLX19jV1dXHxsfDw8O7u7xra2xDQ0MfHx8NDQ4CAgPw8PDa2tq/v7+qqqqlpaaRkZE/Pz8oKCkkJCTPz8/Ozc2xsbGfn5+ZmZqNjY1oaGhOTk9AQEA9PT4UFBQRERHp6enp6OnKysu3t7eysrOLi4t5eXp4eHhzcnNjYmNfX19bW1xSUlM4ODkKCgr7yqRPAAAAB3RSTlPy9PGa9Z+UPZuSOQAAAVtJREFUOMul1elugkAUhmG0y7wIKu4ruO9LrXbf2/u/qGKIaWeEY5N+f4DMk8k5cyBYl2lLSvrMUqdinevPt3ZXaUlZ2ibdbAEmK20LTbjh+j7NRJEDAsLUEkQDWJZrY3hPEK1o/zrkEsR6s9k3Uoa8LrQIQivnQxClSqMNNUFcAduhEsQ10Aqv8h6MHEHcuPUcPMm9lMCThRpDOVY4v6qdxYpSexLNIw92rCgC8/DqBgSd+DqaUFj1Bu3k2ZY4pJ/UywPyWxim+jbxvVZfPHXVKSolijD/F51TorZtyML1+apIorIDfDtZzKPvNugniWcOeYkXWX7SMoUJYHQslugZmsLxDLFzDFHBjG2IKmZmhij6RDkUXMiYlQ6ihVd3Hd0sjrsd5MF7VJ/O0IP8IvZMq9OMqnOvitOqMP0sI3n6d0BdEpkx0M7oIqWJnm3bPU2k/vD3OEtbYi6+AWGhN+xJXNicAAAAAElFTkSuQmCC',
    '4S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAz1BMVEX+/v7////+/f3q6urs6+vp6en///8AAADLy8tkZGROTk729vbT0tMSEhIMDA39/f319fXY2NnQ0NCBgIFISEg3Nzjh4eHV1dW0tLSxsbGbm5yRkZGLi4t5eXk8PD0vLi8oKCkeHh76+/vq6ure3t7b29vW1ta/v7+8vLyzs7Otra6sq6ympqefn591dXZycXJpaWlgYGBdXV1MTE1DQ0M/Pz8ZGRoXFxcHBwf4+Pjw8PDt7e3NzMzGxsbCwsLBwsK3t7eop6haWlpSUlI0NDVkgAZ0AAAABnRSTlPy9fGan5R5ze3CAAABRElEQVQ4y6XV127CMBSA4QCt/1CSMEKg7D0KZc/u+f7PVJQK0bjJoVL/S+uTdWxf2LiMG1LxhKHOZcS0hZRp5tSPYoa+yQJqgS10kQE6oijDSomiAUNRFGGrRJEEVxRpBwqieIO6EoUHI1UwzfsokQVbqSWUosQU5kotBGGD5Z/nOkKMoKFEUYeyKArQUodq0A8XU2hWXdf9hPdxqPjglBcq1pyqh4rK6sqvBV41IPRmYCpRLOFGFsmzotPrZUTh9z/xcE482UNZdB2crCSyNuBY0aK0x28cJWocm4WLPKfWaU3oADa/RZlgFV2kt5qw25rIopfTRBG9R020Hb47DrxP6ZPe4pfszvGb6JMeSAN2VeWlKztoTkLvtJ9LqRde1V2uKLx+no38+ibwLIlUE2hlgiIWEAPLsgYBEfvD75GIG2IXX06sMqb7tl5JAAAAAElFTkSuQmCC',
    '5S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAw1BMVEX+/v7////+/f3q6ur////s6+v///8AAADj4+LMzMwGBgaAgIDS0tKlpaYXFxj7+/v19fXq6uoMDA1AQEAkJCUfHx8DAwP39/fl5eXY2NnV1dbLy8vDw8O3t7ewsLCenp6SkpKPj49zc3NnZ2dHR0g3NzcSEhL4+Pje3t7T09PPz8/Hx8e/v7+8vLyampuLi4uFhYR6enp4eHhXV1fs7Ozn5+erqquqqaqWlpZqampjYmNfXl5eXl5JSUpCQkMsLC0qKir5tIeuAAAABnRSTlPy9PGY9Z9P6Uq5AAABSUlEQVQ4y6XV13KCQBiGYdSEDzc0QbE3EOwtpvf7v6rsCBFY8i8HvifLMM8sS5tValVFVrWmqGUpN+lxp1+P6zfSkxUlM8k9/ppkpsiK40VMCXF3ETta+No5mxLp9LQ4lIoXzRh1pKLJAHaUiCRfKkzwdEK8Amxr1bkJCDH6eF/z4RkIBSH0CDTlosHQulp4pAhOpzkfNHodJmDwYQ+4hJgCjrEce8CBEDZDUoNa6TABhkrey8Bl8MKhSgpeZ6lRX1Da9eKhTKyjrVwsHDhdmehGABydFqP4oXpPlEj/ff9/MUPalyhEALhFsUO+iSgsUxCRJYguxHRB9AqiLl7FQdxnMrK2uFID594WQXwwLt6t8Q2YezW0Jibww4EoeL2VrQ4wVOernuTtz+DK334fwEAm2k0uWnZeVHJio+v6Jicq5buHfAfi3f4Cr7cx42jBOcYAAAAASUVORK5CYII=',
    '6S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAzFBMVEX+/v7////+/f3q6ur////s6+vp6en///8AAAD29vbS0tJHR0jy8vLNzc0XFxj8/PzY2NnCwsOSkpJhYGEODQ4ICAgEBATe3t7V1dXGxsa/v7+xsbGenp9KSksxMTEoKCggICAUFBUQEBH4+Pjj4+Tg4OHW1tbPz8/Jycm5uLmsq6ympqeampqMjI2Dg4N3d3dwcHBPT09CQkM+Pj42NjcrKysjIyTu7u7t7e3o6Onc29vKysu7u7y0tLSkpKSgoKCPj49nZ2dbW1xAQEG6eNIlAAAAB3RSTlPy9PGa9Z+UPZuSOQAAAU1JREFUOMul1ddOw0AQQFEnAfa6pvfeC+mN3v//n7BkIfCSnSBxX+blaB48lta6SlpSyZSlzmVdqHie7/vqRwlLX7KDVWyFLkbAjSjykFeS8ICyKJawVaIIoCaKOhyVKFpQFYUD3fqzL4h3KAKbtEnYJaKaJjED6ABkDWINQUVNupA3CBca4WjA3iBakAnHEI4G0YZhOO6gY95xH44BtA1iA8twLMA1iDnwWs0BOYOwi0SVPINQU6KezLedNHsU3Kz4n3oDT7p+1P/F7TlR7T7KolygmJFEpgcUHLOoFwEozUxixVfr08Llu60udAC732JOvJwu0oEmemlNZNBzNFFBz9dEv0BUk6iSrQl1DQD58hsANJQmQnKAYKH26VwAh5eT33Q0tlWWqeqPK8L1XT7k6z8AWUnYbaBjx0UiJmqO49RiIvGH1yOVtMQuPwGWJzd0dh7TDgAAAABJRU5ErkJggg==',
    '7S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAtFBMVEX+/v7////q6ur+/f3////9/f3r6+v///8AAADIyMnS0tJISEn29vYuLi/o6Ojx8fHt7e3Y2NnW1tbAwMCqqqqmpqdvb28ODg4ICAkDAwT9/f3i4uLd3d2ysrKvr692dnZycXJCQkM5OTo1NTYYGBj6+/vPz8/My8zCwsK7u7yfn5+SkpJlZWZiYmI/P0ArKysTExP4+Pjh4eHf39/Gxsa3t7eampuLi4s0NDQmJicfHx8WFhcyvMb/AAAAB3RSTlPy9Jfw9fGd/dD5PgAAATRJREFUOMut1ddugzAUgGGSDv8lZDACZO/VzO71/u9VVRYC3Ppw0//Kwp/MQb7AqV85Ujd1R1XlXOdr/y4ryB/WnMIhc7KahSOKYkbWu0U09HYKC7uIBo+DKRztb3E17FrEAnZKtXvQsgg3DNtKjbJPyYXRBGJZzCGShB5DEnoMUUwhlkUTxqJoAR1RRHoMQcQQS0KPIQk9hii6sFCiOMBEFksYi2IIaUcU7adBSwlC9w+iUyWeLztZDBNSXxL+BUhcuxil/NSLbGJC1vRv0SBv1jeECeD0W7xSbm+K/tIQycoQPmauIQLMNoZYJeiygXueOWlXbxyHB724V7nIyBm+3tS8v1/CWYNc6IK1p7a8qId1INx+g5N8+xtgKwnvA/j0yqJWEqHrumFJ1BxVVdUfyLn9BjuMNRgBAnXkAAAAAElFTkSuQmCC',
    '8S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAA1VBMVEX+/v7////+/f3////s6+vq6urq6urp6en///8AAAD7+/vZ2dlISEkNDQ739/f19fXV1dbQ0NDMy8ygoKAqKisVFRYSEhIFBQbx8fHS0tLDw8O8u7weHh8YGBkHBwf5+fm/v7+ysrKqqaqZmZmQkJF4eHhiYWI+Pj/t7e3s7Ovj4+Pe3t7Hx8e2trempqeioqKdnZ12dndqamtYV1hTU1RNTU5BQUI5OTomJiclJSUhISIKCgro6OjIyMiurq6mpqaUlJWLi4uBgYJzcnNnZ2czMzMHBwjCoN1IAAAACHRSTlPy9PH1n5qZlIbE4BcAAAFwSURBVDjLpdXVbsMwGEDhtCOfQBtqygxjhtKY3v+RViWqJrt2Omnnxr74ZNl/LmIdFK28ivuW2Ja1I6Sc0BFSBUs6JIqB+VA6QhLvZF2bRADQAWgYxC0k54/NNrgGMYf+amlCSy/sCtXyau12SLq5ovRNYmuF+MquGEJsuMcQZr5wYhiZ5nEHLIB748ScKgBV3yRqC7JaxwYxAa6jMXCnFzWguVqfgCOt6ME03bjQ14oB3KSbMVxpxfP6mbcQaUUAnZPVeryEM/1bKtC+DC9a4Nl6EbHu1TTTG7ImxqmLwYOHd3gpFCFVO6kJoQq1/4ujbeKs0swXpx5LJ084CeDVzaLfIW1gEmPWfeiFy28PqlABHG6KIXIjVdht5BJbEQ5qgSJ81EJFdD2yXLKqJfWmF6RNTj9Ju9p87UsMlTcxtUdtiCPtTP2gLBqci1Lg53x9N5uVWYRAI0+UZkCrLIuCJHr1er0nicIf/h57RSu33R/1LT///CJl/gAAAABJRU5ErkJggg==',
    '9S': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAxlBMVEX+/v7////+/f3q6urs6+vp6en///8AAAD29vbe3t7S0tJHR0j7+/vj4+MQEBAGBgbCwsKrqqsrKysnJycUFBTt7e7Y2NnV1dbNzc2/v8Csq6ydnZ6RkZGLi4wuLi4iIiMYGBgNDQ74+Pjy8vLl5eXb29vPz8/Ly8vGxsawsLGmpqeioqKPj5CIiIh9fX1vb3BoaGk/Pz85OToKCgrv7+/g4OC7u7y3t7eampt4eHhjYmNeXl5VVVZMTE1CQkNBQUI/P0AfHx+NQ9kjAAAABnRSTlPy9fGan5R5ze3CAAABS0lEQVQ4y6XV127CMBSA4UBb/xkkgSTsvaGU3b37/i/VSBFFdrGp1P/mXOTTiRQrsnWVt0zlLyxxLisnpMa1sZDKWdKSRgfozKUVkliQVdKJMoeGGrGEsHRXCuFGI7zsUQs8jYjASYcDkUZ0YZSOEXQ1Iv7ZEWvEBzTT0YStRqwgKNrFADYa4QMkAA2NEEuywqFOiHaPfQIvQivEwPW7UFOEXB06wihiaBjFDAJhFG+wMgofKBvFGl6FSdwCU6Now04YheP7Ba049n9RPidmyb1ZOH32FZOoBEDf1Yt5eOa/feLQ+rTwOPauChXA9rd4RK6likIPuaCgiApqriKqqNXUt/TJ8sgKbUWIawB4djYA0BSKSEkESVvsCq0eRIuT37Q6sUWRBzGYVA2n7/FpPv0pUDQJOwa+bFnkJFF3Xbcuidwfbo+LvGXs8htoKTELooNnpwAAAABJRU5ErkJggg==',
    'TS': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAA2FBMVEX+/v7////+/f3q6ur////r6+v///8AAAAAAAGRkZHw8PANDQ7IyMgwMDHQ0NCJiInS0tLPz89AQED7/Pz29vb09PTV1dVISEgGBgb9/f3JycnCw8O/v8ChoaGMi4wsLC0oKCkXFxgRERILCwwDAwPo6OjY2NiwsLCmpqejo6SdnZ2Ojo9vb28fHx/y8vLt7e3e3t7MzMzGxsa7u7y3t7eampuXl5dzcnNnZ2gyMjMiIiMcHBwKCgr4+Pjj4+O1tbarqquEhIR4eHhjYmNhYWFYWFk4ODk0NDU+t+ZEAAAABnRSTlPy9PGX9Z2qu2yoAAABfElEQVQ4y+3Vx3KCUBSAYUw5v0oKiiK22DW29MSa3t7/jXJHCI4XxEW2+TeXGb45w9yzwNjfM+La2zdkV8ZB8JhznLQ67NtBNbMGCSMYcrSAe5HnMUAhHYwIRB/gQSy8aiFRbANUJQvUx0A/NGNJE+4E3IrIBLK6SDM8h5sOfIjKpFHURKcsVShlYCiqY1oXmlAN4UiJM1HNaKXDIqXECZx6M8x/wfrGvrfdWMnyRZ1WPlKUL7ylXraZS6ToiQuj68oMPqPFQEb4lSLEK6QkP/fASCJEv1brieSni7Z7/CBhsc5+6ojECtUfxeUucdO8jRdXJstunOg2SWJa20XPJZlM0n7ZJt5QQAWTaFFQwAuyRU0EYE3C4t4DAUnpotjwgR9NWxNdfPF7cKIJxxcE57UmbBPvhf/BuDlNyDmr3q+mrBqIJhSpQeNMvuxUA+rVyDt1MjkpcyePGSdm+wWy8dsvAeU4kasD4/ymSGyIimVZlQ2RMGRX8X8g1eEP6FY5Inea9vkAAAAASUVORK5CYII=',
    'JS': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAApVBMVEX+/v7////+/f3q6urs6+vp6en///8AAAD29vYKCgsTExTv7u/Ozs79/f3V1dX4+PjS0tIXFxgNDQ77/Pzt7e3BwcE/P0CSkpJycXJHR0gpKSoeHh4REREGBgby8vLe3t7Y2NnY2NjJycrGxsa2trafn5+Kiory8vPh4eG7u7yxsbGrqqumpqempqaampt4eHhtbW1oaGhnZ2djYmNWVlZJSUo4ODngb6ZTAAAABnRSTlPy9fGan5R5ze3CAAABDUlEQVQ4y+3SyW6DMBSFYUPaeyA1MwRCoJnTeR7e/9Ea1UUITyy6ySL/ypI/Wbq22aXLbLkTRmMxh+QCvuEZdTlMPcRrgbw/Qif8UTGFf3UWJyeio8jtIoV/bRW3QBIYBD/QsTdgRloR7HxUnFcASoNY4K800gsKO7Ilg6Cb1wRoPlekiL4oDMWg1mn/L/IxsZ3e2cUyhb+2iXUDII3N4r7Fbw8m8YKuSi/m6JsFqhCgJ6p4xrBSFkEiiSaTxApysSRCRXBJZClEXxC1niToUWx8LN/FYq9O+1QDyYG+szIB6r3+n3KPCuwo4qHl9efirsxiA6CwCa8GsPCGwhmIIo7jYiAcRmOxicusXfwAMfItBbGzBQUAAAAASUVORK5CYII=',
    'QS': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAA1VBMVEX+/v7////+/f39/f3s6+vq6urq6urp6en///8AAAAXFxf7+/vi4uLY2NjV1dXS0tKYmJj4+PicnJyXl5dhYWEvLy8GBQb19fXz8/Tq6ure3t1eXl84ODgTExMODg4LCwv9/f339/fv7+/MzMy/v7+7u7ympqZ4eHhubW5HR0gpKSoiIiIfHx8bGxva2trPz8/GxsbBwsKmpqehoaGSkpKGhoZ1dXZoaGhcXF1aWlpCQkM/Pz/k5OTi4uO3t7exsbGrqquLi4uAgIBzcnNUVFRJSUoZGRqOOhRqAAAACHRSTlPy9fDxn5qZlB5vnsYAAAFsSURBVDjLxdXnUsJAFIbhYM1LTKKUUKRJFaQXEbC3+78kQ1ZGNmwWZ/zh9ycnM8/snD0nMzFODwxdDk8Mc1+MmPR65tqWDGLG9iGJNNC7zkpHbIsFIt2HCHELG1JRChu4mdQa78BMKa4hHnSQ8gulSEMiKCwPHIXIliEnyiU0FeIMit+jSEFSLeL7xYUoL+H8H4Xo9L5v3kFSc9uOl3tVi4yY2Dn00jCOnrpTYB07anNV/zlf71i5uT7QSw7rKaA8031BIousQvi3FCm0gKpKmM2C5/fQWt+oYaqEn5yTD555qQ9l/iSq+8Sw2NCLfImyrRN2ESi50eLJI8hzlGixyZtatPnJsiIJGYisdkUCOdOwqHyGRMkKCZtw3JBwdkQtJKwSIh+IeFfhTscESeXnokjs3raZhviLWbCmcehMlDN1BhlzxKN5MXA022+z0m+/Box04qrji25GFjFJ1F3XrUsi9ou/x/GBoc3RF2TnQRGeMCifAAAAAElFTkSuQmCC',
    'KS': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAw1BMVEX+/v7////+/f3q6ur////r6+v///8AAAA9PT3Hx8jFxcXj4+PS0tIWFhb19fXY19j6+vr39/fLy8sRERINDA2QkJFISEj8/Pzt7e3U1NXCwsN4eHhiYWI4ODgxMTEsLC0eHh4KCgoFBQbp6ene3t7Pz8+/v7+4uLmvr6+qqqqmpqdnZ2dZWVpTU1QnJyjn5+i7u7yxsbGgoKCdnZ6ampuLi4uGhoeDg4RzcnNsbG1dXV5cXF1MTE1CQkM/P0A/Pz8iIiPHYHAUAAAABnRSTlPy9PGX9Z2qu2yoAAABV0lEQVQ4y6XVx3aCUBCAYUyZP7mAIGDsNUWNJb3X93+qiBz1gDAu8m/uLL4zcLgLrMMDS+vg0JJ9WUfryTGmnEx1Y8bLMalkbZb0oJlMfehWNiu2ogNmNbwBdckRJ3Aan6fAWDThQls0MQVXNGGAoFiY1TNaUiwCaUNHFOHbgKOIgdMFWooIFwCcFwsgDOFdFd4EuFHETGpD+CgW/eXxAtiFYhSfEcwKhRefz4Ct3q0fwVwVUgVsVcgA5nmiC0Y2SwJ1h/zCZ444t21fksq27WXFbv8R9X3CuLe6uIgYeJrwQiByisXdkFWTItFi3Wu+OGPbV1rsAvjZFY+kq2ZFzc2I0M8Ij2xORjR2RJARfkTS+oWHlYyQa1a1L6bJ8CBpEZMeuE/SqVVd6N3nftPGZVmajKRy2VBu/4xv/fYDoKmJygLol9OilBJXjuNcpUTJkn3FfyC14z8QIzDqM39zkgAAAABJRU5ErkJggg==',
    'AS': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAzFBMVEX+/v7////+/f3q6ur////r6+v///8AAACtra0YGBn39/f19fXc3NwQEBHPz88JCQnS0tJmZmdISEglJSbX19fV1dW8vLwTExMNDQ4EBAX8/f35+fns7OzLy8umpqeSkpKMi4x4eHjw8PDo6Ojj4+Pe3t7Y2NnFxcXCwsK/v7+3t7iysrKqqqqYmJmJiYl8fHxxcXJfX2BSUlNMTE1AQEE/Pz84NzkpKSohISEWFhagoKCdnZ6Dg4Rra2xjY2RjYmNCQkM2NjcsLC0fHx83dZrwAAAABnRSTlPy9PGX9Z2qu2yoAAABY0lEQVQ4y6XV1XKEMBiGYbbCW2CRdXe3rltd7v+eul0tIWQP+h1Awjxk/shMtNsbTZWbW02/Fu3O3286Tf+HiOYfJG9jC0MIIg2klSIGxJTCAAyVcABwFGIM8xw1hUhgFDckwkUS4vockqGiCo+HR5hY/f4ehc8w0YDNvhgaIWIEk92rBpUQsTgsRQoWclEHu2hZVj1DzpKKIbiGbduGC2WpmHHJTCYKLpe4pkQMIPFwyBoeJOINxsfmC0wlwoDOsVmCbVCkIXsuLwP9gIj9PX6vMA2IaDRqnoW56wlCkn+I4jXRyaTVopXFbatEMgNkvXAxcAHIDcNEjVMmchHnkg9RiACWQTHCn4oo8oYg7IIg2ojxBJFCTEkQhSyHnArOmWKlZfZ5b8XY5zk42/IXbKv6Kl8xYP0kXdOUY+ld+rrppBS7H2ep3v0S0FUJMwF8W34R8Yme53k9n4hcvz2u3UDa/Q+f0jNsbbEa8gAAAABJRU5ErkJggg==',

    '2C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////+/v7r6+vp6en///8AAAD08/PDw8QHBwjm5uYlJSUfHx/7+/vv7+/d3d3Nzc1TU1M3Nzf4+Pjb29u8vL20tbWHh4eCgoJdXV4uLi4ODg7h4eHX19e6urqurq+lpaVwcHBbW1s+Pj8ZGRoUFBXR0dGrq6uioqKYmJh6entmZmdhYWKbm5yQj5B0dHVHR0h1eUW/AAAABXRSTlPy9fGclN+3aLUAAAFFSURBVDjLhdXXkoIwGEBh1M1BitIVwd7blvd/ugXN7GwmJJybf5h8Ey4I4HwMHVvDkSP6cgbq9fKcC6WBo2xSpEAwV7ZQxIx3C5NwPWQbgyjB2y6LEA4G0SzdmjGG46RTTIHpe9Zup8hWedTOHE7aHko3SIVVhDC3ijvg2kSE3MIoKgiFTXwBsU34wFPYxAHqiU1sgZkwCflwf4RNJHAUNrEBStdvMzyXADjxamkWsshwgnpFPP4r04SWWayKlVVkCZC4FpHSVk2MYsO7rVHspHgYxV2KnVFMpfBNwo+eHuDtc79TZAuP8CXCBrkdIuV/lS4uqF01sUAt0cRDrnhy7jVRyJVSzpkm5G125295E12IeVAHnyKYXKs6KE3vS/vBvDTTdoIOpPYzNgbWNuEegZOrioEi1nEcr6fql7//7zHq+wP9Aq7/KK2Z1Em3AAAAAElFTkSuQmCC',
    '3C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAn1BMVEX+/v7////+/f3q6urs6+vp6en///8AAAAqKirCwsO7u7vh4eFbW1smJicFBQX09PTy8vHl5eTc3NzMzM0uLi76+vro6OiioqJwcHFNTU5HR0c8PD0VFRYPDw8IBwnt7e3W1tbS0tKdnZ6JiYmAf4BoaGheXl5VVVU/P0A3NzchISHZ2dm2t7eysrOtrayZmZl3d3diYmIzMzLGxseqqqpi4So6AAAABnRSTlPy9fGan5R5ze3CAAABU0lEQVQ4y43V13KDMBAFULATLohmqsEU94a7k///tsBYPGiEltyXHUZnVBYxaN8Tjcpkqhlj0XTxOV2HhhBdEyZZLTyg2QpTCOKCT/ZKYYLnRyGuAB625QKOQhRA1hYLeCvEqzjZbQkAUxJC7FHBgJwQadjt1CJEDqDaGoQoACwMSizQEUoEK2YCu9HTlrQwXMCmRQFEg8I245PRJQbWg8LHp1WpByyHV3GAahPdXPV7CdFnrjrLhgNH3TErL1G/H2TX/YBvkuwHLS5sRYrkCCD3CXFGlzhRinvfDqXIRr/KJxeZUiy5CFQiDV8eAM8Jg0GRHGo0dSeaFs1kwU/aJ5bFL8TMJXGAmKMk9nyk5tWRBOMjO15vkuDLZOtnv4jcj61bmRvDTOZx5e4UPW2bcMW9rdQNOuFM3zELAKOEXwIoZ6LQBcGiKGKC0P/x95hONDJff67dMFbZ2CWfAAAAAElFTkSuQmCC',
    '4C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAnFBMVEX+/v7////q6urs6+vp6en///8AAADz8/Ktra45OToGBgbNzc65ubksLC2pqal2dnZmZmdiYmJdXV03Nzf29vbd3d3U1NSjo6R7e3twcHFTU1M+Pj4lJSUODg7l5eXZ2dnDw8OcnJ2Tk5NpaWkyMjIZGRoUFBX6+vrt7e3o6OjIyMi9vb20tLSBgYJaWlpJSUkgICDi4uKYl5iGhoac7KgUAAAABXRSTlPy9ZqflLAYH7cAAAE+SURBVDjLjdXZboJAFIBhtJ0fKiCorAJudV+qbd//3WqFpCLMsf/Nufkyk0xOMsarIdc11LOMjqoX7Pe2uqtjPB4yAK92xKMIgJ0o5nBWoliCI4oI1koUPiSyKCAURR9GShQj6KtJFJk6MYXi9mZ9nfDAl8UabKXewdIIC1wligy+RPEJq9+ZgN0uPHDni8Uig8GxVZz5a9kqsqcieStbQe7VxGMpOEoUPliyGEIsi3A22wqiShDTw1QUmzEwDuQdA1y9sCiLtSKthK8VSSVSrdhWYqITu3DQA3of4aRVBMMel5u4XJHZIkbc5zZFn3pxQwypN24In7JeNYcNcaDsVM1jQ1TXpJFXXdIU6jsv3LnKN7Fb5CfNm5pKOVjXKW1QxkjesRngSMJcAWuzLjo14di27dRE5x+/R9eQe/kBD1sp5dF02doAAAAASUVORK5CYII=',
    '5C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////q6urs6+v///8AAADNzc0FBQXi4uKGhoby8vIhISFwcHH19fXr6+u7u7usrKuBgYFmZmb7+/vV1dXDw8OioqJcXF02NjclJSXn5+jc3NycnJyWlZaQkJBgYGFXV1dRUVJDQ0QuLi4SEhIODg7k5OS2t7exsbKNjY10dHVMTEwpKSkbGxvv7+/IyMh6enoaW8b1AAAABHRSTlPy95if9l9TJAAAAUJJREFUOMu11dl2Q1AYhmFaX7YYYiYym5Jmanv/V1fit8Li3476nrDsJ8Q2KR+KtHpcm0tR3+ubRdfqvVFVejvx0LXv7WJaHBixRNeOF5b+yubFlVZ54c2Kgx67ulSYAhA3iaAsqTBRt2CEBYhIC5+Az4iL7zc/doE1I6gNkMrFSsD8R3G835tLpgMPRpjAvp2WnBE7QOyjA4ArI4ISlMH90x8CXxp7LmFWosojbVJQtk5HYAQlFVsnlIogA5DZErFGUxGwwkVbzAqLhM+KXxIWK1YkzpzYbI8CgEi250lhJwLmS5gQJ2NCrNGvGAsXw+KROGLYaSR8GhG0TEbCoRGvfxeRoJJ2ssIbHWQstGVaPXZaGsRFlXrMnNaTcIFbL2V3UE7PPSsiAI5MGE8ApTEU6kA49Wvf+e5vUue/HrNfoM8/pFAjgIpk1w8AAAAASUVORK5CYII=',
    '6C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAnFBMVEX+/v7////q6urs6+v///8AAADy8vIFBQbMzMwvLy9gYGFcXFzd3d3W1tbDw8S6urqjo6OcnJ1wcHBGRkc2NjYoKCkkJCQgICAODg78/Pz19fXv7+/l5eXh4eHIyMi9vb21tbWBgYF3d3dXV1dQUFFMTEwYGBgTExT4+Pjo6OjZ2dmwsLCtra2rq6uTk5OOjo+FhYZmZmc+Pj7Qz9DE0Lo6AAAABHRSTlPy9Zif9duHSgAAAU5JREFUOMuF1dlugzAURdGk9YYASYAwppnneez//1utwkMIuea8HCGWjHRtmdZXyxj9XjWl1VbVWJ1O5/W53Xpf5AnLyhLvogeMjMIFV5mEBXSN4g6RMootzIziBKkyCgemRvELu1PQMQgPbGAzlMSPTZGNJM4AGUBfEAPY9tR0B64gIgh1hRALwoGjrgmkgkhgousAmbxGV9cYEkGsYaHrBpEgQuDWvwLXponZliCUX4q5vLfTTU4e9Y3n1JpY4u5XI4vjrGcU+zWw/jGIGABnKIozRQJRDErxEMWyFANRjEoxlsSh69qA7XXHH8Xes8n+RYa9sj6ImNc4dXGhmqAmXKpZ1cSDInbZXk3MKLIoe14TyiuG1VuWH6kLdU/zJFTJMHDydCHMVA9hzkW36QQ9ic1nrA/4JmFtgZ1VFe2K8PW974+qN79qSuMf6PsPGgEkYyWNnioAAAAASUVORK5CYII=',
    '7C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAmVBMVEX+/v7////+/f3s6+vq6urq6urp6en///8AAAAsLC3Hx8fn5+fx8fGtra7Ozs6kpKRwcHFeXl43NzcHBwjU1NS/v7+7u7ttbW1kZGXz8/Pt7e3s7Ozh4eHY2NiysrKpqamDg4Nzc3RVVVVAQEAyMjMlJSUPDxD5+fn19fXd3d3a2tq4uLhaWlpMTE08PDwgICAaGhubm5x6enoEacc1AAAAB3RSTlPy9fGfmpmU1MiaNQAAAT1JREFUOMuN1elygjAUBWCgSw5l35GCgAputdq+/8MVbCxkYm49fy5DvkkuzJ2J9mpoVIwXjf0XTZ+e47db4umlrs022eAWc7bFXHz+iY1CrHFNBIRq4ZRl6QOB+pRqKCfAUwgTSBizAVgK4aaJzdhy+pRJCGmBjBYhUJOiGNsgBG+DFD6Q0cIEHFJYAN5JUY9tkCIDMlpMbUxCboMU3jg9pMiBlhYXwCHFYWyDFIUbHxgleGhRJRUptisApx0hQgDkfDj4jacUPhe5Unxz4StFwYWlEtY+j0bwsbfuil0Q4XwV5wEt7ogQ85iyqCHGk0QAMStJ5Hwl4jWQRMpXGl6XkuDH+HHLD5EFa7re/GLd1jP7rlH8U5uxI5yhUhO0RkjPWALgSInFBUBvi0IXROq6bioI/YHb49nQyDz9AMVlM8gYOfMnAAAAAElFTkSuQmCC',
    '8C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAnFBMVEX+/v7////r6+vp6en///8AAAAFBAX8+/y5uboqKiolJSX29vbx8fHY2NgKCgvz8/Pm5ubMzMxxcXFkZGRRUVLc3Ny9vb2zs7Ojo6N7e3xdXV43NzcxMTESEhP5+fnt7e3T09PIyMjAwMCtra2ampqTk5OIiImBgYFVVVZHR0dAQEEfHx8YGBkODg+pqamPj49sbG1aWlrf399paWmuNd77AAAABHRSTlPy9ZyUBmWbxgAAAWxJREFUOMud1edugzAUhmHS8pqRkEBC9t57tL3/eysJ0NTCh1T9/hyBHtnWsS1b71Z53iz7VayK/h3MA/1HxdIGWfmAv9aG0ESfNE1JRAAKoCOIGMbd0D3BQRBH6CZlASfHKCZnVDWpTp16wyic8VNUzbN4ME/KDHxhHVfw2nY4gq3Ujz3gKTg4kggUj8ykjjU80vhTQbSAr/UNiM2ixk/HmBqFC8N8wUuj6MEt3+K1UVxgl69nZRRzqNfS7uOad64OnvvZGYES9mVDnr7U0zgDe1sS9tUD/I1tFFnaUXAvgtAiiqBTPsYkBuJqiRgCMHJEsSRNTxTNTLT+f2/taSZCSYTtlgLURzs0iupAcXyIY4IaBjHkd0ZFcUFPryAG6NkVRIs0KquDguiQZpvVri6e0zRnzWwSQz82/tlb2b7T887+Vuhp7X7xlo8qn6BDdjNFsQDcMtE4AeOaLiqacKMocjVR+cPr8fbqBfoG/6gmTmClZRMAAAAASUVORK5CYII=',
    '9C': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAn1BMVEX+/v7////r6+vp6en///8AAAAHBwgoKCghISHz8/LMzMxbW1wtLS7t7e28vLz8/Pzw8PDm5uba2tqqqqqioqKDgoNvb3A2NjcYGBkSEhL4+Pjj4+Pg4ODd3d3R0dG3t7etra6ampt7e3xgYGBVVVU/P0DX19fT09PBwcGnp6eenp6KiouGhoZmZmdPT09HR0gODg719fXFxcWRkZF0dHWHLxi3AAAABHRSTlPy9ZyUBmWbxgAAAU9JREFUOMuN1Ylqg0AUhWGT+qtRo1GjMfu+r13e/9k6NBYZ7J30IByUj1Evylhvljlty34Vq6Wfp36qX2hZ2iKLEHAzbQlNHHmmL4kev+kK4gzOcbJwIBfEHc6qlrAVRAipqjF4gvBgrKoLpSBcmKpK5DW2sFSVwUAQKxh+xH4Ja0FMAEqAkzTTXJpYncsQ1HGzRWF3/IkLB1mozMCzjSKEk1HMwImNIoDCNokuMDaKFdxtkxg5EBnFJ7i2Uexm+54s9MgijXbmJ90At3eDGAAQxqJYCP9cnaISV1GsKlGIoluJRBLJdO0ATjBN/hSjwMH7EZ5CHU3Ub1onbIoMPf2GWKNn0xBXnnGqDhoi4pm86nnzSQMAiv2XdhNtHhf34S5tN+6HDy8XZqqGMCdTbfqCtgzM39gBmJtEpwSGPV20NBH5vh9povWP3aP9agf6BtUuJC2a7OxhAAAAAElFTkSuQmCC',
    'TC': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAmVBMVEX+/v7////+/f3r6+vp6en///8AAACLi4zOzs7w8O8LCwwvLy/IyMgJCAny8vK5ubkeHh+ioqI1NTX5+fnm5ubCwsLa2trU09OsrKybm5uQkJGAgIFwcHFbW1sZGRrq6ure3t6WlpaHh4dmZmdhYWJWVlZRUVI+PT4pKSolJSUSEhP09PS+vr6ysrKlpaaenp53d3ddXV1ISEntcc0dAAAABXRSTlPy9fGclN+3aLUAAAFgSURBVDjL7dVXcsIwFEBRA3k3LnG3KaEHQksD9r+4COwxRbbIAnJ/nj7OSDMajW09tS1T7Y4lj7Ja1fKj13PUSMNBbF9Ay6o2ySfwI/KlBhydaotK/KAaiE2Rp4mUU9+SAJsIiLU9fHbwKeDnIiNY34sp8xjsGSxE5TK5F1kuAyVe4FVUHm5wJ1RvVyLBdXTxXIjn8x7/ohDVjfkPxAY3qBdTOKpl4BNJrQgFGHbtNazqRSxjyvIaMYS5THcFWEqNmEdRXyR7dyF6E11ccnpdEaNQmUT3s2sU6QpYfRhEwimvWfQp+m4Uo1IsGsW+FKNG4ZQiaxLZbOkD/niW1Yr04BOdRaRQUCMSrvN00ee2WBPLO/GuiQVFfjnHmggpGpbzSxNyKC6rt68O0b8wW3czkG0ae+522HCnwemwvpqmF7QmMb+xHAhNIpgAu+BWtG5EaNt26MhVrT/8PTpty1jnF/FlLIJtNo3HAAAAAElFTkSuQmCC',
    'JC': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAgVBMVEX+/v7////+/f3q6urs6+v///8AAADz8/P29vYODg8TExQGBgdTU1T8/PxbW1w4NzguLi7MzMy1tLWsrKtwcHAnJycdHR7u7u7m5ubc3NzV1dW9vb26urqioqKDg4PQ0NCcnJ16enpmZmdgYGFNTU4jIyTi4uLCwsKQkJF0dHU/P0C/BzuyAAAABXRSTlPy9fGYnywJdDkAAAENSURBVDjL7dXJbsMgFAVQnNQX23ie58yd/v8D6xbUxMIPtlnkLvxkdAQ8hATb75gpuz1zbWGONhQdskP0/+cwfRIOCO8+xZYQVuEjeInnE559DoEgMooeuLmEGMrfbwhcCXEEwjxfAApCpFCpXEI0iQT1OyGWfCa+n55Uq0QvnMtq6tYmuqE3Ci8EcDXtQzaTRqQoIDORolTiTIqTEiUpGiVaSrTdRQAQcdduCi8WqP9EvSC+IRI8JtVFgXUmTVywTqiJM2SEqrEmBsjMqub6TmN5WP2XWkQX7lz51dGtoin1bzNxpssh5CiWarpB30jMd+wDwGgSvAYQ8LVwVmLMsmxsHocc++the4HY2w/aNyWxCx1DSQAAAABJRU5ErkJggg==',
    'QC': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAApVBMVEX+/v7////+/f3q6ur////s6+vp6en///8AAABaWVoWFRaenp4jIyT7+/sEBAT19PXh4eG5urrm5ubc3NwuLi8LDAwHBwj39/fX19dUVFXu7u7f3+DNzc2jo6OampuEhIRwcHFAQEE3NzcZGRry8vHq6uqtra6rq6uQkJB6enpzc3RnZ2deXl4qKiseHh7T09PFxcW+vr5/f4BiYmKzs7NqamtPT08AeX0qAAAAB3RSTlPy9PGa9Z+UPZuSOQAAAWpJREFUOMvN1NlygjAYhmHUtvlQBNkXAQH3fWt7/5fWUOJIShJP+55Ex2f4JWTQPvqaqv5AI6/S3rivk7P7B/S09kVyD8BpF3KXaAsdTYYjETvgQcQiqn/KUjsGsBGKL2AesmEnofCAEakLTcAVCMuHmTQfS2AlEBNg+LynkUBMgflYLtg1/oN4/NNpRGIqFHdrmMutWFg+sCRkQ+ERWEl2PafPrwLNnIpEAQyXdN2zx98VLijZXG0dgD8TCbJFKz0UCHJHU3kDAkskyLr0YR7phDy48FOeJW5zih3CCVFyEV0jpRjHAL4thahQ58nFCk2ZVBRM7AWC39VCKhImHJlw3HsAwNRdRyjCQ4DTrzAosgSiQjuvK9bgyzriAL64I/ZoCtiqd4SNphtbP3nxHFOcd2xIV5CZ4R9zYowzzzdmkj1d1MPWdFWdoBKV+oxdANgqYc2pGC540eOEnaapnfBvfvIqbdDXlL3/ABCHOFALFQq/AAAAAElFTkSuQmCC',
    'KC': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////+/f3q6urs6+v///8AAABCQkLGxsbz8/IGBgcODg5bW1vm5ube3t74+PjNzc26urpycnJnZmdUVFUhISLv7+/U1NTCwsI2NjcuLi6+vr60tbWrq6uioqKYmJl/f4BhYWFPT089PT4zMjMnJycaGhsTExT6+vr19fXY2Nivr6+NjY0WFhaGhoZ6enpMTEw3KCHqAAAABXRSTlPy9fGYnywJdDkAAAFZSURBVDjLhdXpboMwEARg03SnmCPcEMh9X03b93+64uCkSY0382dX6GNAyBJi8Ca4vA0EvYpwblvlurLbStc9R6TjiHvJBhh32w6o70L8iTUwui4ZgJJ6xAfgqukCmBAnLkBGnMiAHXFiBCC2CvWme2BBXMcSWBMjohBAxQgv3gBIGeEXUAntos3+AgSsGJ5VCSPa2z0gsIuiHZ+qySoSNX0gt4qKjBLjm+qSFS/mqsQqzBJhnFNd4vaIH9+/XW58L+8RRykPt13K0BBGrOI0PrEiygGsjoyoodLYxRRdZlaRapFZxbcWqVWUWoQ2EQ6XHgAvGIa9Igo8FFdRtEj2iBqPaUwxxXNmhlj+E7khMnTx9AwMkaDLQs+JISiASlql+iGmoK+tv53T9jBr/GJh+aaSaIJpO7kTtEbNn7ERgIQTcgdgL5+F8ySSOI6Tkh7iCHoV9Qdi8/4La94pdw14fXcAAAAASUVORK5CYII=',
    'AC': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAnFBMVEX+/v7+/v7////r6+v////p6en///8AAAAKCgvx8fG0tLQUFBX7+/vm5ubMzM25ubn39/fi4uPT09OsrKxbW1suLi8lJSXBwcGioqOBgYJwcHBjY2NLS0s/Pj8cHB3z8/Pt7O3g4OHd3d3Z2dmQkJB6enp0dHVnZ2heXl43NzcqKisgICG9vb2GhoZra2xVVVVSUlOcnJ2bm5yIiIn5+lF0AAAABnRSTlPy8fSc9ZQgDESvAAABX0lEQVQ4y6XV2XKCMBiGYdSWNxpAEMF939fa9v7vrViYEcxiZ/odkMA8JEzmJ3Hea44ttYbjvorzVr33O1H1Qd15GkQivOoQT6IHtK3iCoysYgpIm+gAEFjEAMaCrUUckfOUk1ksYeLeoGMUaxjcLxejmNxfX0BiEnOY/n4MvkF8wjVrNrA2iHG+FCuY6EUIwm9mkdDUihiQQggpoK8VXR4Z64QnKCXUiDakrTwJxBqxg03R/YauRkyhV3QDkKroUapQCS1FDMvlt4WRIhZR9ChyL4oWilBiEqvzyirCGzAOLeIAQOIZRUyevlEMC7Ezik0hhkYxL4RvEv5yBEB36WtFuBecBMApQzONOFBOooqYavqK2FPNhyJ25BHk6SriTJ4BAEoFPaYZdrbFJKpwL6k4frmp109EOjCsafP+T8RZa6ugCQf3fzvubArIZlXUK6IdBEG7Iup/OD0ar06gH55NMGQ67gysAAAAAElFTkSuQmCC',

    '2H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAwFBMVEX+/v7////q6ur+/v79/f3+/f39/f3s6+v////uNCTuNibrDQDvOCjsHQvrCQDtJxbsHw7rBQDvPy/uMSDsGAf+9fT+8O7rEQDqAQDsFQP7y8bwSz3tJBP2joT1iH3zbWHxWErtKRj//Pz70c3uLh3tIRD96ef81tL5ubP4rqf3nZTwRDT+8/L97Or95OH708/6v7n2lYvxUkT/+fj+9/b83tv829f7xsH5tK33pJz0fXTzcmbyaFv3pp/1gXbyYFP1rnL4AAAACHRSTlPy9Zjx8fDwn0ec1FEAAAGjSURBVDjLtdXXbqQwFIBhZra5G4899D4wvaeX3X3/twqIRIrJ2MlN/hsO0icL2ZZwxr8dWz/HDvgsZ6S/7xc3QGvkaIvMzmVSTtbaEpqIfJy4iQoyk7giiruIu5LdGcSD4PVxN4sbnBvEORCr7mOUii+Lqw1FXvv0YLipLopqPu8AuOHhYI1hfzHO7WLisnurSBmdVDZxG5biHthEHPr/gU2sBXd3NrGnXKyBTRR+uxc2sRKlf2sTXpKIDNhEztwC2MQKS3U6RFF0F1WXRYERkbQNc88gRJI0bptvEnkz6ZMbg7jeTvu2U6AJQ98jltljegBdi/QxW34QuzNmmIXdnqeKtXM810UVBxJBxIMUnALeTjDYXGtiJWAXKuu1bEE3Bg+aeMKwJzWtUT/iXBO5D1+Tb0P4rImMIaiH2D9NzMJ6IKQfaQI8BUQDRDwDXUwRJu8Blt5AgDkSEL19A2Ry/3HXtwXjpF+gZIV38eROPiUdcP3UdLZHqggkVC3Np39oFFE0st2PRYPpzH6DjsHyszu2AEMxAvZGX/h7jH85tn78eQGkUUY1m+OHBAAAAABJRU5ErkJggg==',
    '3H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////+/f3r6+vp6en////uNCTrDwDsHQvqAgDqCADyWk3vOCjtJxbuLh3sFwX82NP96Ob708/tKhntIA/wSDnuMiHtJBP+8vD95OH2kIf1jYLvPzD7x8L3lo31h331fnT0d2zzbmLyZFf+9vX97Or7zMj6wbv6vbf5uLL5sqv3oJfyaFvwUUP+7uz94Nz4qqOge9ncAAAABXRSTlPy9fGclN+3aLUAAAGMSURBVDjLtdXZTuswEAbgtD3xzHjP3pZ0X+gK5/2fDkNBwqZ2ueGXImf5NJLHtpL9G2apDEdZ/ijZwH+elNZ/Mcgy77uoOtWfvRKemHS8qzukp6gQvEJZoYYyImag1GlnrzVfRsSFaOqG0oCIiKermLihaEMRptCPxMzQKiEKO2sAbEKsORp2yhNiQwo3eUqsjEIUKTEurECahiJACpq0yAXoIi02oOxdsWta8Vmjer1fgxEd3PDaoRxHVo6UPs9OfR1dlxdlKkDTaXqOzWVfE9YIfB7vWLFq+n7jKqT6sXVXWuR/Kw6L5fR2Tux0uTj+ELsrcOBm8d444+6gKQLRUMsYU+5AzkkxF+q3nvhP7COVnrcOfJCzJ1bAbtGoP+9g7Yn1l2At+xIXTyw4C8OXnihBB0DD3p/LmqQHJF1yX2wZyO8A2nHY04Lxb4Lryc+ujwVX8lag4uLl7srNAeU7QIieuQMa6YA5xle/rI1E3Kf2h60By/QOeqbjoz1m81AM8nQGv/h7jIZZMqM3B9EtzDJd8ucAAAAASUVORK5CYII=',
    '4H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAh1BMVEX+/v7////q6urs6+v////uNCTqBADsFwXrDQDrEQDvOCjuLx/tJxb3lYz6vbf1iH7sHQz82NTyYlX7y8fwRzj96ef4qaH2joXzb2PtIQ//+fj+7+395OH708/6uLL1g3jyZljxXU/+9PP6wrz5sKn0fXTyaVz93dn6x8P0dWnxWUzwTkDvQjIGh7JrAAAABHRSTlPy9Zif9duHSgAAAWtJREFUOMu11dlugzAQQNGktcczXsCUJZB93///+xpII8UEhqfeJyOOLDG2xOhrxPZ4L4YajUXYNE2T9+fxqL3JCsAFW7TFVJKdsWKPWApW/BCdWXHyKhOsiBEdL7Sq5qyIkNaCFWvCSORJsu0T80rd65lR1CccQMyLTPmk/h7VJyKkjWDFkXDPitw+5+lAnbqFw2qzL4pi7e1l1ylK0oYeWa1h0ynWoJr6hct+mjJrtAtEuwmqVLAiBtrx4gCw5MX8nM5Y0fRvIooPk0WzStwhXn6I/IaIoOJ6HgoQ8JaHYpqBkVJamAgHVj6C+zQQBcgma5x5gIYUgbigfGa8+VthGYiS5Iu8FnQMRAyyHawCsSDTAobSQIgSdAA0HEUothL1O0AzawmRS3gTYOafU99ewernBhaus86Tc+R1DTxN+s428kpLrdSy//QXldLKp9z9SCr0C/4G7WA5dMcS0RZjwTce/nsM/oG+fwFEMSYLkxwF7gAAAABJRU5ErkJggg==',
    '5H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX+/v7////q6urr6+v////uNCTvNyfrDgDuKBfsHgzrCADrEgDpAADuLh796ef+7+33n5f2kYjwTT/sFwX82NT70s7uMiHtIxL+9fT819LzcGT5uLL4r6f4qKH3m5L1iH31f3XyW07/+vn94+H7zMf6wbv4pJzyaFzxVEXvQjL829f6xsH1jYPzbF/1hny1ebNDAAAABHRSTlPy9ZednE36qQAAAYNJREFUOMu11dlugzAQBdCkxePx2IYCYYfs+9L//7w6IVFiUsND1fvCFRxZshnE6GPUG/PcG8po/OxBMrmlSILnzfHoZZGpwjZUvyxhCdIorlGNQ3xxxsCExbVTyNC/JXcK2t6rU/DpoGj8pvL7hI6ASwx7BKAQiCrsEQKjVGh5dIhQKb32joeMZg5xKsuJuWykXNiimyCV0YAAPij+sMZ8ccnNxUe5d4hIqbo9/ItD1IS6ThpE2jrE8iAxzmIdp7lDeIWgLMtInjznXlY70LAoPKcwyVeBY4Ks/KOoZvMwabcUzmfVmwj2nDjJb1PPMZke+bZYRioFBqjOZtLQNKYOS0s0ipkYoqcpwq2qrSVKzlqihYa28tIWkt2TPorcWWJGwOwAzS2RSN0Rmq/t3ZYKLABq59kiBw6vgKdB90x9IPYwwEiv3k89XxBCC5D2we/frRRwBUKGrne7ETEYEFfut59kMQix7puPIuMi6Z+gjaqGZqzwumLs9Wc8/PcY/AN9/gCzzii0uKL7hAAAAABJRU5ErkJggg==',
    '6H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX+/v7////q6urr6+v////uNCTsEgDqBQDrDQDvOCjtKhruMB/sGQf96uj+9vT82NTzb2PtJBPtIA/sHQv70Mv1h33tJxb6wrz5uLL4pp32j4X1f3b808/7zMj4rqfxVkjxUEL+8e795OL2lYzyaFvxXU/xWEnwSzzwRTX83dv5u7X3mpH0em75savyZFbRFcNCAAAABHRSTlPy9ZednE36qQAAAYNJREFUOMu11ddu6zAMANDkXolL8h7xyN6d//95desWiJxKeSoBQ7J8wAeSsGb/ZsEYvqtHMZtPDuLVKrl9n8+mSd4RD06KqUgAJA6KnrBSIbGTIouC4gUpVUFxtVAGxTPBWQXFiSAJisTm18V6GRCvpDkDm3pFrDOuCzH46hMNcG4ukhsoPaKinJ9UqTPsPeII0AzLG8HJIzYg+2HZC2w9Ygv82ZOIXeHm+BICG49ICdphaYFSj2hRZJ2sRbD1VcxYsfXw6NhX9QVRlmVEC39vy7MRcy6Dcxo9RaHuj/Gn4rnqu3G8kq6vmjsRbYkIoRq2nUVCukSuiC9otOYaO3XAmrXWeI0dsUb9FSIvph632DoipfGYTWF43FLqCtDfYX42cHREhXoa2DtiCTIBBlaOUCmyAxiPyhUxE98CMrtpTSONNwLN/r7q8QlrHhMIbna/du4ABX+CHDrvFBaWNVvb+Lu/zC3bYhWajySnogxP0AKbRzN2/x+bq3DMH98eD2+g/x/LsCbQxVvEaAAAAABJRU5ErkJggg==',
    '7H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAh1BMVEX+/v7////q6urs6+vp6en////uNCTrDQDsFALvNyftJxXqCADuLx3819P+8/HtHQv6urTuMiH94+D2jIPqAQDtIRD+7evxWUv708/6wbz2kIfzcGPyYlXxXVD7z8r6v7n1h33yaFvwTD7sGgj95+X3lo31gnj/+vn4p5/0fXT82NH6y8bvQzMV6sTDAAAABXRSTlPy9ZqflLAYH7cAAAFrSURBVDjLtdXZcoMwDAVQktaWvMTsOwECWdv+//c1qdsJJkXpS++TxnPGaGwN9l49OmuPPYu3utdRu7Fpo/viyptsslXSRm0nW0xFA1qmaSo1LIkd8vAawbFZEiCDKIpihOPiV/xbizVgsCDyBFrGjPB1tiA2XWkYKxG3zBWzVAAxLRrEgBSGX9sgRYmYM1LEADEtcsSSFJn2uSFFYE+DELYNSuSIHSVsG6QIEBtGiiNARYsBoCNFlGJoSGGKzZ4RwuZ/RdBfqsLOa3XpgweRDQCgsL9dDSpQMOxdcRqSkHMhk5gdEik458nHyRFnxb8i9SGUtkzOjqjBLgvta2FLqF2B/DvhT4E7R/SKz6OOjihQz4CGd0ewOhEOEGrHXGEEiCmA0MwE23M1bUJHj6duciWF3UCqPPv15g7oixtIoVq6284fBRfjSPwdihTF6LfUfGxS8At6gsokeDZjb2wuVozO6g+vx9qj8/IJI6EraQV+19MAAAAASUVORK5CYII=',
    '8H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAkFBMVEX////q6urs6+vp6en////vNCTsGQftIRDrEQDqAQDwQDDrCwDtKBftJhXsHQvuMSHsFQPqBwD/+Pb70s71hnzwTj/82NT7y8buLh75ubPxV0n+7u396Ob3mZD+8/H6xsH6v7n4q6P4pZ31jIL0dmrzb2PyZ1r95OD83tv2k4r2j4b0fXPyal7wRjj3oJf4sKk8DKU0AAAABHRSTlPzmp+U1n8jfQAAAZZJREFUOMu11ddu6zAMBmD3nJDUsuS9s/dq3//takQNULthAhTofyNdfLAtkoKD/8HzvAWTVwlGpEiLERiKDxVSqFa8eJfYC5RHThTCEAFRpUtGbGKCeVsq0hdG5BjN+yVFvDJCRZD4VSWMEMoLwYlGY9EvBxI585aVxHw/6RqUU64eF0JoAGnHVqyzLsTQ4Z4TSR4aIYQx1xkjMmnCbLk2odw8FjOwuOjXkmzVPhQlRv4TtyJaPBR1pNe3zVSLJSPE2X8PJ1JL7lZtIJwzfbGYz7uyQYKEqzpYAkLgq775msILP8nvV0NGTZlJ9jmkB/423POnos7Op9Tfm9M5q3+INtdaxFHWb9cYC63zbiRy6QDAyPXkKEPoI1UyEEsJt5hq6ozfytVAbDT4VLYCH7EdiK2Arzi4i91AZDGMo7OBSKNqBJwoh2fZypGId6PTzkAPgHDtuKYdxN+f4PY/qz5rYnM/cty0Dzt3FPYGSJy43i4s9gCx5rufEiq05bP5KEjb9PkELWT9asaKX0zh67/HW/A8/z4B1qMp/mGj8eIAAAAASUVORK5CYII=',
    '9H': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX+/v7////r6+v////uNCTvNyfqBQDrEgHrDQDtHQvtJRTuLx7tKhrsGAbuMSH96Ob83tv+8e/2joTxUkT+7Or819P1iH71f3XwTT/7zMf6xcD6wbv6vbf5tK34pp/2k4ryaVzvQDH+9vX94t/5uLL3oZnyYVPxV0nwSjz708/70s73mZDzdWnzcGTzbmLoYSQlAAAAA3RSTlPy9Zo1JwzCAAABhUlEQVQ4y7XV527DIBAA4KRwg+Wd2Nl7dL7/49U0rVScQn71JIuz+XRCcDKjp1Ey+nnxKEbj8P0wOYQfxqOgyLyz2nbzoEQglkhaa+ZFTJRGWeWfbB8RJ7RmVayMxXVEbIn91Jp4GxE12bwfckt1RFwJmn5ogK4R0ZH5qmGoi4gz0bIfFkTniKhQ7wpR7BS3EVE4o7I6U8CriBAb1Jo0aFPGhKi0M7uM30RUiGZRXBTNEsIvhmqRFGfiVVIUToFIipZxmhSlVTpPilfkd5EUQDRLihNiLZJiMp83aeHjf8Vy2lbPt9Vs2unxTjQ1IyP5/awc9nmXD8QVM5CgsfJt1GcSd/tArFH6AGtesx74FE+BeGN5I0YZuKX8EYgPkt+R/SS0DcQUQYYB2AbihcxAGJoFQrwjDEpsRSj2wPAbcFYOhMgB5Y8BieZwv+vlBTXcgMVL+efJbUiBB4qq6L9QOZDg3DF++s/agVOzVH9MFKuXdAct8PioxyZiKMYiHePHt8fjG+gTQ3IhsQFduQQAAAAASUVORK5CYII=',
    'TH': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////r6+v////uNCTrDwDsFQPrCQDuNyf4pp/vPS3+9fT71NDsHQvqAQD829fuMSDuLBvxW03tKBfsGQf3oZn82NPwSjz95+T70s37zcf6wLv5ubP5tq/2jIP1h331fXPyaFvtIhD+7uzzcGTyXlD/+vr+8e/5sKj5q6T2lIv2kIfxVkj3m5LwUELvQjP6xsGTYFBrAAAAA3RSTlPy9ZmsLl14AAABjklEQVQ4y+3V2Y6jMBAF0GSKcpnFjh087FtIOnt3z/9/3RCizshAw9O89X3hShwVEmXJq1+r2XTvYSmr9b8ebPzHI3fPAbyyXr2GRJ7AM0DGGaJ5818jXqIUqOkCG4baCC23Y8FJO6yEHYXhPRaKypHw6FOxP6CFzgAqg7uhOGm3YCzdG6p7j/FQ7FMokQUbRi50eSPPt0Qftxe4LH4/+vZH9CJiy3+M3NkZqS/6pZ5C/IBJkYMnRBVld0HNtDhDJRULW6ExmBDvUrrgx1K0LZNXmBCl5+UAx8ZTKr6ALewckwTAFuP8P1HUzS3oW3Rr6mIk9h9ISKzuasWo63Fii2MsFXd4KCu4yrBrSn76lrhI5xGu9bsKeV+la4kDOk+ihObPioex6KO+CttZoibu2OHUWCJleiAUZpaAg+SDETuwhc+t73BUp4GAhJPzZbhDKhn/dX9LIX+CkO77yc1dUfAHEHj7bre5MNzhxhTfbz9ojWdENnc+ohZFOn+CclksnbEIhmIN81kv3x7LN9Bf0XooANiJhxcAAAAASUVORK5CYII=',
    'JH': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAflBMVEX+/f3////r6+v////uNCT+9/bvNyfrEADvRDXqAgDsFQPvQDDrCADsHQr2joTzcGTuLh3tKBf+8vD82NTtIhD1h331f3XxVEbxTT/vPS396eb708/70cz6w735vbf5uLL3lo3xW03+7uv94t/7zMf3oprzdWnyaFv5sqv4qqML8f0nAAAAA3RSTlPx85n4MkSnAAABL0lEQVQ4y+3VyW6EMAwAUGiCjd2EfZ19n/b/f7BBVKIJQzhV6qE+EANPkSw7SvAWeMP8F2sRhPNv5a6cXsJgvkkkCaJpi1cCVL8i3jH+F39QxKuix1h4xVVztyR2zfBMGdMF8VFRut8/WKrDgjgTsNYM1IoFUZyJq4qp37hiinsL0CUm8dRSFOPqq/Z3xaFuku14KpOmPs7EpUWFimuTJmwy7DaO6EiChJwScaIcTE5xZIlPkiYMyU7SgCGluyUeKEeS6QzGFFNLpCjd4KclagUOANVYYsuZIzK82bWkBBYAejrVRoDwE6AsHCE2oOS3MavKrsIVZn5UDiPIVXt52bkTahiAxmSptwfNYAAfl7u/rbjX+uabj7JCvfNP0J6OazNWCleEwh/h+u2xfgN9AZANHVkza1QiAAAAAElFTkSuQmCC',
    'QH': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAllBMVEX+/v7////q6ur////s6+vp6en////uNCTqBADsGAbrDgDsFALuLx/tJRP/+fjtKRjvRDTsHgz97Or0em75tq/+7+770s7vPS35ubP939z6v7n1h331fnTzbmLwTT/tIA/96Ob95OH82NP4qqP4pZ32lYv2kIf1jILyaV3yXVDwSDn7ysX3mZDzc2fxVUb+9PP4sar82NTRrspzAAAABnRSTlPy9Jr1n5TrAThtAAABkElEQVQ4y9XV226jMBAGYNLd9czYYxwny/mU8zlp9/1fblFopJga2pte9L/hR/oA2WOJ4E8wnpdAfJbgl3Obxf9CF0yC55dsTkZrKB0TPIsrKq11g8lsQGwRLLFWgLVfZGSozrN4rhg3XrFHm9xLKan2iqOSadfYmqVHhAlB1NWDotwjZqCm73Uu6e+ACH+UyFZDIupWG07VcucXYkq8EGKNlNSGzgO73g5scVDaGIh84oKas/ZaENBN+ERktOVt+lZpZr33CfGGxihSDRBprEKPEBeFVjcS52uNJ68Q8fWUJMe1EDkvPV/pEj2edYSb7xVpUe7ie1vtyiL9IGY3KSWqoq17wrbXi544IQOAwb3YooY2OA0dsUa4x/CFdVdx44irhC5s+b3JyhGVggd5FHVwRIHQjywdESvuAZZndy0V9gQehCteQTpActQTYgH4BJCXH3f99YgGuhi8Rd7JbZW9Ayt3Q7PNLbWAKB2eftxQQvY8dj5WjbTx+AnKMf3sjK1EX0zEeCZf+Hu8BOP5/R+NkDXC1QH1zgAAAABJRU5ErkJggg==',
    'KH': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////+/v7q6urs6+vp6en////uNCTrDwDvOCjtJhbqCADsFwX819LyYFLqAgDuLBvtIhHsHQvrEwH+8O783Nj808/1h33vQjL++Pf6wbv5uLL1gXbyW03+9PP96uj94+D7y8b3m5L2kIfxV0nwSTr3o5v2lYz1jYP0fXPzbWH95uT7z8rzcmbyaFvxUkP4qqN0FRULAAAABnRSTlPy9fGan5R5ze3CAAABkUlEQVQ4y7XV2XKDMAwFUNJFtmxTGbMGmjT71iX9/68rS5LGBDt9qV58hzkjzEgzBM8Pga8eHgO4V8HonNL5x7RL0/KjHMOpRsGlyZFU1qU1p9VFBL9iIqOwDUtSIoEB8cJFK8I44iX4RKHkK/jETioEn8i4iUOPaN+xBbdIISczAY+ANy5U6hE4mygWL9wiXq+EEFokTqGYiLDQMvcIzdNK1k2cAmkPoIXMncIU9bGQOnpz3VTNmxMj91z0rDkXVDfxznbcNPEK2DQ38YnuJkNiReYkNhSpdHBPzwIKRYcBMQvT84InWRpawlH/Iqp8v8zaFC73eXUjkiOXXKpmrDsl67x+74k1McZQ0A62JLDOVIwt8U2sLa0XTHSRNpb45N1j1EZjF/nBEoeY9Sv+skQub4R8tUQW6x7QfG4J+CS0ANIX2GKKHK8BZ0lPwDuTV0LqGfQFTCdS4OmT5SoZnNw2NtgAw5eu2ZZGYQ1U5Z5+FilUZu7bjzDiJvNvUEkV+AWE0Bcj8NfoD3+Pxzt/oKcfGW00hQiUyLcAAAAASUVORK5CYII=',
    'AH': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAkFBMVEX+/v7////q6urr6+v////uMyPrDQDsHgzuNyfrCADsFgTvOyr+8e/uLBv7zcjtJRPqAgD3lYz2jILwRjf5urPwUUP95+X819T1h331f3XzcGP+9vX94t/6wbv4qaLyal3xWkzwTD397Or82tf708/3npX2kIf/+vn70cz4o5vzdGjyZlnxVkj7x8L5tK30d2sIyOk6AAAABHRSTlPy9ZednE36qQAAAZZJREFUOMu11dmS2yAQBVA7Ed10AwKtluTd4/GsSf7/74KiySRowU9zHwRIpygEXcXq2yoa/z25l9U6HOuNDl+sV6NJzq4YTTESz4yQRkVOEpqoqFjgOSZ21liDDxHxSKJTlEXEiQpd0fuyqC0dk1eyu0VxAHjsHy+L4gj2IakNvC0JLaDqFwPmsiB+AuS+yQAOC6KjP1uxQzrOi61iVWutLwXL7azYs1BDBJSzogVpsI+R8GtWFNaDgVj1NCN+MFbpkBPyfkbkQNnnAUI7Iyrmv9W1QS6mIgVS/5ZEtJ+IjDH/FBlyPhFa674JRoGY5mtE2bS36/Czt7YpJ+JSAQFx028dk++f61A8nZ2QQlqXJ5mz0vddsQ3EwYk+0phMeNB33UsgXkF8EDRy6EIXiA7EOHwMRENyBCS1gbiyEaNAGoikczKcwr0lodhKkP8DEHq8p7Uk8WF8S2I33XV9IisHYOhdz55cxqg8UAi3pbMtkZUHXC6f/tWyYkxj9bFBwOd4Be1dea/GNslYrJN41vdvj7s30Pff2Ponm95o+dgAAAAASUVORK5CYII=',

    '2D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX+/v7////+/f3q6urs6+v////uNCTvNyftIhHuLx7rCgDqAwDrEADtJxfsGgn70MzsFgT96OXtKhn/9/b+8vHvPy/96uj4r6j/+vn839z7ycT5trD3opr3nJPyaFvwTD3wRjf+7+394+H82tb6wLr6vbX4qqLzcmbyYlTwUUP71NL2kon1hXr0fHDxWkzToJwiAAAABXRSTlPy9fGYnywJdDkAAAFdSURBVDjLhdVnb8MgEAZgpykcHCPYjvfKnk3+/8+rnbSVXJnz+wWBHh1TIlh+BFQ+lgGbS7AY93e3YjywCEZFVtfIREkzKjESpUYjjFVfPhFzaywXQqqDRzTKyCosNwJbj+iUyobFWLuZFmvu5HpowSXxpMiLInxtOJqoMUqm9YUWncWaFA0KuaZEaI2qGSWe2nWMEicV6T0ltsb0t0KJFi0wShxRYkmJWAo8M0o8lEkYJQ5aujpO0zRMPeKqATbQJ4LQI5yURvTRwiM6Jd5xPrEvV7/JR2IyhIjnxDYJZ8Rd3WmRCmt3pLgg4JMSGUrO8eQXhRPAwei9T8TggHMOzvfW14nuwUB0sp0SmRzAD5GnCfFQ5k9E6pxPzFI5AW8gdDa90qM28KqAlW+3tZK8j2r8J3ZG4IAtderSgpU5JQ4osaJvv1UXRoubWs0IdmT/xYLRWcz/HnM/UPD5DWI1K6ddiYUTAAAAAElFTkSuQmCC',
    '3D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX+/v7////q6urs6+v////uNCTtIxLvOCnrDwDsFgTqBwDtHQzuLBvtKBf96Ob82NTuMSDpAAD95OH7zsrzcWXyXVDrCwD839zxVEbwSjzvQTP/+Pb+8vH+7+75t7H4r6f4qaH0e3DyY1boAAD70s76wrz6v7f3oZn3m5LyaFvxV0n2lYz2joTwUELzdWnDwyk5AAAABHRSTlPy9Zif9duHSgAAAWFJREFUOMuF1edugzAQAGBo7fNmQxgJM3v0/R+vIakqoeDz/bFAn86+sy17Xx4eXx5xhecvv4Nws/zhe4skm1yDqU+LFAuxAQkAIr5bRcm1oACa7y3iUBjTBWkFcrCIfJpuzyEVPLeIoboGczkRLz/EItwiFTJDxC7ZP3iRIiITIPSJICKPjRoJJi5SKcgwkewPlYjPiJgR8AoX5Mp1gIuRm3BVJNdqeNdjE1sVy3AetaK79VkyCfUxSUslHpaVhhxARaBo3Ntq6SV/nUKkY4dSG12fiUUsbgPaD5doXGJXBw5xm+64CECpBBVjwXiOiU5GlMoeudkCGGUgQptomGCUUiai7brY1vwJZsLZ6u530Qz+iO5XxBCbf2Hin2ZlllYAewPg3fpKW27YKwNvbdWeZDTnkGd7x7KCUVZcsK5HiindYOJYaNkS/O5PI8FFKFOHIMePU+gTPHz36+F8gb5/ARskJp0gim9mAAAAAElFTkSuQmCC',
    '4D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAilBMVEX+/v7////+/f3q6urr6+v////uNSXvOirrDwDrEwHrBwDrDADpAADtIhHtKBfsHAv6vrfsGAX/+ff96OXzcmbuMSH839z6xL7zal34sKn3m5L2lIv1hHvtLBz+7+382tb7zcn2jIL1fnPyZFbyX1LwSzvvQzT+8vH5ta/4qKD3o5vzdWnxWkz70c1tMRE8AAAABXRSTlPy9fGXnUWfCdoAAAFVSURBVDjLhdXZjoMgFAZgnQ6yo2i1LnVr1e7v/3rD2GQyNXD47yBfWA45Idh9BVC+dgHyJQg3E+kw9P/HYbBd5ChE87HEVqQ5UR0oKs4fCBQToQMoipjOCBQjZzUssJYZKBJCSgSKMyEJyl6vi0sUUi8InYRhDtEwNhrB6MElFh0Xpu48domE0AmBoiS8AkWm6Pw+jc7souHyea/repKqTKziQXBLTCTGYrKKUtA1yoizVdye+zWzauf7h9hmZLpHoDBVP8DiyLRHdEWRgmINJFKf6JbMIxoxwiKTcZyB4swxf0KiYnkUsZtbFFriKJK0d4k0ogZEWOeObrgsxIBfQnBnE1VODXgT2t4s4iTUn1DikVp2uVKJ30DSytUvahWKX123vbPcgJzV7ortGY4wL6Gq5zGO2xQSCWvZFX790rQKLHo2eARK0FaECE7o/z18P1Dw/QNJ0CyTfUnQ7AAAAABJRU5ErkJggg==',
    '5D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAgVBMVEX+/v7////q6urs6+vp6en////uMyPrCgDsEwHtJhXvOCjuLBv96Ob819P3m5PtIA/pAAD+7u3sHAv4pp7yaFz/9/b6u7T4sKn3lYz5trD4q6TxUUPwTD3+8fD7z8v7ysX6v7r2iH3yXE7vPi/83tv829b2kIb0e2/zc2byY1XvQzZ0rtT6AAAABXRSTlPy9ZqflLAYH7cAAAFLSURBVDjLtdXXboQwEAVQ2ATP2GPAmM72km3//4GhSGGjMHZe9vJghI7GbSSCz8CdVRD5EoTze7aLp+yy+WMYvBRpjJwC3UuJXwJIpn2syRmRgJiCNSvwqseUrIC5PCcSr+h0V2UuQWsFKBuHUP1uSZrGIazcUEr4xYijMbSP4nMKR0YciiIeBsTTkpiTCVx7hMLNG8WxuA2DJnYdT2OqfsgRC0bkIEV7qYkgYUQpUFoanoxb6R5h6FKoInYv8YkkrfcRI8boWHMdNMcnSp/YPn2zNObqFjq1VjvFAxScXKIGIQTkvLigVELIvpH5e+mBUEjbZbE9Qw9Gcs6WRD1VmAh1C+Jm5I8gc1+apcVUTSDFenmlFcpRELTcbnMYBST8id1BCQWF69SFVZZKlzgAQeu+/cI8IreIYecRUfWnC8PInfAff49V4M7HN/sKKY9/zk1lAAAAAElFTkSuQmCC',
    '6D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX////q6urs6+v////vNSXrEgDrCQDuKxrrDgDtJBPuMSDsIA7sFwXqAQD4p5/+8fDsHAv96eb82db70Mz5trD4rqfxXE/95OL839z81dD6vrfzbmHyaVzyYlTxU0X/+fj+9/X+9fT7ysX3nZT0fnPwTD3vQjTvPS3+7uz6wbv2lYz2jYP1hnrzdGj0dmmUS6mJAAAAA3RSTlPzmJ+U3IeQAAABU0lEQVQ4y5XV127DMAwF0LQiqW073o7t7NGs/v/n1UHSAm0iGr0vfNABAUkUNHmbsBnWxVgmf8kqy9Jf4El05E+sqB3KmBUXoovgxMoktmLFhlQrWLFzuGbFUeFcsKJX2LAiTez+WDSM2BIcNLouKJagQSfS+M+QmCFYs5MWVBYQZ2UPtWhA0zYgWlT5UHKFZUCUKOuhTCXOA2KuYDqUD8BdqIfSFd/jqnAzlAhVGxAFSR1lkZQUBcQCUDrtpINlQIi1I6014Tp8t2kPBsqGndO4itn5eIQXyzER76cj4uTPvJham1Ss6AlUyYmCDABFYVE7DQAa05BYAcItzsSvRbxXcI86fLwShUH4Dsrohdh6/SOkvy6ehcjRPoDFIvCilL53UHlot5E3N+E34RNraQDUcaduEkjkghMzkpTzt9/5XvAi9dmIEDPx/0kWYxn9gd6/AJOzH2Pa9OwIAAAAAElFTkSuQmCC',
    '7D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAh1BMVEX+/v7////q6ur////s6+v////uMyPrCwDvOCjtIxLsEQDsFQPuLh3xW07+8O/71ND6wrz2jYTyYlXsHAv6u7X+7ev96ef84N3tKBfqBQDoAAD/+Pb+9PL5ta/4rqfzaFz95eP82tb7zsr2k4rzc2fwQzXsGgj7ycX4pZ30g3nzbWH3m5L0fHDCHZEBAAAABXRSTlPy9Jj1nwPg9hkAAAFBSURBVDjLhdXZboMwEAVQ0tYz3kLB7GtWSNL2/7+vJpYapOLxfUJwZF9sS47eIjL2Owslen89V+dPl3P1ermLVoOYRrg0+9UQa3FALWxqLX3iS8ISDsp4hcjLskwlPryzzEvFq5S5R5gWb4z1fIbCI07JrWcsUWrPPMLlgpjSwtgapOh5pgtKuBqkSG0NWuyVSkhRQAYxKXJpaxDC1aCEq0GKQtsapBikMowUD8QLLSbEhBRHnfGYFH11dIDc/aDoQiKeyoC4tCktSpFlR1IY5Eju7YgAgHe/qJTgAEKefKIDZQFwpeNtEU9owUIkL7bECHIBjuhxQ/y09Z+o22u3Mctgezog5LjdNJf1U9Q4+P723jxF8+1fsSty4HigVh1mnumOEglqHOjdP7SG0eLUnAOC5f9O4Y7R2YVvj+AN9PELgx8pbWBfKDAAAAAASUVORK5CYII=',
    '8D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAkFBMVEX+/v7////+/f3r6+vp6en////uMyPsEQDsGAbtJxbtIhH/+vnuOCjrCQDsHQzqAADwQTLtLBv+8vH95eLwSjv5trDxUkTvPS37x8L5sKj+7Or829f819T70Mz6wrz4p5/3o5r2jYP0fHDyZ1r+9vT6zMf4rKT2lYzzdWnzbWH84N36vbT3nJP1gnfzcGTyX1G6YMc+AAAABXRSTlPy9fGblJD2/nIAAAGCSURBVDjLjdWHboMwEAZgkpQbNmezAtm7GR3J+79dA40qoXBOT0gMfTLmv5OI3oZRqIajKH5V0aB7P1/Ouw8GUWeRacqG02NniY748nQX5E+aWGBmTGJMJktFnMUkk9mkNnJRREk0uZ/eicp+YVOsbXOuMbVhkWoirtAtmkgcVso+1p6qIi4q8hstj4shqIDMRU2sIGBioEITecmMiMxlrohvz7w9bjP2u36Rg3NtYsZB3ismhPv24oq06hVjkm17cRKcKgJ3v/tBHPeKpTNwaFIHQ+9KX5wrVx+rilxttdTBuebQU989pvCsph6vy4yzcqNOctuaRdMURXRLF4dXIk9nL8TJb8NiZpwrgmIvidxCYioAIGtdfJBJABjnmjgA3gEkBLkyhWkDGoL1rEfYKTTgQbKjfRY7z3+C/dn2TSGax1uMMqfxJ3IrWMba126kFfKlJ3aVBBLZBzK1QInLbKgvK8lkHO7+3t/isFjI8tUUfj5N4SAO1+Aff4/RMArW6AfJAi057sBILAAAAABJRU5ErkJggg==',
    '9D': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAilBMVEX+/v7////+/f3q6urs6+v////uNCTrEgHtJxbrDgDvOCjqCADtIhHuLRz96ufuMSDsHQz4pJ3+9/X+8vH95ePzaV3sGgjpAQD83936vbfxVUj82NT7z8r6wbv5tK7vQzP708/5urP4r6j4qqL3mpH2jIL0fnPzdGjyZFbxUEHwRzjxXU/+7uz2lo28qMwIAAAABXRSTlPy9fGYnywJdDkAAAFfSURBVDjLhdVnb4QwDAZgrtfEdgbz2Ov2bP//32sOVZVQifEXC/TIiV6IEmw/Aq4+toFYq2Azf6721fzFJpgN6Z9o8NnPRszEQYMToHufyGyMsYyxiFKPSDRGdVhb1IlH5EQn105EuUc0gDvXdgiNRzxAhq6FEh7eGXaaYaHx72NwbSC4e8RZo+zSThq6eERo0OCIRkHtyzQpYwRURvkSc0Qa9Yz0VfiEq334KIqWE6IDeglWNEA1K/ZQjIIVN9JnVoQmthkrLpqughOZJWhZkVCRC1bsqkrwwtW6SNdENoYr4ru8iLXE4h0r7qToxYmjjqTUiV9UYKSUCK1PpBKUEwqibFlkIznwJqS+lsQxgglMU+xpQVxL/BNY3pZWGcD8rmLguLzTA+EkkAbvmdPRW+izP7GbVlJRzqUeFSq2KSd6bfWB//p5eRe8aMtuRYj631+4EXxt1m+PtRso+PwBicIqw120FlsAAAAASUVORK5CYII=',
    'TD': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAilBMVEX////r6+vp6en////uMyPrDwDsFQLrCQDvPC34p6DsHQv82tb71NDuOCjtKRj+9fT3oprxW03pAAD5ta7+8vHtIhDuLh35r6jyaFvwSz3+7ev95uT7z8v6wLr2jYP/+Pf83dr7ysX6vLT5q6T0fHDyYlTxXU/xV0nvQTP3nJP2lYz1hnrzdWnzcWRDPLnKAAAAA3RSTlPznJRnYpscAAABWUlEQVQ4y+3VWXLCMAwGYKgsy1ESB5x9ZS9L2/tfr4F0CgOO0wP0f9HLNx5ZetDsbebOfAZTmT2QwE+upfXy4AHcha8Yc4BNKBH5EL2KmFHTDtYSNbM2y1cRkhYyhoK0PpacUfwiFJ6E3EDGugHoGItnkWhvK2VQMX4MvnwWVQMxysCX6EGfd1LRoxjiDWLxIxK3WJKK/sVfJraWdBNHUiuraKJhqYnGEqyiBcXc+Zsj094uYuiMkDpljYFFnI3xYFUaTlNpLmARsVJtX/YqE+UOLOKeugZwCwCniKZEcqonxMV0blGlzLVTHEjh0iVyEkKQNy7WMg2FSKU/JiIheyBCmSV2kZywB1eCqrKJXAxgeGVnEXujf4U2n9bd9n0OIJW5vdMt6pvQGI/9dkE3QefxiX1RKEIqXFMXHHK2comWMord2y/MAdzCp2ZCwBYcwnEa3GQ+dYG+AZPLI0mDPEyyAAAAAElFTkSuQmCC',
    'JD': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAflBMVEX+/v7////r6+vp6en////uMyPrEwH+8/H/+fjvPS3uOCjvQzTtJxb82dXrCgDtIRDqBQDpAADrEAD96ObsHAvyZFfuLRz83tv5tK74r6j4qaH2jYP+7+77z8r6u7P3m5L0fnLzc2byal3wSjz6xL/6v7n3o5v2lo31hnvxXE7roEsRAAAABHRSTlPy9ZyUBmWbxgAAAQVJREFUOMvt1cluxCAMANBkGjsJMTvZZ5+u//+DnUmkziEsPVf1AQn0ZDBYInvJ4rHLilRk+XaNH/hzkmfbJBVTUD5T+EQtUqLB+l/8TWEJb0VA9KfH+GlMGxCvnZ45nx2oKSBumiERgqmrgOhrLYmkkrzwiDXOjAiutgiLouJ8LTRabUqUKbFvbEJ86DkurHTORkUrmPiKiVEAgJrCokfJAAh5SJSAdwAMofSLsjF38CCm3vvEuGRYCXaTR5w1/QjSF98uA0q2Aomj/6RHQ4sgMYSqfVeLUKfwjV0EAyba2K2DY66rYuJNdGKIv/5Vt4n+4OqQ6sLjpgvzRCfnv/g9dqkf6BtMyiDJH7rkcwAAAABJRU5ErkJggg==',
    'QD': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAjVBMVEX+/v7////+/f3q6urr6+v////uNCTsFALvNyftJxbqAwD95+XsGAbtIA7rDwD5s6zuMSHrCwD0em/wSDj/+PfvPi/sHAzuLh3+7+3839z+8e/819TxWkz4r6j4qqP3m5L2lYz0fnPyaVzxU0X6v7n6vLT5t7H4pZ32jILzcmbwTD77zsn6yMPyY1X1hXrImWWuAAAABXRSTlPy9fCXnURdY+0AAAF2SURBVDjLzdVZc4MgEABgkzTALmDAKF45jOY++v9/XjXJdJJ2lYe+dJ9w5nMXlp0hmIyDoRhPgqkvgtHbpyzzH2AUvCZZnVOA5PM9xauIkAOAxiTrERUarUBzgQ0tcgVqvi+vJ27wQIoj6iS+F+PqTIoFD/f3RZxqyAkRJ4oVj2XN1ZYQkvH5c7kJ+eyvIvoPQkqfaFxW2VbQHcvaAUA+b0Bte7re/pk3IUBqZM/Niazrl2JqQd6cdKCS1eUaaZPCmpygNRrNtXLCKW2jmJrCjQ0BQGG91ihIMd0tBJhzNZ0uXUlV6SLLH6coiH1QQYvYJ4pEekSFx2EhtXNyUNyssIshsbSGMTz0i1xpwQTwsk/EggvGmFCmoEWRhC3oSJhklFiaLsOD8HRFiAjhWwCeqCozrp9VNF/SO92FIDoAdtZ32gOaLgeu+zt2soIJWw913Tjh0nhIbG1q98O3X+PNMx8lXnxTuPs1hSPPJI/8r4fvBQo+vgDoAy4i+bpYUwAAAABJRU5ErkJggg==',
    'KD': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAilBMVEX+/v7////+/f3q6urs6+v////uMyPtJRTsFALvOCjrDwDyZVjrCADqAgD/+fjtIA/81M/9393wQzTtLBvsHAz96ef70Mv82dT1gnfxW03+9vT+8fD+7uz95eP4qaH3mpDtKhn7ycT6wbv6vLX5ta/4r6j3opr0fHDzcmbyYVPvPi72j4X1ioDxUkPtmXmnAAAABXRSTlPy9fGYnywJdDkAAAFpSURBVDjLhdXZjoMwDAVQ2o4TJ2Qp+w7d9/7/7w2lakcjiPFLLHR0RRJL8VZLj6rlyoO58hafLtim5bsrt+nOfD4vvG9IoTF9d6HUxVd4f+Kq4vXQ3DXyCiaEL/kgMuRyB5QIUeVAiYfCG1BiLS1mbpEC3FB1QGU8dHwFt7AmkRwDQoigQIGdW2D45JxHvHYLxm0YRionRCSDvdrw2imEzsEwrnKniMN+OamNTVwCt69VWOJehqM46cgm5N2aPuRMCmhVH0IK+BfiTc1pq2OZkRnw5PIyIeqm+Qy4CZpgJEblFuWcqMJkRnT6TovExnFDCl8JWVDiqBhjqnWLALlggmPmEqVA0WcIZNW0qELZgxeRt3pCmCPDAQwpUWvG4qz5V2z0ZST6OqAVb2DxOP2ne8nFkCAPrt2e9JChO/eJXZRgQvngFobFIo4MIWCnInWgb9/XV6BFptMZAfvRFC6ArsX86zH3Ank/v1puLKCI4Q4uAAAAAElFTkSuQmCC',
    'AD': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAyCAMAAADleEJoAAAAk1BMVEX+/v7////+/f3q6urr6+v////uNCPrDADrBQDvOCjsHArsIQ/rEgD7xb/sFwX96uf95OLvQzTvPS7uMCD+8/LtKRjtJRT83dnxUULuLRz/+Pb7zsv2lIv2jILzaFz+7+35tq/4sKnxWk3wSTr6wLv6vLX4pp73oJj3m5P1g3n0fXLzdWnzcWTyYlT819T4qqP71dEGDBTQAAAABXRSTlPy9fGXnUWfCdoAAAF3SURBVDjLhdXrsoIgFAVgqxNrg4SpaV7Ssvu98/5Pd7SaOiXg+uPgfMIW9gzOoO/Y0h84w644vc+xl3ufL3rO1yQr8r+m+BK5dDGyigIMB6tIRChWNjF10zgVvxaxBzsHNLOIJfnzhBKzGMcohydyp0axBfbDG7AzihL1516Ei0nMQyRNMYg8g6iAon7MgK1BnCGuTb0uSr3IuAq8SZ2VZBOtWMuQ+0EQ+EGIhVYcwUK3Scpw0go/jsQjUcQzjRgpkYweWQq50YgCryPbEY4akSiVv5pA+W2RSwTvbgWtW6KAfLffTOLQEt54/L8P6tFbGGIUWZeY+16HmFFhF54rxNgqLuBY2kRFjDHamsVUxbWI5dQkMqZ4LbgKJ3ox8VGDhshgrhNVKGvwJOlNI44Uv0REJ90qC+U+V3Flpa90I+O7iLAw/e2O7oL25h07gzOO0rbroeAizWxijZQW9tMv6dLRH1fKu7pw0+rC3tCeXvft0XUDOT9/9LMtuqmJOWkAAAAASUVORK5CYII=',

    'RK': "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAAxlBMVEXx8fEAAAAODg6Xl5dbW1tMTExqamqIiIi1tbWmpqYtLS3i4uLv7+/p6enb29sJCQkGBgYDAwPu7u7r6+vX19esrKybm5tgYGDf39+SkpKAgIA/Pz87OzsxMTEkJCQZGRnBwcGwsLAeHh4QEBDQ0NDLy8u9vb2ysrKEhIR4eHg1NTUcHBzt7e3m5ubU1NTFxcW4uLidnZ2NjY18fHxwcHBoaGhWVlZSUlIWFhbk5OSpqamlpaWenp6KiopNTU0qKiofHx9JSUntFXKJAAACJklEQVRYw+3U13LaQBiG4W9BlAR1oQICRO8YA6a4O/d/U0G7CiFoBTtyDjwevScr2P8ZlRkJWVlZWd+3wdZtp7W2TgiZAJU0uEWOGVWkKkfCbEBZS+NFKvwD5vS4BMJX8CifY4eurhi9/yC58Rl+oOtUyFa1cPblLy7S9ScAx2/UZChX8JrOShz8yjauVUrCZo8eVFPhCqGV/z92D81+WjzXCfHT4vZxw6ikxG6400mHFTqbT4fXbDSG7RNWE7FJ395gHsPlE5Y1ejCI4xrdqOMKxjZcG3G7CMIN3QOwNMLDMQdbDz1t58Xxlg78YhehE7Kbn3D0SjaSX2GDsGdN8zr0vhjuoBPdkWLjWHt4YZUuoanAn4+lCkyjDyC9bl+BQ2Zv+3eSv8AjZmfsxWhO8qVCT8Eo/PQWcWxZKwMVdoYY7hOazzD7sQHa7t4Bhuw6q03Cxxa75cI5Np5BGwTG/qX8VMyRBIwWe6ARjpLaMvobjUQl4gL9W6PPym70SJRO5S08ic5lIkxeTmYxk4ztaOOjOHpk3jkIY/VsU3uXVgrg+aIY3X8HehsLli+K3cuRpgfvIIhXJKYt1ASxGh+qYyiI0YwN6VJLFNd5c6L47jMYjc/gZ1HscLAcCGIbnGqCuMrDZlfItsBtJITrMLl6J2CDBe642Grdxk+w+BjqzdsuATKQoKdXqebgWubrLJHqkoobyf3Oauy+FS6S6vcKsrKysr5SvwEHiCdzMp8KKgAAAABJRU5ErkJggg==",
    'PA': "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAAsVBMVEXx8fEAAAAODg4tLS0eHh7j4+M8PDwYGBgJCQnu7u5eXl7Q0NCUlJQwMDAnJycTExPp6enn5+fS0tLFxcWgoKCBgYF9fX1jY2NVVVUFBQXr6+vLy8urq6tQUFBHR0cjIyMgICAcHBwCAgLg4ODW1taxsbGurq6lpaWdnZ2NjY1sbGxkZGRgYGBbW1s2NjYrKyva2tq8vLy6urq3t7eIiIh7e3t4eHh1dXVLS0tCQkI4ODj7Sk2IAAABgklEQVRYw+3W127CMBSA4WNGJgnZg70pUKB7vf+DFZtGAiI3hxNVatX8N0Ycf7GRuAhUVVX9oVqWHVFtjx1aE7HFscY/6c7VuMF4TXDiycTQiXjNFwOrvGcjOMEKX2pI+5AeNocXmCHxgO9tE3FdXPNXYTOKPNBpeK4xpt0TT97xRSFi8X27DK5V+N9jh47dEif32dijYl1jLKHiBZ9RccKHUxpuanzYpeGQz9STa6t47Lb57DHD7CViePzKR2lH4NO6YBdiU/ziN8hhdZ/dYCTFx7sFGc5XV5gMh9njJZgnxUMxsYGE38VkQMMbMUlouCUmCg37YpLOxIMM+zrsHkeWD6K7p+EVGEZfw3G8Wnri/Boe98/+VRsfwByi8YqdNTFcmKlYbLKL9lPoYTHkjhkAWFhs5Pa1TBWLF0wSBuvtEhjiMthHUPnb0AcG2xI8x+AEJCkIfCPD3bTQqg7ICqkHi/oF9ha+Sd9SrainSek4gKI6QbwbNXJZ22UHqqqqfrRPyKkXrmVtzpsAAAAASUVORK5CYII=",
    'SC': "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAA0lBMVEXx8fEAAADv7++Xl5cFBQWTk5Po6OhSUlIQEBDt7e3j4+Oqqqo7OzsjIyMICAjl5eWbm5uEhIQZGRkMDAzZ2dnGxsZ/f38fHx/X19fNzc3Dw8O1tbWsrKyHh4dubm5oaGgxMTErKysnJycUFBTr6+ve3t7T09O7u7u4uLixsbGlpaWenp5eXl5bW1tXV1dKSkpBQUE+Pj7b29uioqKOjo57e3tycnJGRka/v7+urq6hoaGKiopkZGROTk7R0dHPz8/Ly8vBwcF5eXk2Njbq6urIyMiGkUpQAAAC+UlEQVRYw+2V13biQAyGNYNxBWOw6b2ETgIkS3rd3fd/pV1rxsfYsY3O5myu+G/G0tE3zZIGzjrrrG8XX7RL/wxvGWM/gKC9Vr99i7o85ssgwEPGmBsNvEeYn2ZfMHAW8dV8lwun1UJ48+nIrEiAVYSVyB4V37UDgoZINz/N16fAfcZiP2aKnhsKvGDxE7bR06HAJYYyQ08ZHVWgqIixD6HjCh17ErzGWC10VNChkuAlxuo8sA20FSDpgkXPuGfUHAm3WQvMGZq3RFjD6EJg3qDZI8I5cUhVmpdo5ohwg6EsaQ7QmgNReiSZXbQaRFZmxUgYJkMZVPiRoTw0qvhdAaqaAs6j4TBZzb+6Wo6wPnePMrSH32vgu7/DgBOWHogMDS+gJvuTQ6lLLK3fYZFdQxfHMqjZOWLX3+BBLMeBKwi14ElOAqBm7H3kJ/LElRk6DXrDVk7ibZndzcovewIFmaEWMmOAOn6UYOcPdylwDetRZLQV9KCfYCg4CRZs6sPFcYW2aKL9oK9cwqucZC4uPxEVfURXRV3WVXWEwR9y+xvIZdQYH4ri9cQl2zaTwY9yuwcc7xNQQ048FUcNNYO+zBEtvQ+bFdlErCi8h1sc72R1XyTBm+BlMZVj1g0SzRTVbfME9pqhXsR7F2ooE02X78kovfXpABwuj+FnmWgD4BVx6Qk6yJAJRG/sCebyd8NKYUx/Te+bBR/uHMMaeDj6Od061DxI0nvYcrxjuGJgTyiWYGHgozJPf17xKe48amO05JJWr1uCpj0q3y97ej4BlqUzdky0uLWTsLISHaYo7UT4OQgurA+O36ofdOnZ5tvLJ5tlwLGsLFgA1XHElQW/x4KeTWgrVJjrsaiCAWsqDFfxsDI4ZDgfD7M3FTJcSjghGYarr8DOV+DJmALnIFkfFNhKgVWdADchRTen2TqkiQ9PwitIVcM+wRY4pGuRnSlFE7I0c7PWvYBsmVoaOspxOCm1UW3dzebtRcdaXq9WOZTTmcJZZ531n/UHcj0u3hhm/MEAAAAASUVORK5CYII=",

    'UM': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'BL': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'SN': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'LM': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'CW': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'BK': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'KT': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'ST': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'RS': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'BF': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'PG': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',
    'RB': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABaCAMAAADTj8UHAAAC/VBMVEUtIYIqIoQwIIA1I380JII0J4M2HXw0IH0yH34uKIcrJYUjE1UiEVAyKIYqIYEkFl0+Jn4pIH0kFVojE1giElMlF2MxJ4MrKIg0NIgzMIwyL4gmGGgmG2s1YaEoHXMwLIUtLYkyRZU0M44nHXczLIwtJYUqH3k0WJ0zOpAnHW4zQJM4OJMpJ3YqKWwxJYEnHWcxSpgzLIkwOIEpImkzUpoyJYQqJHEtQZQsNI4tJ39iS9w4c7pALZY7t9xOb9ksOpExHlk9gMc4L5A/KI8wJXgmHmAqGl03qdVWMrBEMKI4LIErLXtNM2NmRGF1QOc+ktpoONZIVL1IIpdAYZNTRXcyLm9DLV0+i81pNMo/SJxNS4pGOocqLoRYMF+BafFUQcI0gbhGWLE1TqFgWI41Iow7TYQnL3JCKWp9UGY4IV1XxuxB2+J0Xc40mcU8j705Ya1kMakzWKhYLKU+QKQ1ZqI4PpjumIM+MHs2OnhKseaDVdhcRtJcN7o2aLRTdqw/aaY7Joc1Q31GOXkqOXlWNHcwMHc3JmluXPtSlfVahPRCyeQt8+BVttI4nNFKZM84p8qEmMlQn8aFUMJNPbVATK41TJTfpI02R4s+G4FBs/WRUPCDWOeVc98wzdV0N9VJxNRpcMp6RcpjM8JcSr9vSL1IhrdgKbZdaJk7VIxDHow6NIqUZH4wGWZ+P/dSd+Zd2eBjY9tKpdQ+tcpDY8VQcr1DcauyrqlURqlEPpq4e4Pni3tjRXWLUmrIS153ivRiv9l9RdhyidayvtJeM854scxkmcx+P7lxNLlTj7hFIbBMXKRIf6PSsaKubp3uupD5p4pvS4H2h3Y7JHM1G3HcdW9/Nm5jMW1bS2ZzpOlS6tqPaM9ae8ldXL9qjr14drKklKJXK5nDh5bTlI1+Z4ikdYWzYXPEXHOdPmNfsvNwvOlQiOOT2eF12uB7ddtwQdROJcy/28bFacKGar9hNpeGRpWMfpIvQ4Tt1oGcS2t3KVahUNTLz7iMrqS/sset4a2kfmcgAAALSUlEQVRYw7TSv2+iYBgHcES5qgnmbkEkMQjxAoGESCKnp0ijXoLeIHXSTlrjahOjN1mTbraDOqhDlw7ddXG5/f6xe/il0njV+/UlvO/rw/PhDSDy/i+CvPuL/DPs/0qezFf/Ib6w8+6CXMWHw/iJQMeKunDjYmw15HgKOxlS4OKrwDsPprk4eQElPxwwwgQnrJ2V/cOOn4+nsUOMcRwG1GSnAk9IcenAAV5xcK8zsaWZPSbj5LkWNETgqB1epWE4e2eIZG+NBIPBAMcH7ZyB7UYhbS5MTHN00Ik/cCIOJjnMwRSsfg8DoXZYCro5hZ02Ov1/MclxHO+poF6MoiZGdwlg++QLhfktf1AJOE0WRlELp5VjOCBM9eltIX8EoycxNjaMYvHJEP8Ei5qm3WweH+t7LO1wirYxSqWJPZYwwg7WyuVaWG4yqamEW0IPMOpiCd0HI2yuXl1dVT9V1uv1i12AjU9jJ5WXl4pfum8+Pz+XAULOwJLdSF5fX4viN/H78ubmGvsFRhA0kSJgdgIYh9DtXq89xAiJmc/nvbRi1uC1uk0hwPYykfqIHEQx+1K6rvcKjIK3zXkqW3jfAxj5JZbz8I2NpyL+zTCMxtNTn3iFmTfwAL7x9vHhcVCHWXt40EpQRA5xwllRXizBxuY3Hkwmk1EuN/regsXCfGQPPr4zguNV8xtLP9Y1mO+Icm29rqm44tk55GLGixX5rtmsYJL63Gw2X1RCqtRqtTv843kYL5fL9wQu3ZvBcCJQhuDIWfgjTkDgDRGfP3/ezYQHZ3Y4g3iDH4vyJoY5gWSjiUSCYWI4n+JjTIqVmRQvU4wg81kknEWi0GImnInucNiaenov0e7k+/2C3uan82GnoN/yuj4VxrreGecjmV6iqE+zCCT6Gl8a43G0aOJivsEYfbLTGBtCI9PoFOP58RjqjWyjbzBHcWi71ZLbQX052GwZcTMTB5tNndXE2WC2rC+X9dlyltS0jRhxntmLWz9GoUUup1YXX+jSaLSothat0kgdVaFYrebgz9YdJVuqhS+tnX0+H2AfJKKUlUik2/2gdEMxSlZLyZIMJ1WSabVEIF0IAodFopdRGPcY9IcInB98kUgoFqNoykzMGmk6BFesS9BziGEF2JNw7HVC3oZXOGzHwTIbY1meFyA8z7JszL7gtr2BITJQgWHAmiPPJt27noN/Nk4fQEnFcRzAsR2UL8DX4oUVQhHvRUFHmVlawBlQlwiWgJaZmCM1y3mmOa/MSq3UUlNzjzvHqZVme++99957r+v3INtXfU94vvf7ff6/n3h0BTxgGLDBQ+AyeODwHwZzuVTqr7grGe5XzSW3No017921DXPNXb1G/YYhbesBbstPf7K5628YMrzPj+n/Fzzqd9zVBtKnD7wNhhds/T+4rYsLZCD8bHATwG9ci7b8F6bajB8v2HA5bXuaQDDexsISQu1vYWnBNWPqnzHZBoO54wWhbpe3b/+UJhg/3oZ82MtAhXeuKT9Mpo4CwjVjGEBV6Ht3Vwjc9jemNW28lhYaGgrYoheUSNyTDMWiPxUMxbQP+XS4jc3w4d0t+ht6Vl0rq9Tr9sfs3dl0ZfulmTNnkvXeg4bYwlej+3AylnSDJdyQ2MIAh/QVkOnTy4JuW7VRV6n3iinZu/PBgweXgoLmWkNjUGQsifsIQkMFAltLA92EIQZLikK3P9YuaOYoOqSzwlZB4sAtDx8+LJkxox+phk0dyYPLkCDHqY2xWdYGOo/Ho8ADSyp9qL5sZ6p93dTB9B6d6fTwRIVeF5OenlpTUxMYuHw+D5LYIrWl8+gzl9rb56fGrDLQORwOYDK2+ufbUvNTC5wSb99ht7/V6qMIb6ivzz937lx9ff0MsD1ut/oYvG6derJseX7qvV0uBp4Jg4S1FMW5tRce5RbrTp22tU48LZW297TPy1uyZEleXp4dj0O37nHclpp451RpYe3oR2kxWWbM41nHxlrzeEUBhVHnzxdX0nnt+vanW9InhATZ+y8htWO0msVnczg8DptNKZ2WURjwyE2Jc0jM4fSuq+vL4QSff1wYlRHchWO7Pz02nNdFvedEZVDgnEC7eep16zoiKPhxg5TtSndvXVuYMcZ0EhswfdgA2GHF1t0ZGdP28UY53kuJiZdmV+wJzu6UnZ1doS5dG+3EQhB+0P37ywft25Gxe+3oUJAmzJNKpYCtV0yZMtp5WNDSFL+UA/HtsysqgvcEh4SElE4rLC5yOoEgYxYvXmxvZz1v9JSiEUo2W6kEzOaF63ThbLhzc3YTLbAHmzdfulCdrb460am0NHr69ICCi+s6MZENHh4NKyfi+GA3EZvNT7rhhfMpbGm8V5mLEjQuwjl2kSn++Y1KjWJaSEWFWj1x4j6nooKCgKjjKvmEnJyQeRPZfL6EDUmqOmrGr5q8cB7YLJG0xL9mZ+6hlvj4MY+nhahfvHz5Mrg4IDf3hatKjk4oDOkAVhSblSQSKZMkfIwi1Zdt/3C9xRtPvFFW3vfCzitRUdcOddb4hAdv3fr+2bNn79fkZGgczsgRI0zEMIw/yK9R5OIiwnHA7HCvpqeHM69ojt4sK7d5sufQoUMZj9tjfE28yOtF8O7gq0eTXc/QqsVCCS7x8QEc0U8EOEKSgLMofMnNgwcPv3mTebQsS4TPejJl97QdE6BTqZRokmVnHBhn5EJCURygx1g+t1t9sePxJzBcdPT2KYzFomD8jUeaww6/yTzYdEOK47yJO9SoTCzRSjAW9jXHj4fk5noJ+cpEdz6u14XzMVxzxx0aTPhYWGbmwadPMxMxdtKp0yiKrQ94FQ9KlCVhsVh8kUj//LkuIUHCMjjXXr5S4LxSBGMlSgkFw3yqToaFHcs8+OG6CmMl33mHj4k+PzlqH1+iLalLInF5uTY8XPs6gRVaG7AtrSnt4thlRgRGsCksDLhpeFhzIovM+pycqMmT1UlxEVmryMniAwfEQiGRYMSX1W6L3PLZP/WScxCDAZxCHs1PrroVdvhIWIsQ7tD1QIv34eUu3oSQhQmT3378WKU1GoWocmzBthT/c0v8L0+1YzBBk5iFSfjJNw8fOalCyVQGRIWg6KC6QG8CZQnFmubm5jCNEAqYp3PBrsj8/Et7nV9bMWViEqOoUGyM0Ly6eUaOotXuyUapPkIrtls9NUkoxlCi5eSxY80qFBGOmDHCzbHhYnqDp52IRpPJEISCgiUi4rx1XjqtmBCebnVHtd4u3kl2qx2NhBCKEZtOntxUjYocIyPtA7MUlQNEYmY3GsJAzFgc4ZKeWuId51N1wzsuGUGJuPI47YzVc4yo3FdoTNCqVGLCc9Gi2sn3/f1dxAyCYJLyKyYat9T4+dXHasKu6wiCgNMIsXC+/Qyh/G2rexx8bnD6ts2LFi2OTNmy1zvBSIhRH42WXBtBUM+7fvfOnvX3PzC7RYwiqFD11h1BxAliBHF/59Polx5HlO89u9ljUaRjw650Fy1snLzxuhdcSVy0+a7fXb+aLTEEApEjp1vfMRAGw7SbLNavLkHsfQDwGqcxnhdiImRQcbh1TQNXwMhFD4/Nkem7dpUQpHXw9fV1kAM2hxj0WoYQcQEeHmvWj/EsqmTQmPA/9vWVM5hMEhdBIXpDw4X9MgYi93VVqVSuUPsWJuwgO+GUs8ZJPeEErZsVjdkWClSdckynrldY0RgOru6TJk1yN2smgjC/JXvP1epu3cCaMQ1CYlS9dU10xcJqKxKrJs2ePRu0A0yUyRhmSCNfiNwKOn4MxVSsDjafasabNs2e5OpAY8gQ0nwPSKufAtgU5GuDg2sbZjDM4qd0+ymUn/awAuwOe5sm/7H/d2z1fQTN/ImpfAF3+1e+AN+q9+8UNgYxAAAAAElFTkSuQmCC',

  };

  GameIcon: any = {
    'TP': 'assets/images/GameIcons/TP.png',
    'BAC': 'assets/images/GameIcons/BAC.png',
    'DT': 'assets/images/GameIcons/DT.png',
    'PK': 'assets/images/GameIcons/PK.png',
    'AB': 'assets/images/GameIcons/AB.png',
    'WM': 'assets/images/GameIcons/WM.png',
    'ARW': 'assets/images/GameIcons/ARW.png',
    'TP20': 'assets/images/GameIcons/TP20.png',
    'PK20': 'assets/images/GameIcons/PK20.png',
    'C32': 'assets/images/GameIcons/C32.png',
    'PK6': 'assets/images/GameIcons/PK6.png',
    'UD7': 'assets/images/GameIcons/UD7.png',
    'DT20': 'assets/images/GameIcons/DT20.png',
    'AB20': 'assets/images/GameIcons/AB20.png',
    'PK620': 'assets/images/GameIcons/PK620.png',
    'BAC20': 'assets/images/GameIcons/BAC20.png',
    'VTP': 'assets/images/GameIcons/VTP.png',
    'VDT': 'assets/images/GameIcons/VDT.png',
    'VUD7': 'assets/images/GameIcons/VUD7.png',
    'VPK': 'assets/images/GameIcons/VPK.png',
    'VWM': 'assets/images/GameIcons/VWM.png',
    'VC32': 'assets/images/GameIcons/VC32.png',
    'VTP20': 'assets/images/GameIcons/VTP20.png',
    'VBAC': 'assets/images/GameIcons/VBAC.png',
    'VAB20': 'assets/images/GameIcons/VAB20.png',
    'VARW': 'assets/images/GameIcons/VARW.png',
    'V3CJ': 'assets/images/GameIcons/V3CJ.png',
    'VBAC20': 'assets/images/GameIcons/VBAC20.png',
    'VDT20': 'assets/images/GameIcons/VDT20.png',
    'VPK20': 'assets/images/GameIcons/VPK20.png',
    'V3TP': 'assets/images/GameIcons/V3TP.png',
    'VBJ': 'assets/images/GameIcons/VBJ.png',
    'D3CJ': 'assets/images/GameIcons/D3CJ.png',
    'D3TP': 'assets/images/GameIcons/D3TP.png',
    'BJ': 'assets/images/GameIcons/BJ.png',
    'VPK6': 'assets/images/GameIcons/VPK6.png',
    'VPK620': 'assets/images/GameIcons/VPK620.png',
    'VJKR': 'assets/images/GameIcons/VJKR.png',
    'JKR': 'assets/images/GameIcons/JKR.png',
    'VCR': 'assets/images/GameIcons/VCR.png',
    'CR': 'assets/images/GameIcons/CR.png',
    'KF': 'assets/images/GameIcons/KF.png',
    'VKF': 'assets/images/GameIcons/VKF.png',
  };
  FourSuitsCardImage: any = {
    '2': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAgVBMVEX///8DAjHXACTZACfT09vExM8TET6Ihp1nZoI2NVtHRGjw8fS1tMH98PTi4uf40dklI02lpLbxoLKGhprgK1CWlql3dY/aCDbx8fHi4evxsbzmX37cGUT64eTvkaFZVXRFRGbhPWDV1dX1wMvsgZbqconpXXvmTm0WEkX57/JXV3Nh3a2nAAAByUlEQVRIx82UiW6DMAyGYyDlplCucvQ+t/d/wDlJk5IWFjRpUv9KCVSfbMf+AzmST1BCKXUMTJinwJTm4S+U44NUOh1xjaEWFSat7gD3ZgrzAHxHhE0BggmqAQBHpsfnZBwrATz1EgBk41jkQzQswJ3s2Vo97lk0o0Jep1EuQGpixEEjM7UA8GZRvpGKGBWaqBIwo4kKPaQqoyl9NEk2qyxjW6s5ZTUsobmpAejK52CTRsrcPHddXMTuUvIxeruXm2085p4Xhy1ru4jHxrRbk6e+asuyXrg1G5M2g7hAyta5MIWHPJmxZ5QWT+s/lXXZghvUl8BTO/5PwRAkmS7qkAqS36EVIo9fvVRJfXEAtmREcBhORFsNL0AQRH7pBXvl786yeWJFqQL32vsZGS2WSqw7d4Vpl8Ss9ozUHxVSZwaVgXZD29thi4O9HDotcSJHILRlx+zigjWlVZAcxLc0pMVkH/jWP2O5JfdI6Yr2ngRmi+ErLAWlkhvEEhxfa0nRt9H3SEhdp7FWuI2t9UZZnPsj9Qf2XdUPV/aDhlRIOQH12CZdXnDsFmv3KmtCqIhTJkTpKs2myZORlDphNl2U0oToOpF/0nEW9QNwdxL5KTV8ZAAAAABJRU5ErkJggg==',
    '3': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAb1BMVEX///8DAjHYACXw8vOlpLVIRGcUET7j4+nDxM3S0tuIhptnZoE2NFomI0z98fL40dlXVXTxoLOWlajgK1HaBzS1s8HmX3t3dY0VEkPcGULvkaPhPWD74eXW1df0wcz0ssDxsr3sgZjpcIflTm773ub7oUgnAAABsUlEQVRIx82V2ZKDIBBFQUARJInEJWZf5v+/cXqwWIKDWlM1VTkP+nJkaW4juqBPgFOhOZl3iMixYVfMWKXClpymxzpgrGiBCqrAK1PaDuOKjB9UMG/CKjA+2LW3MC9KebRFFqMtUqzThFnbEhoGK+cEKoSQCjZD0RwMGxRHi5pBoEW4BE+iZSh4dIUnV1UEcdhGovJ696blv2uQSF+GHmOWPCBFbEDhGz0xXMhy+iMSmpvoJT2gYualysWOASSZbS0qGGMCpv4oJn3+ddxMrTKPkrNtsv3Eaw8QiXB/mybLYo/048XRemsPVuQR5apqZxzAiT2GHdyuC4i8wkk2svvM83SbdJINWe2tZosslTHMsVMUeXWwBclYW2l4um7qIstSRKF+RJafuEUhtVnXMvcHWH+E8HLlpVF58f46HeFgn6duG9+1wa1xzIBuPP5bmLY37ZwZTuY5+LGENhnRYizvNQtxmsIObQISWo21+OToh0Dr0totGOzsSmbyoaogvrXN5RAUpAerZFwGv8ftmMzXW8+0tCC4R6UOmrUzAYmRbiTvgRXDOY/7/or+icsq6xsR/RAhRlDvnAAAAABJRU5ErkJggg==',
    '4': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAdVBMVEX///8DAjHXACTYACnExNCIhptIRGfw8fMUED798fLS09lnZoL40tnxoLLgK1KWlqo3NVnbBzbU1Ny0tMAnI05YVnbi4uni4efCxMumpbT0sb92dY3eGUb64eXujqPpYHzkYHziPl/1wMrsgpZ3dpDrconlT22gg8chAAABo0lEQVRIx83TaY+CMBAG4A60FLnKLSJe6/H/f+JOq7VQNpSYbOL7QU19wpSZlkTkWxJRSlcowDgVz1exG6xhFcDezXgINXezBqAjTiawJEEWLqpSlpQsWGQ5QEScjAEw2d9lRgFy9b3IeA1QulkjSzpZJf90sjYEiAXFdAJ/LezfCv2UmZRYNcaijKnXWdHlr2SUsZZ8U7b2wuG4+esWWNNOCi+buSoEaDgx2RSeZzu+B5mwMirzPdvxGl6JdcXBQ2a5wD48CVZUzh+5LZg0aiXzfB+NknfNyjfSdyBF8KpaJOO7jgkBsyMvh/ElTEevEAdBlQv8lErlJN1bmbTWnbuqrRllCleThdSX+3Knv6L6MLyLVqgdAOQG9o/LEQd7v5wmhVszApmj7O55k8nm9SMWTdjhOaaLmsNgDi4TtWSCPdv7owagh/9maHSEOiC+JphCKzob/YCP08M/a9bNWK8NTuKgGQdMWOej45sW3jPDqCF7VGVAY/zSrUsytbnHZnKvdlsONxKJ1qydUaXETowPnK6cvLkilHb2vf8h/5RolfoFWpEQqn8BMC4AAAAASUVORK5CYII=',
    '5': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAdVBMVEX///8DAjHXACXExM7aACXw8fMUET/S09qHhptnZYJGRGb+8PK0tMGlpLXi4un40djxobJJRWjgK1ImI07aBzM3NF2Wlqp3do3nX302NVfysb5XVndZVnTeGEX64eTukaHiPmHV19f0wMrsgpTkUGzobo7odIcp7LNHAAABuUlEQVRIx82ViXKCMBCG8xPDVRBQTu+rff9H7LpgiKEgM06n/cbRCB9JNrsJ4iz+GCVN1JgmYSLnaVO9yR4xhg+Invc1d54WCsJbTmtb0uQORBZPaCGKLTpCNaER68r3741CTWklt6QLBGPaGnjMyQMwGomMdDMAfPGaCNjO0CQF+5amYiO4Cqh+1jJAh6DW+o9NDLheH2ghRijIi9U9zIafGEEVnC36Zmsc30VLoF4sq0/QyP+LQd0cjh9Dy3OtOkxyZzXwIhfYmgF+5I5je6rCHTfqrZVDLNgzEt0RPEbcOy2ml0EjH/MirP6W6NnxlRXdX7Sa86mDNLSMr6SdQuSJeNCwwWmPhfba7lIjhCALo6YMsv7sqNnRlmZpbZFrZ1kUdvGnPK/XbK7JG+8Ob4YVA2h6cXM7HSmxX6c6eYpTp4A5OkTN6V9szGp70g7t0p7uP4t935dfco2Ufru8F7rPH9aNY1JTcoF0AhdALjrkIPV7VhymHtc2rLGYH/SScX2sG6N807wbdJ88nXmuF8rAOGGSFYdwe9ozUbxUqIRXGpu1Ji0VNkHXk+mRZSOltPf9RfwS51nWN17rEbXzYIw4AAAAAElFTkSuQmCC',
    '6': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAgVBMVEX///8DAjHYACTYACjVACXExM9HRGempbXi4uclI00UET9oZoQ3NFz88fTT1NqHhpv40dnyoLKWlarfK1DaCDXv8PK0s8DnX33R0dqzs8N3d45XVnPw8fXxsr1XVXfcGUTx8vL64OTvkaHhPWD1wMvsgZZ3dYvqconmTm3EycWqrKfuQvXqAAAB80lEQVRIx82VC2+jMBCEsbF5FwrhEXJJ0yR93f3/H3izbs2aEtOqUtUOSoKUT7v2zmCCT+lf8M3SUPcBk+SZIP3ZrlF9JqxiP1WCShut4wKc9mL4Nw+McvA+KhYisvepEKW3GHfSY+fbpRBFwFrpWRGNuSUrWEU9dSRIUenFAJRPPLcVjPbaJ9s4Baf9WCYanpsfE7u3+3vUG/3V9rNtX9UOweA4CXF3HWu4J2N+F7ipd3Haue998Ua5zi7AMxA7rZHa363GsiuMnymb5VGVCaNUB6vaxxW0DX6XFsE+nW+W1DbLg5lu23Cz4MYMXu8D1k2rlHzH3e8EKeuZ2kgVSjXjulS8Kbcdj6glpZpxkZik7bpC1MIVMpdMkM3vJlS4QkX1ni1WTpA9a2oJynxke+ueMVDmBKNGO3DoWjt+51E0Fg2+p/gMYCRAUDMl6OfqkRbHtbgxh9+uj9e1osMjqC+q0+XnjkpRMHh4uZxh7PNlmDVO2ALSmXwfYD+GcnDTNsNOSpKdF0WuHrlW1ZiMNNXreB8k7KSxGWcnDIxVYwKCIpDxX7aW0gvrj8YqZewa/NiBKCoFJ05TxE0+0sKJb92CIOroDOQJVBnpnH5syjeAlHqZPTN9nHQ4UcvGeVgHpVS9PNtsJeY4bKwrL7SH4Ef19z9vZBR/VJfCPQAAAABJRU5ErkJggg==',
    '7': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAclBMVEX///8DAjHYACXEw8+HhpxIRGfS0tsUEUD88fL40dlnZYLi4ejfK1IlI07bBzS0tMGlpLU2NFvw8fWWlal4dY3mYHzw8vHxobTzsr7vkaVYVnTcGkLj4+n84eTW1dfxoK/hPWD0wczsgpjlTm7ocInqcYSMI0vMAAABcElEQVRIx82WyXaCQBBFX4HQAgIyiuJs8v+/mLY50oNwqE0S7wI391Rr1asWnPHP+DbltJWQjT9TzNHCmWorTUG0wiKhoGDZKlMS7bJWE+WMxhDnSKREybK1JYr/ulhB5DM6S5SC1bOa9wPOrDMFlql4E4hZCULAmjoEq2sgtgYGcg9afBxHOJz266nVdMIYZd7mzcsF0aGFZp15nuu1NT0RubY20nK8NiWF3oKol47rBe5tFckT37wjaQ54svE03zqxmiGTO21lkblzEkGSLRT7UdthpIyDIC8q+ZTWQKMti8TZgfukhcJN90V9r2W6u2vxKf0QrOuKCi12j+seWH9dG+vgZByBblszjL8z02ZpJ09xVc/e+H+pVEaq1dDem2diXFYjlQqIaWUvy38bfW9ozbzWGcVOY8tUPtLCiO/llcs+su5bEQZ+/Px4pXxI5sPamXx7LKlGWCWAGaULXOKxkvZ2k+8DCWxu+CXOLOsHOFYOsphPp5oAAAAASUVORK5CYII=',
    '8': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAeFBMVEX////+//0DAjLXACTaACjU1NrExM9nZoMUED9IRGfx8fJYVnXi4ecmI0398PH30dmzs8A3NVvgK1KmpbXyobPbBzaWlamIhpvmYHzi4+l3do/x8fX0sb/ukKTeGUb64eXjPV/1wMrsgpbrconlT20VEUa1tMOIh5kdcSapAAAChklEQVRIx4WTB5ujIBCGHbJgIJpoLOll293//4c3TVgf4u24SwBfpvCNhS2KAqD4zYCeyP2O01+awv/8EmAdmi4hh3ioyrVhKyvEZDtPsiFIrQPgvdwsUm13KgpXIuckjdw2xmz0vKNpblwcuqimRL0xXHxuhAmFhmFfRQTBGk0barOOVJ7b2jIW0FkJC8lRpeYQIIw1lnwpioWo9tugeRo2YUEHDkYIU4s9gVunMopQO9nKXbFW32dXjS2rtXBtliQPrK6rSa3X7jxS0+rSpkUuQmAJVNQ1LEjqYXboZVDAF5fY4CcsdlGrMrYoLs7IL3TlxgJQpR5TC1kn8Vo0rb33tUHKLmgKRaUqyAVGLAdD8/eA1vDNRHDhG5MclJx/57pI73QzYSAn4wsadQJZnCqeVXvctrSah7HrUikddsNqvyUq7oD02iZA8r8dVivmUr1wORuy9Z/IbfdIIccn1UIdb1VL3N1Xb8JtU6HeRHOARnkRppyClUm24dr2BCn4OWH2B+bZf4+I2rCLV9JKATR08nneGHnDp4dJXQil90074thN6R7FF1Iwk+EkX0IU6UN88QaoCMCBm9m5nvOCqMvckv7Xjx2AeslRxdIR3ZJFcBbSZoo178wOL6O1kj5gwK/nDeX/fB53HFafU5SAN29veGlHkf+aOh+sYhLtIRo8+ecem7I6jNwj46Fj5++ilQobEyNGbeQ2YkqxQSFwmfR3YZg+Th0+w7jSK2M8DA9CaDdwf9Stti+l1w8q/Z2VEPKMlPWupB+90t2enX1tpXYhm64K5lzYsdLmAmmlPqml/6V4SqIi10fdYy855yqZRqXf9YSa0j+V11kaU5Blg8ImPLeYxD8SwRX2guznOAAAAABJRU5ErkJggg==',
    '9': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAflBMVEX///8DAjHWACXZACb//P9HRGjDw86IhpsTET/w8vLS09ulpLVnZoP98PLi4un40dmWlam0s8F3do3xobImIk7aCDQ3NF3nYH3gLFHh4ubU1dhYVnQ3NVjysb7eGEb64eTukKFYVHTiPmHgK1T0wMqYl6rsgpTocYvkUGzvkaP4Xwj6AAAB/0lEQVRIx82W65aqMAyFJ0BLC4hlBFG8jDrX8/4veJJACx1WwfPvbASBfittk93qS/fyP0ii1hhdKCCdsiWqY4iVh6kMqdpImYtF7gxQaL7LAdQxQFUYCylWCvAawF4BjJsLhgtgAkBOH7IgVk4fzBPRFEA6I+ywC3ufQxArOVuJzSDcw1OFlFLyqWBP0QLaA2ov6CIZC8YjUWpTwoIqTSpSg3UqaNrrqgH0OiUBRKitulcu1WEnlQBKu0zvl+ZZU7zjCajwIWnOm1DW5EGugF6CYy3mDYXQ80roXEdWqWShvfz94u22meOZKmysvm3bRO+bWaEUwGla7E0TxdHB53RvNFWN1HscRVHscbqGQbbj7TUiyo8nwEkO40KCFY/jK2HUid8cCOix6NtN0kHWjDum+NNsJzsWaeqMPzQwPndTZwhRnQ1enX9a6hCx3cy7wqvoF1I2VuItZZc1Oz4c17ouX+tUyEhaZovt44Z27sYOfx43LOz3o/U6Ll0JWDdKWrvhYlwcZAtxt4bkZEQPrsN1jJUa9ohJ+/R+cDuhFHWy/TgZNghHG6zUWErOSn+NGeGytkEsuWDzcDRvNjua/VGfrX3x/a4ZjHTdeluRyoQs6Mu6/MDcj7cGq/yocV/uPie/jq21kacCI/nlbJma/xX4ve4/njUIHv+k7qmofwGpBBSPpWfR8gAAAABJRU5ErkJggg==',
    '10': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAe1BMVEX///8DAjHXACTExM/YAChHRGjT09sTET5oZoLx8fLi4+eXlquHhpv88POlpLX40dngK1ElI020tMLyoLJXVXM3NVp3do/aCDXnX3w3M1zw8Pbxsr3cGULi4er64OTvkKPhPWDi4uPV1dX1wMvsgZfqcolZVnjmTm3tkqElbIpcAAAB+UlEQVRIx82Uh47CMAyGm2Z0QmmhtGyOee//hOcsN02KhE66YSQSxV+dxP6d6BL9rQkqcJ5TSsUkRQnhdtoSafV8IlaC2JxYSwc/uizWwKzMo0MJH65H0HZFiI22hYneTQBP3VDgaRuDlbiXnHL38ODJ7BVqQg7GkYEjd7ECbyogMHpW4BkwLoaElO65C0JW03lLRwEgedMYN7fDw72BRb+EPdwrfPhYmBBcCzE9bHANvwn1ljgFAt2JF1gKMgqyG2J5giE2SlIhZguZNJLnsOIzxqjeltRc9sMme4mBgbyVrTwqyqgx7dgWKX+UIvovlvsLx9Nsquu9Fq+6eBFwTeLdf9bFsc9layItaQZqEcfM47Laf1uqvYS8eJygUXMuZjA2cDlCtnsXkmIavOMlEbKSWMo4TEXsKty0VQTcFfV1Akzvu3Set5Tzpi3gH1XYSwZAS2GveRK8qXgOhYJuRgtLwKo3iry7AfVNy+i0WMMHvx3A3fN8gsLez/1o4xxLoOxTJqOH8sO4c9U2wo66nGc17IdYZaE0UpQ6vVeVf6YTzBBDHZm3rYp1OPXrsJeD0iu1MU32r7GdpJhiuyOmTOmj3jjyXXbmbHsnIWugBKepHKzKF0qUz1HPNPNDBs+nKJxm7VFsrqUYCTkWUhGl1O/7a/RDdnmL+gLrexPw7YJB4gAAAABJRU5ErkJggg==',
    'J': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAbFBMVEX///8DAjHXACXZACiIhpvU09tIRGfExM/88fL40dkVET/fK1LxobLaBzXh4eimpbVnZoGWlajmYHw2NFrw8fXi4+i1tMBYVXTzsr7vkaV3dYwmI07dGkHw8vH74eThPWD0wczsgpjpcIflTm4rGbL7AAABaElEQVRIx+2U626DMAxGsQOEAuEOg47e3/8dR5wlaWg3Ik2T+qNHVZGsIyeyPwii4FXoOS88tBAgfGtv7c/a3k+LAUoPrQXg21YEAN2vwtKkKFOAj61LEWnho8XRxq3qcOF1vgSafl04TsmT26eVW9gJlj14g5zpIbAkAhFX3mGvpjpYK0NkK+/wCd9U+sQZCceLwcCVJVA2Y06/3kh65xlZyKR3tcGxxFTJySLE7i6GkhTApHaS7eiXB4auiuOhrZd/k+0GCW0ZCjrPcpH9WP4k/4NTyBnda5Pxsrb86bhPaEsAaK043k7TstjrqXEOLswKiEnuqUkyObjxPm2OdkTiREudba+wpozUoRrvmam9q5UZjRxFTQFBgkwmtMUfVj+TgNSv+Vkb0axeHM3IKB+f7V18c4GKeed8HdMo5pV86JTTMPCWOO9V2XewD6K6sLWGMYqRS6U7Wc9aFs55H7icg38i8rK+ACu1Dh9isyLHAAAAAElFTkSuQmCC',
    'Q': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAjVBMVEX///8DAjLYACX+/fv8//8DAy1HRWcTED5mZYP98fPw8fLi4uj40dilpLSGhpzT1NnDw83gK1ImI03T093xoLN3do02NVvbBzaWlamHhplXVXXDxNG0tMHnYHzw8fX0sb/uj6PeGUb64ebjPV/WACzi4uvsgpbrconf3+f0wMr2v8rGyMWqq6vlUG7lTm2kkIR/AAACQklEQVRIx7WU2YKqMAyGCW1aZBcEUcR9Zs7+/o93QqS1o1P1Zn6xlPYjaZOU4JnkeP3hrgi+S3HZl/EzqFBAwijxLETwlRGjlaamCPyqNegyIJruiR8jjwWbrQGUNwI9wGrqRghetxHAm3EPkPuwClrb16B8GEDkLLPyZc84EoyhL3B2B4yB32nuYNqPKWc7yuPUtfCG0PisdQDZ1F0B1t4a0lBtubf1h41tQBSPVIvQe4uao1XleaMBOg8h2G0DCCSdBY+V5GuN69jv0U40sA5e0AaxXeXNA0KYupsS8hgtV3me+abvBuTtmLhHZfC6xBcRuwWO+5m8e6vWkRCulXQZzmd3wdIAFPyrZsswvOW2DQAg6M2VmhN1w8UVMZz0aNpiemCKOeEcIxKOXMkrS8mj5cyOfyBYreU4NJ+YM/0/TBBrssS26KbGMbEIrZZpYNSyT41gz8zeYotrlONIqU3bUWsTPjiUqwSUcBP67lAicB1vDMJa8Lrko2zL8bd7T7nLJfVUjFjPcVlcOlJeZuyTqwwQ20JO74vd39OeEvtxGj4tL+GMric3ksM2cPrPO3l1XAAgY6xjyDpxeyBITke9q4DUrTI5npOfoSvn04jslJqOy8illmanPRLDlDnFBwcbzDkswQoVr3d3vho72hIHkq5aQFO+YmHq8pAGVg1RteojuhXykqt0ztS/Gb9lzlWWxNgERZfYEMmBC2R6tmREBj/ncSBqNCXdPJR9mVhEcPvr8cdCfDUlXqwkP2nnfv8Hx8AXVXg68N0AAAAASUVORK5CYII=',
    'K': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAeFBMVEX///8DAjHWACXZACdHRGbExM+IhpwUET9HRWni4unS09mHhpr98PP40dlnZoK1tMDgK1ImIk3w8fTw8fHxoLFYVnPaCDSlpLWWlqo3NFl3do7nYH3S093ysb7ukaLeGEbiPmH54ePV19f0wMrsgpTocYvkUGz83+skYvgKAAAB00lEQVRIx82W6XKjMBCE1QQhEAkY8GIOXzn3/d9wR4oOMGspf1JJl8uSiq96hplRFeyF/agk58XyIP+PpUBm90cAdRRrG2Bid7HSbHMgZ3eUO7cJEC2t4aA1AE5rOKgUwEBr2K0dgT+0RnKrgIYSiwQdQCpYJKiEUhUOWnaCElN2YTeFsobQIKa84nUjja3pVRATR7Xjyi6E1e5tmgBmh6cAcAgURG9MK0JdsHaCDoGeWmWA6CJDTuqUXQCL2h3KbFmsMqPjb9Fmxv6eH7fUUdzM7FOf7DZcrWZtWYHHPkluuW6Ckqg9tSPqYc21DYwqG/GSKGzNpXDiJq+HxMhzhYPsZdlZiOK+W0zCK2VKe3pufv2TCzpqQsAP7VknprD94kWrPK/HoUpzN9qzZjzlE0xX57cNZQO7qtn8KK+4Tm8BKqKWyy9QBwCjB08f1zM19v06rwIXpgVGZ1Wz+bP9pwUmV9hzoltwTRR98V7l0IA0ZJ/lfdV9orrpP4c1cBr0gBCgpdbeUnzT+ouBlOl8HzvRY+PWP7uSgSSacTG++94M0mVRkImoY8orWqSd8p22+1jdmfpQdJiYHBaXdSavPbtVZZ085ykvbr8IvF7ZN+nlS9Q/EH4TVLIu1cIAAAAASUVORK5CYII=',
    'A': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA8CAMAAAA9rjhhAAAAflBMVEX///8DAjHWACXZACfw8fTi4uk2NFr98PPS0ti1tMGlpLRXVXUlI0340djExM9oZoJIRGjyoLKVlah3do2IhZkTET3aCDPgK1OXlavnYH3R0t3ysb6HhpzeGEX64ebtkKLiPmEWEUHV2NPCwsz0wMrsgpTocYvkUGw4NFfdLVNWahA6AAABxUlEQVRIx82ViY6CMBCGKUUOhXLfoK637/+COx2OFgjUZLOJf7RJ4cv8dA7QLto3yGDsaqixmBBSqDEXMFNJ2YRr94GnC65qTzMCV7VnsVO7RpwwCYk2KSBcZA+bFPjFuBJD4WljTLJXemJWDgrPIcfGpmc1VGy/5UmahOsFrhueQhuuhQRtuB7wnCh/3dXAc3Zy1l33mNteKbiuef7AqnIFT3/coOuapyNtU9wu5SSJIW+b5qp9ixZTcjxZS6pK/ekFL9PzBeekkE/5pFam0zlnxITrxxFUTilwb0umXNLLHxxbHUSn8QIyinVURoFCUnCia4d65xgJMf0hXhxCAV4J4X7/yzx5VECpaG3kKP5D6Qh+EDhmAevYiiWHAAvnRUA/oTtQIpYwnvdXSCk8l1r13fvDZ8NWMjgJ5CXA+nk+QWEf59JbvD7EyJ140kor52WoJcyeYEdMhn7GOrQiVhJhj0RNl94b3qccBknfjFERNghG61spGyi2KH3bFRTXch2rEcKY2XFMGfaHa0rtG2Z9I7VSQmKgqoD5MA3V0OVv5J7WZK72O4PEml1Iw1piG83lk3RWrVJQQoyx+dzftH/S5SPqFw0oEw9WZTgGAAAAAElFTkSuQmCC'
  };
  VirtualBackground: any[] = [
    { image: '/assets/images/red-background.jpg' },
    { image: '/assets/images/brown-background.jpg' },
    { image: '/assets/images/light-brown-background.jpg' },
    { image: '/assets/images/blue-background.jpg' },
  ];
  SetVirtualBackground(i) {
    const index = i % 4;
    return this.VirtualBackground[index].image;
  }
  VirtualBackgroundDouble: any[] = [
    { image: '/assets/images/Gamee-Play-red.jpg' },
    { image: '/assets/images/Gamee-Play-brown.jpg' },
    { image: '/assets/images/Gamee-Play-light-brown.jpg' },
    { image: '/assets/images/Gamee-Play-blue.jpg' },
  ];
  SetVirtualBackgroundDouble(i) {
    const index = i % 4;
    return this.VirtualBackgroundDouble[index].image;
  }

  private _GameList
  get GameList(): any[] {
    const gameList = JSON.parse(localStorage.getItem('GameList'));
    this._GameList = (gameList) ? gameList : this.GameDetails;
    return this._GameList;
  }
  set GameList(value) {
    this._GameList = value;
    localStorage.setItem('GameList', JSON.stringify(value));
  }

  asIsOrder(a, b) { return; }

  CheckNumberFormat(val: string) {
    const NumberReg = /^[0-9]*$/g;
    if (NumberReg.test(val)) {
      return true;
    } else {
      return false;
    }
  }

  checkNumeric(event) {
    if ((!((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode === 8)))) {
      event.preventDefault();
    }
  }

  SetGameChips() {
    this.chipsSetting = JSON.parse(JSON.stringify(this.GameChips));
  }

  get LoginLink() {
    const loginLink = localStorage.getItem('LoginLink');
    return loginLink || '';
  }

  get GenerateRandom() {
    return Math.floor(Math.random() * (9999 - 1000)) + 1000;
  }

  IsAllowBet(): boolean {
    return (localStorage.getItem('IsAllowBet') === 'true' ? true : false) || false;
  }

  toggleSound() {
    this.IsSound = !this.IsSound;
    if (this.IsSound) {
      this.beepAudio.load();
      this.noBetAudio.load();
      this.betPlaceAudio.load();
    }
  }


  removeParam(key, sourceURL) {
    let rtn = sourceURL.split('?')[0];
    let param;
    let paramsArr = [];
    const queryString = (sourceURL.indexOf('?') !== -1) ? sourceURL.split('?')[1] : '';
    if (queryString !== '') {
      paramsArr = queryString.split('&');
      for (let i = paramsArr.length - 1; i >= 0; i -= 1) {
        param = paramsArr[i].split('=')[0];
        if (param === key) {
          paramsArr.splice(i, 1);
        }
      }
      rtn = rtn + '?' + paramsArr.join('&');
    }
    return rtn;
  }

  CalculateProfitLoss(betList: BetList[], runner: string): number {
    let pl: number = 0;
    betList.forEach(element => {
      if (element.IsBack && element.Runner == runner) {
        pl = pl + ((element.Stake * element.Rate) - element.Stake);
      }
      else if (element.IsBack && element.Runner != runner) {
        pl = pl + (-1 * element.Stake);
      }
      else if (!element.IsBack && element.Runner == runner) {
        pl = pl + (-1 * ((element.Stake * element.Rate) - element.Stake));
      }
      else if (!element.IsBack && element.Runner != runner) {
        pl = pl + element.Stake;
      }
    });
    return pl;

  }
  getGameRule(gameId) {
    this.gameService.GetGameRule(gameId).subscribe(data => {
      if (data && data.Rule && data.Rule.GameRule) {
        this.gameRule = data.Rule.GameRule;
      }
    });
  }
}
