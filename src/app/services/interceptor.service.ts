import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';
import { SpinnerService } from './spinner.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(private spinner: SpinnerService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.spinner.start();
    let path = location.pathname;
    path = path.slice(1);
    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
      const authToken = localStorage.getItem('AuthToken');
      if (authToken) {
        req = req.clone({ headers: req.headers.set('AuthToken', authToken) });
      }
      const token = localStorage.getItem(path.trim() + 'Token');
      if (token) {
        req = req.clone({ headers: req.headers.set('Token', token) });
      }
      const gameID = localStorage.getItem('GameID');
      if (gameID) {
        req = req.clone({ headers: req.headers.set('GameID', gameID) });
      }

      const gameCode = localStorage.getItem(path.trim());
      if (gameCode) {
        req = req.clone({ headers: req.headers.set('GameCode', path) });
      }

    }
    return next.handle(req).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (event.body.RefreshToken) {
          localStorage.setItem(path.trim() + 'Token', event.body.RefreshToken);
        }
        if (event.body.IsAllowBet !== undefined && (event.body.IsAllowBet === true || event.body.IsAllowBet === false)) {
          localStorage.setItem('IsAllowBet', event.body.IsAllowBet);
        }
        this.spinner.complete();
      }
    }, (err: any) => {
      this.spinner.complete();
    });
  }
}
