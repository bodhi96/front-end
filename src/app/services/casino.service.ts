import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CasinoService {

  private httpClient: HttpClient;

  constructor(private http: HttpClient, private handler: HttpBackend) { }

  GetUserWiseGameDetails(paramval?: any) {
    return this.http.post<any>('/api/Casino/GetUserWiseGameDetails', JSON.stringify(paramval));
  }

  GetRoundSummaryHistory() {
    return this.http.get<any>(`/api/Casino/GetRoundSummaryHistory`);
  }

  AddUpdateBet(paramval?: any) {
    return this.http.post<any>('/api/Casino/AddUpdateBet', JSON.stringify(paramval));
  }

  GetMarketHistory(paramval?: any) {
    return this.http.post<any>('/api/Casino/GetMarketHistory', JSON.stringify(paramval));
  }
  GetRoundHistory(paramval?: any) {
    return this.http.post<any>('/api/Casino/GetRoundHistory', JSON.stringify(paramval));
  }

  GetMarketHistoryDetails(paramval?: any) {
    return this.http.post<any>('/api/Casino/GetMarketHistoryDetails', JSON.stringify(paramval));
  }

  AddUpdateOneClickChips(paramval?: any) {
    return this.http.post<any>('/api/Casino/AddUpdateOneClickChips', JSON.stringify(paramval));
  }

  AddUpdateChipsSetting(paramval?: any) {
    return this.http.post<any>('/api/Casino/AddUpdateChipsSetting', JSON.stringify(paramval));
  }

  GameLogOut(paramval?: any) {
    return this.http.post<any>('/api/Casino/GameLogOut', JSON.stringify(paramval));
  }

  CheckGameStatus(paramval?: any) {
    return this.http.post<any>('/api/Casino/CheckGameStatus', JSON.stringify(paramval));
  }

  GetBalance(paramval?: any) {
    this.httpClient = new HttpClient(this.handler);
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    const authToken = localStorage.getItem('AuthToken');
    if (authToken) { headers = headers.set('AuthToken', authToken); }
    return this.httpClient.post<any>('/api/User/GetBalance', JSON.stringify(paramval), { headers });
  }
}
