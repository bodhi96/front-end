import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { HubConnection } from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';
import { callbackify } from 'util';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  private hubConnection: HubConnection;
  private IsReload = false;

  constructor(private http: HttpClient) { }

  ConnectHub(GameID: number, callBack: any) {
    const builder = new signalR.HubConnectionBuilder();

    this.hubConnection = builder.withUrl('/NotifyHub', { transport: signalR.HttpTransportType.LongPolling }).build();

    this.hubConnection.start()
      .then(() => {
        this.joinGroup(GameID, callBack);
        console.log('Connection started!');
        this.IsReload = false;
      })
      .catch(err => {
        console.error(err);
        if (!this.IsReload) {
          this.ConnectHub(GameID, callBack);
          this.IsReload = true;
        } else {
          location.reload();
        }
      });

    this.hubConnection.onclose((error) => { console.log(error); this.ConnectHub(GameID, callBack); });

    this.hubConnection.on('BroadcastMessage', data => { callBack(data); });
  }

  private joinGroup(GameID: number, callBack: any) {
    if (this.hubConnection) {
      this.hubConnection.invoke('JoinGame', GameID.toString());
    } else {
      this.ConnectHub(GameID, callBack);
    }
  }

  DisconnectHub(GameID: number) {
    if (this.hubConnection) {
      this.hubConnection.invoke('LeaveGame', GameID.toString());
    }
  }

}
