import { Injectable, Output, EventEmitter, Directive } from '@angular/core';

@Directive()
@Injectable()
export class SpinnerService {
  @Output() change = new EventEmitter<boolean>();

  start(): void {
    this.change.emit(true);
  }

  complete(): void {
    this.change.emit(false);
  }
}
