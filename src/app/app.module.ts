import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorComponent } from './component/error/error.component';
import { GamesComponent } from './pages/games/games.component';
import { TeenpattiComponent } from './pages/teenpatti/teenpatti.component';
import { BaccaratComponent } from './pages/baccarat/baccarat.component';
import { VideoplayerComponent } from './component/videoplayer/videoplayer.component';
import { MarketHistoryComponent } from './pages/market-history/market-history.component';
import { SpinnerService } from './services/spinner.service';
import { ComingSoonComponent } from './component/coming-soon/coming-soon.component';
import { RulesComponent } from './component/rules/rules.component';
import { ChipsSettingComponent } from './component/chips-setting/chips-setting.component';
import { DragonTigerComponent } from './pages/dragon-tiger/dragon-tiger.component';
import { PokerComponent } from './pages/poker/poker.component';
import { AndarBaharComponent } from './pages/andar-bahar/andar-bahar.component';
import { HeaderComponent } from './component/header/header.component';
import { TimerComponent } from './component/timer/timer.component';
import { BetListComponent } from './component/bet-list/bet-list.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ApidocumentationComponent } from './apidocumentation/apidocumentation.component';
import { NewsHeaderComponent } from './component/news-header/news-header.component';
import { WorliMatkaComponent } from './pages/worli-matka/worli-matka.component';
import { AkbarRomeoWalterComponent } from './pages/akbar-romeo-walter/akbar-romeo-walter.component';
import { Teenpatti20Component } from './pages/teenpatti-20/teenpatti-20.component';
import { Poker20Component } from './pages/poker-20/poker-20.component';
import { DragonTiger20Component } from './pages/dragon-tiger-20/dragon-tiger-20.component';
import { Cards32Component } from './pages/cards32/cards32.component';
import { SevenUpSevenDownComponent } from './pages/sevenup-sevendown/sevenup-sevendown.component';
import { Poker6PlayerComponent } from './pages/poker-6player/poker-6player.component';
import { OneClickComponent } from './component/one-click/one-click.component';
import { FooterComponent } from './component/footer/footer.component';
import { RoundHistoryComponent } from './pages/round-history/round-history.component';
import { AndarBahar20Component } from './pages/andar-bahar-20/andar-bahar-20.component';
import { Poker6Player20Component } from './pages/poker-6player-20/poker-6player-20.component';
import { SvgButtonImgComponent } from './component/svg-button-img/svg-button-img.component';
import { Baccarat20Component } from './pages/baccarat-20/baccarat-20.component';
import { VirtualTeenpattiComponent } from './pages/virtual-teenpatti/virtual-teenpatti.component';
import { VirtualDragonTigerComponent } from './pages/virtual-dragon-tiger/virtual-dragon-tiger.component';
import { VirtualSevenupSevendownComponent } from './pages/virtual-sevenup-sevendown/virtual-sevenup-sevendown.component';
import { VirtualPokerComponent } from './pages/virtual-poker/virtual-poker.component';
import { VirtualWorliMatkaComponent } from './pages/virtual-worli-matka/virtual-worli-matka.component';
import { VirtualCards32Component } from './pages/virtual-cards32/virtual-cards32.component';
import { VirtualTeenpatti20Component } from './pages/virtual-teenpatti-20/virtual-teenpatti-20.component';
import { VirtualBaccaratComponent } from './pages/virtual-baccarat/virtual-baccarat.component';
import { VirtualAndarBahar20Component } from './pages/virtual-andar-bahar-20/virtual-andar-bahar-20.component';
import { VirtualAkbarRomeoWalterComponent } from './pages/virtual-akbar-romeo-walter/virtual-akbar-romeo-walter.component';
import { Virtual3CardsJudgementComponent } from './pages/virtual-3cards-judgement/virtual-3cards-judgement.component';
import { VirtualBaccarat20Component } from './pages/virtual-baccarat-20/virtual-baccarat-20.component';
import { VirtualDragonTiger20Component } from './pages/virtual-dragon-tiger-20/virtual-dragon-tiger-20.component';
import { VirtualPoker20Component } from './pages/virtual-poker-20/virtual-poker-20.component';
import { Virtual3playerTeenpattiComponent } from './pages/virtual-3player-teenpatti/virtual-3player-teenpatti.component';
import { VirtualBlackJackComponent } from './pages/virtual-black-jack/virtual-black-jack.component';
import { BlackJackComponent } from './pages/black-jack/black-jack.component';
import { ThreePlayerTeenpattiComponent } from './pages/three-player-teenpatti/three-player-teenpatti.component';
import { ThreeCardJudgementComponent } from './pages/three-card-judgement/three-card-judgement.component';
import { VirtualPoker6PlayerComponent } from './pages/virtual-poker-6player/virtual-poker-6player.component';
import { VirtualPoker6Player20Component } from './pages/virtual-poker-6player20/virtual-poker-6player-20.component';
import { VirtualJokerComponent } from './pages/virtual-joker/virtual-joker.component';
import { JokerComponent } from './pages/joker/joker.component';
import { VirtualCardRaceComponent } from './pages/virtual-card-race/virtual-card-race.component';
import { CardRaceComponent } from './pages/card-race/card-race.component';
import { BetSlipComponent } from './component/bet-slip/bet-slip.component';
import { StakeLimitComponent } from './component/stake-limit/stake-limit.component';
import { GameDetailComponent } from './component/game-detail/game-detail.component';
import { GameCloseComponent } from './component/game-close/game-close.component';
import { GameResultsComponent } from './component/game-results/game-results.component';
import { VirtualCardViewComponent } from './component/virtual-card-view/virtual-card-view.component';
import { RockPaperScissorsComponent } from './pages/rock-paper-scissors/rock-paper-scissors.component';
import { VirtualRockPaperScissorsComponent } from './pages/virtual-rock-paper-scissors/virtual-rock-paper-scissors.component';
import { BlackJackClassicComponent } from './pages/black-jack-classic/black-jack-classic.component';
import { VirtualBlackJackClassicComponent } from './pages/virtual-black-jack-classic/virtual-black-jack-classic.component';
import { KabutarFuddiComponent } from './pages/kabutar-fuddi/kabutar-fuddi.component';
import { VirtualKabutarFuddiComponent } from './pages/virtual-kabutar-fuddi/virtual-kabutar-fuddi.component';

enableProdMode();

@NgModule({
  declarations: [
    // pages
    AppComponent,
    GamesComponent,
    TeenpattiComponent,
    BaccaratComponent,
    DragonTigerComponent,
    PokerComponent,

    // component
    ErrorComponent,
    RulesComponent,
    ComingSoonComponent,
    VideoplayerComponent,
    ChipsSettingComponent,
    MarketHistoryComponent,
    AndarBaharComponent,
    WorliMatkaComponent,
    AkbarRomeoWalterComponent,
    Teenpatti20Component,
    Poker20Component,
    DragonTiger20Component,
    Cards32Component,
    SevenUpSevenDownComponent,
    Poker6PlayerComponent,
    AndarBahar20Component,
    HeaderComponent,
    NewsHeaderComponent,
    TimerComponent,
    BetListComponent,
    ApidocumentationComponent,
    OneClickComponent,
    FooterComponent,
    RoundHistoryComponent,
    Poker6Player20Component,
    SvgButtonImgComponent,
    Baccarat20Component,
    VirtualTeenpattiComponent,
    VirtualDragonTigerComponent,
    VirtualSevenupSevendownComponent,
    VirtualPokerComponent,
    VirtualWorliMatkaComponent,
    VirtualCards32Component,
    VirtualTeenpatti20Component,
    VirtualBaccaratComponent,
    VirtualAndarBahar20Component,
    VirtualAkbarRomeoWalterComponent,
    Virtual3CardsJudgementComponent,
    VirtualBaccarat20Component,
    VirtualDragonTiger20Component,
    VirtualPoker20Component,
    Virtual3playerTeenpattiComponent,
    VirtualBlackJackComponent,
    BlackJackComponent,
    ThreePlayerTeenpattiComponent,
    ThreeCardJudgementComponent,
    VirtualPoker6PlayerComponent,
    VirtualPoker6Player20Component,
    VirtualJokerComponent,
    JokerComponent,
    VirtualCardRaceComponent,
    CardRaceComponent,
    BetSlipComponent,
    StakeLimitComponent,
    GameDetailComponent,
    GameCloseComponent,
    GameResultsComponent,
    VirtualCardViewComponent,
    VirtualBlackJackClassicComponent,
    RockPaperScissorsComponent,
    VirtualRockPaperScissorsComponent,
    BlackJackClassicComponent,
    KabutarFuddiComponent,
    VirtualKabutarFuddiComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    SlickCarouselModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    SpinnerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
