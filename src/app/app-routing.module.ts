import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GamesComponent } from './pages/games/games.component';
import { ErrorComponent } from './component/error/error.component';
import { TeenpattiComponent } from './pages/teenpatti/teenpatti.component';
import { BaccaratComponent } from './pages/baccarat/baccarat.component';
import { MarketHistoryComponent } from './pages/market-history/market-history.component';
import { ComingSoonComponent } from './component/coming-soon/coming-soon.component';
import { DragonTigerComponent } from './pages/dragon-tiger/dragon-tiger.component';
import { PokerComponent } from './pages/poker/poker.component';
import { AndarBaharComponent } from './pages/andar-bahar/andar-bahar.component';
import { ApidocumentationComponent } from './apidocumentation/apidocumentation.component';
import { WorliMatkaComponent } from './pages/worli-matka/worli-matka.component';
import { AkbarRomeoWalterComponent } from './pages/akbar-romeo-walter/akbar-romeo-walter.component';
import { Teenpatti20Component } from './pages/teenpatti-20/teenpatti-20.component';
import { Poker20Component } from './pages/poker-20/poker-20.component';
import { Cards32Component } from './pages/cards32/cards32.component';
import { SevenUpSevenDownComponent } from './pages/sevenup-sevendown/sevenup-sevendown.component';
import { Poker6PlayerComponent } from './pages/poker-6player/poker-6player.component';
import { RoundHistoryComponent } from './pages/round-history/round-history.component';
import { DragonTiger20Component } from './pages/dragon-tiger-20/dragon-tiger-20.component';
import { AndarBahar20Component } from './pages/andar-bahar-20/andar-bahar-20.component';
import { Poker6Player20Component } from './pages/poker-6player-20/poker-6player-20.component';
import { Baccarat20Component } from './pages/baccarat-20/baccarat-20.component';
import { VirtualTeenpattiComponent } from './pages/virtual-teenpatti/virtual-teenpatti.component';
import { VirtualDragonTigerComponent } from './pages/virtual-dragon-tiger/virtual-dragon-tiger.component';
import { VirtualSevenupSevendownComponent } from './pages/virtual-sevenup-sevendown/virtual-sevenup-sevendown.component';
import { VirtualPokerComponent } from './pages/virtual-poker/virtual-poker.component';
import { VirtualWorliMatkaComponent } from './pages/virtual-worli-matka/virtual-worli-matka.component';
import { VirtualCards32Component } from './pages/virtual-cards32/virtual-cards32.component';
import { VirtualTeenpatti20Component } from './pages/virtual-teenpatti-20/virtual-teenpatti-20.component';
import { VirtualBaccaratComponent } from './pages/virtual-baccarat/virtual-baccarat.component';
import { VirtualAndarBahar20Component } from './pages/virtual-andar-bahar-20/virtual-andar-bahar-20.component';
import { VirtualAkbarRomeoWalterComponent } from './pages/virtual-akbar-romeo-walter/virtual-akbar-romeo-walter.component';
import { Virtual3CardsJudgementComponent } from './pages/virtual-3cards-judgement/virtual-3cards-judgement.component';
import { VirtualBaccarat20Component } from './pages/virtual-baccarat-20/virtual-baccarat-20.component';
import { VirtualDragonTiger20Component } from './pages/virtual-dragon-tiger-20/virtual-dragon-tiger-20.component';
import { VirtualPoker20Component } from './pages/virtual-poker-20/virtual-poker-20.component';
import { Virtual3playerTeenpattiComponent } from './pages/virtual-3player-teenpatti/virtual-3player-teenpatti.component';
import { VirtualBlackJackComponent } from './pages/virtual-black-jack/virtual-black-jack.component';
import { ThreeCardJudgementComponent } from './pages/three-card-judgement/three-card-judgement.component';
import { ThreePlayerTeenpattiComponent } from './pages/three-player-teenpatti/three-player-teenpatti.component';
import { BlackJackComponent } from './pages/black-jack/black-jack.component';
import { VirtualPoker6PlayerComponent } from './pages/virtual-poker-6player/virtual-poker-6player.component';
import { VirtualPoker6Player20Component } from './pages/virtual-poker-6player20/virtual-poker-6player-20.component';
import { VirtualJokerComponent } from './pages/virtual-joker/virtual-joker.component';
import { JokerComponent } from './pages/joker/joker.component';
import { VirtualCardRaceComponent } from './pages/virtual-card-race/virtual-card-race.component';
import { CardRaceComponent } from './pages/card-race/card-race.component';
import { VirtualBlackJackClassicComponent } from './pages/virtual-black-jack-classic/virtual-black-jack-classic.component';
import { RockPaperScissorsComponent } from './pages/rock-paper-scissors/rock-paper-scissors.component';
import { VirtualRockPaperScissorsComponent } from './pages/virtual-rock-paper-scissors/virtual-rock-paper-scissors.component';
import { BlackJackClassicComponent } from './pages/black-jack-classic/black-jack-classic.component';
import { KabutarFuddiComponent } from './pages/kabutar-fuddi/kabutar-fuddi.component';
import { VirtualKabutarFuddiComponent } from './pages/virtual-kabutar-fuddi/virtual-kabutar-fuddi.component';

const routes: Routes = [
  {
    path: 'lobby',
    component: GamesComponent,
    pathMatch: 'full',
  },
  {
    path: 'TP',
    component: TeenpattiComponent,
    pathMatch: 'full',
  },
  {
    path: 'BAC',
    component: BaccaratComponent,
    pathMatch: 'full',
  },
  {
    path: 'DT',
    component: DragonTigerComponent,
    pathMatch: 'full',
  },
  {
    path: 'PK',
    component: PokerComponent,
    pathMatch: 'full',
  },
  {
    path: 'AB',
    component: AndarBaharComponent,
    pathMatch: 'full',
  },
  {
    path: 'AB20',
    component: AndarBahar20Component,
    pathMatch: 'full',
  },
  {
    path: 'WM',
    component: WorliMatkaComponent,
    pathMatch: 'full',
  },
  {
    path: 'ARW',
    component: AkbarRomeoWalterComponent,
    pathMatch: 'full',
  },
  {
    path: 'TP20',
    component: Teenpatti20Component,
    pathMatch: 'full',
  },
  {
    path: 'PK20',
    component: Poker20Component,
    pathMatch: 'full',
  },
  {
    path: 'DT20',
    component: DragonTiger20Component,
    pathMatch: 'full',
  },
  {
    path: 'C32',
    component: Cards32Component,
    pathMatch: 'full',
  },
  {
    path: 'UD7',
    component: SevenUpSevenDownComponent,
    pathMatch: 'full',
  },
  {
    path: 'PK6',
    component: Poker6PlayerComponent,
    pathMatch: 'full',
  },
  {
    path: 'PK620',
    component: Poker6Player20Component,
    pathMatch: 'full',
  },
  {
    path: 'BAC20',
    component: Baccarat20Component,
    pathMatch: 'full',
  },
  {
    path: 'VTP',
    component: VirtualTeenpattiComponent,
    pathMatch: 'full',
  },
  {
    path: 'VDT',
    component: VirtualDragonTigerComponent,
    pathMatch: 'full',
  },
  {
    path: 'VUD7',
    component: VirtualSevenupSevendownComponent,
    pathMatch: 'full',
  }, {
    path: 'VPK',
    component: VirtualPokerComponent,
    pathMatch: 'full',
  },
  {
    path: 'VWM',
    component: VirtualWorliMatkaComponent,
    pathMatch: 'full',
  },
  {
    path: 'VC32',
    component: VirtualCards32Component,
    pathMatch: 'full',
  },
  {
    path: 'VTP20',
    component: VirtualTeenpatti20Component,
    pathMatch: 'full',
  },
  {
    path: 'VBAC',
    component: VirtualBaccaratComponent,
    pathMatch: 'full',
  },
  {
    path: 'VAB20',
    component: VirtualAndarBahar20Component,
    pathMatch: 'full',
  },
  {
    path: 'VARW',
    component: VirtualAkbarRomeoWalterComponent,
    pathMatch: 'full',
  },
  {
    path: 'V3CJ',
    component: Virtual3CardsJudgementComponent,
    pathMatch: 'full',
  },
  {
    path: 'VBAC20',
    component: VirtualBaccarat20Component,
    pathMatch: 'full',
  },
  {
    path: 'VDT20',
    component: VirtualDragonTiger20Component,
    pathMatch: 'full',
  },
  {
    path: 'VPK20',
    component: VirtualPoker20Component,
    pathMatch: 'full',
  },
  {
    path: 'V3TP',
    component: Virtual3playerTeenpattiComponent,
    pathMatch: 'full',
  },
  {
    path: 'VBJ',
    component: VirtualBlackJackComponent,
    pathMatch: 'full',
  },
  {
    path: 'D3CJ',
    component: ThreeCardJudgementComponent,
    pathMatch: 'full',
  },
  {
    path: 'D3TP',
    component: ThreePlayerTeenpattiComponent,
    pathMatch: 'full',
  },
  {
    path: 'BJ',
    component: BlackJackComponent,
    pathMatch: 'full',
  },
  {
    path: 'VPK6',
    component: VirtualPoker6PlayerComponent,
    pathMatch: 'full',
  },
  {
    path: 'VPK620',
    component: VirtualPoker6Player20Component,
    pathMatch: 'full',
  },
  {
    path: 'VJKR',
    component: VirtualJokerComponent,
    pathMatch: 'full',
  },
  {
    path: 'JKR',
    component: JokerComponent,
    pathMatch: 'full',
  },
  {
    path: 'VCR',
    component: VirtualCardRaceComponent,
    pathMatch: 'full',
  },
  {
    path: 'CR',
    component: CardRaceComponent,
    pathMatch: 'full',
  },
  {
    path: 'VBJC',
    component: VirtualBlackJackClassicComponent,
    pathMatch: 'full',
  },
  {
    path: 'RPS',
    component: RockPaperScissorsComponent,
    pathMatch: 'full',
  },
  {
    path: 'VRPS',
    component: VirtualRockPaperScissorsComponent,
    pathMatch: 'full',
  },
  {
    path: 'BJC',
    component: BlackJackClassicComponent,
    pathMatch: 'full',
  },
  {
    path: 'KF',
    component: KabutarFuddiComponent,
    pathMatch: 'full',
  },
  {
    path: 'VKF',
    component: VirtualKabutarFuddiComponent,
    pathMatch: 'full',
  },
  {
    path: 'history',
    component: MarketHistoryComponent,
    pathMatch: 'full',
  },
  {
    path: 'round',
    component: RoundHistoryComponent,
    pathMatch: 'full',
  },
  {
    path: 'coming-soon',
    component: ComingSoonComponent,
    pathMatch: 'full',
  },
  {
    path: 'api',
    component: ApidocumentationComponent,
    pathMatch: 'full',
  },
  {
    path: '',
    component: ErrorComponent,
    pathMatch: 'full',
  },
  {
    path: '**',
    component: ErrorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
