import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-apidocumentation',
  templateUrl: './apidocumentation.component.html',
  styleUrls: ['./apidocumentation.component.scss']
})
export class ApidocumentationComponent implements OnInit {
  expandorCollapseList: boolean[] = [];
  apiData: any[] = [];
  constructor(private tostr: ToastrService) {
  }

  ngOnInit() {
    this.loginApiDataInit();
    this.getBalanceApiDataInit();
    this.fundTransferApiDataInit();
    this.getBetListDataInit();
    this.getCancelBetList();
    this.getSettlementByRoundIdsDataInit();
    this.getAgentGameListDataInit();
    this.commonModelListInit();
  }

  loginApiDataInit() {
    const data: any = {};
    data.displayName = 'Login';
    data.url = location.origin + '/api/User/Login';
    data.request = JSON.stringify({
      agentUserName: 'Agent Username',
      clientUserName: 'Client Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      point: 100,
      isDemoUser: 0,
      isAllowBet: 1,
      isIframe: 0,
      gameCode: 'TP',
      websiteURL: 'redirection_URL',
      partnership: [
        { parentLevel: 1, userName: 'P1', share: 10 },
        { parentLevel: 2, userName: 'P2', share: 40 },
        { parentLevel: 3, userName: 'P4', share: 50 }
      ]
    }, undefined, 1);

    data.successResponse = JSON.stringify(
      {
        Data: {
          agentUsername: 'Agent Username',
          clientUsername: 'Client Username',
          secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
          point: 100,
          token: '74D1A5EE-5016-4809-9154-XXXXXXXXXXXX',
          lobbyURL: location.origin + '/lobby?Token=74D1A5EE-5016-4809-9154-XXXXXXXXXXXX&Code=TP'
        },
        Status: {
          returnMessage: '',
          code: 0
        }
      }, undefined, 1);

    data.failResponse = JSON.stringify({
      Status: {
        returnMessage: 'Invalid Credentials',
        code: 2
      }
    }, undefined, 1);

    this.apiData.push(data);
  }

  getBalanceApiDataInit() {
    const data: any = {};
    data.displayName = 'GetBalance';
    data.header = JSON.stringify({ SecretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX' }, undefined, 1);
    data.request = JSON.stringify({
      agentUsername: 'Agent Username',
      clientUsername: 'Client Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX'
    }, undefined, 1);

    data.successResponse = JSON.stringify({
      AgentUsername: 'Agent Username',
      ClientUsername: 'Client Username',
      SecretKey: null,
      Balance: 50000.0,
      Status: 'Success',
      ErrorMessage: ''
    }, undefined, 1);

    data.failResponse = JSON.stringify({
      AgentUsername: 'Agent Username',
      ClientUsername: 'Client Username',
      SecretKey: null,
      Balance: null,
      Status: 'Failure',
      ErrorMessage: 'Client not exiest'
    }, undefined, 1);

    this.apiData.push(data);
  }

  fundTransferApiDataInit() {
    const data: any = {};
    data.displayName = 'FundTransfer';
    data.header = JSON.stringify({ SecretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX' }, undefined, 1);
    data.requestPlacebet = JSON.stringify({
      agentUsername: 'Agent Username',
      clientUsername: 'Client Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      amount: 2,
      transactionType: 'DR',
      transactionID: '9840',
      roundID: 17968,
      transactionCode: 'bet_place',
      gameCode: 'PK',
      runnername: null,
      rate: null,
      stake: null,
      betType: null,
      reverseTransactionID: null,
      data: null
    }, undefined, 1);

    data.requestPlacebetCancel = JSON.stringify({
      agentUsername: 'Agent Username',
      clientUsername: 'Client Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      amount: 2,
      transactionType: 'DR',
      transactionID: '9840CAN',
      roundID: 17968,
      transactionCode: 'placebet_cancel',
      gameCode: 'PK',
      runnername: null,
      rate: null,
      stake: null,
      betType: null,
      reverseTransactionID: 9840,
      data: null
    }, undefined, 1);

    data.successResponse = JSON.stringify({
      AgentUsername: 'Agent Username',
      ClientUsername: 'Client Username',
      SecretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      Amount: 2.0,
      Balance: 4175723.0,
      TransactionType: 'DR',
      TransactionID: '9840',
      RoundID: '17968',
      TransactionCode: 'bet_place',
      Status: 'Success',
      ErrorMessage: 'Success'
    }, undefined, 1);

    data.failResponse = JSON.stringify({
      AgentUsername: null,
      ClientUsername: null,
      SecretKey: null,
      Amount: null,
      Balance: null,
      TransactionType: null,
      TransactionID: null,
      RoundID: null,
      TransactionCode: null,
      Status: 'Failure',
      ErrorMessage: 'Bet_Allow_False'
    }, undefined, 1);

    this.apiData.push(data);
  }

  getBetListDataInit() {
    const data: any = {};
    data.displayName = 'GetBetList';
    data.url = location.origin + '/api/User/GetBetList';
    data.request = JSON.stringify({
      agentUserName: 'Agent Username',
      clientUserName: 'Client Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      roundSummaryID: 9840
    }, undefined, 1);

    data.successResponse = JSON.stringify({
      RoundData: {
        RoundSummaryID: 9840,
        GameID: 1,
        Cards: '{"PlayerA":"AD AH ","PlayerB":"2D TS "}',
        Rates: '{"PlayerARates":[{"Back":"1.07","Lay":"1.09"}],"PlayerBRates":[{"Back":"15.15","Lay":"15.65"}]}',
        Winner: 'Player A',
        Status: 3,
        IsRoundOver: true,
        TimeRemainSec: 0
      },
      BetList: [
        {
          BetID: 3402,
          RoundSummaryID: 9840,
          ClientUserID: 1046,
          Rate: 1.37,
          Stake: 100,
          IsBack: true,
          Runner: 'PlayerA   ',
          IsBetWon: true,
          Dr: 0.0,
          Cr: 37.00,
          Net: 37.00,
          CreatedDate: '2019-08-09T11:52:10.333',
          PL: 37.00
        }
      ],
      Status: {
        returnMessage: '',
        code: 0
      }
    }, undefined, 1);

    data.failResponse = JSON.stringify({
      Status: {
        returnMessage: 'Invalid Credentials',
        code: 2
      }
    }, undefined, 1);

    this.apiData.push(data);
  }

  getCancelBetList() {
    const data: any = {};
    data.displayName = 'GetCancelBetList';
    data.url = location.origin + '/api/User/GetCancelBetList';
    data.request = JSON.stringify({
      agentUserName: 'Agent Username',
      clientUsername: 'Client Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      roundID: 1,
      transactionID: 0
    }, undefined, 1);

    data.successResponse = JSON.stringify({
      CancelBetList: [
        {
          agentUsername: 'Agent Username',
          clientUsername: 'Client Username',
          secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
          transactionID: '1',
          roundID: '1',
          amount: 75.750000,
          transactionType: 'CR',
          transactionCode: 'settle_game',
          gameCode: 'TP',
          runnername: null,
          rate: null,
          stake: null,
          betType: null,
          reverseTransactionID: null,
          data: null,
          siteCode: null
        }
      ],
      Status: {
        returnMessage: '',
        code: 0
      }
    }, undefined, 1);

    data.failResponse = JSON.stringify({
      Status: {
        returnMessage: 'Invalid Credentials',
        code: 2
      }
    }, undefined, 1);

    this.apiData.push(data);
  }

  getSettlementByRoundIdsDataInit() {
    const data: any = {};
    data.displayName = 'GetSettlementByRoundIds';
    data.url = location.origin + '/api/User/GetSettlementByRoundIds';
    data.request = JSON.stringify({
      agentUserName: 'Agent Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
      roundSummaryIds: '1,2'
    }, undefined, 1);

    data.successResponse = JSON.stringify({
      SettlementData: [
        {
          RoundSummaryId: 1,
          Request: {
            AgentUsername: 'Agent Username',
            SecretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
            TransactionCode: 'settle_game',
            RoundID: '1',
            GameCode: 'TP',
            Status: 1,
            Message: null,
            Data: [{
              ClientUsername: 'Client Username',
              Amount: 75.750000,
              Netpl: 56.250000,
              BetAmt: 19.500000,
              BetCnt: 6,
              TransactionType: 'CR',
              TransactionID: '1',
              Status: 1,
              Message: null
            }]
          }
        },
        {
          RoundSummaryId: 2,
          Request: {
            AgentUsername: 'Agent Username',
            SecretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX',
            TransactionCode: 'settle_game',
            RoundID: '2',
            GameCode: 'BAC',
            Status: 1,
            Message: null,
            Data: [{
              ClientUsername: 'Client Username',
              Amount: 57.750000,
              Netpl: 65.250000,
              BetAmt: 24.502000,
              BetCnt: 9,
              TransactionType: 'CR',
              TransactionID: '1',
              Status: 1,
              Message: null
            }]
          }
        }
      ],
      Status: {
        returnMessage: '',
        code: 0
      }
    }, undefined, 1);

    data.failResponse = JSON.stringify({
      Status: {
        returnMessage: 'Invalid Credentials',
        code: 2
      }
    }, undefined, 1);

    this.apiData.push(data);
  }

  getAgentGameListDataInit() {
    const data: any = {};
    data.displayName = 'GetAgentGameList';
    data.url = location.origin + '/api/User/GetAgentGameList';
    data.request = JSON.stringify({
      agentUserName: 'Agent Username',
      secretKey: 'C1AD25B6-0ABA-4D3F-95BF-XXXXXXXXXXXX'
    }, undefined, 1);

    data.successResponse = JSON.stringify({
      GameList: [
        {
          GameCode: 'TP',
          Name: 'Teen Patti',
          ImagePath: 'http://admin.supernowa.net/gameimages/06555eb62898490bbf964b1f68e1803c_616144cc50bb48d988bc5b5741662461_3P_LIVE.png',
          IsLive: true,
          IsStop: false
        }
      ],
      Status: {
        returnMessage: '',
        code: 0
      }
    }, undefined, 1);

    data.failResponse = JSON.stringify({
      Status: {
        returnMessage: 'Invalid Credentials',
        code: 2
      }
    }, undefined, 1);

    this.apiData.push(data);
  }

  commonModelListInit() {
    const data: any = {};
    data.displayName = 'Common Model';
    data.commonModelList = [];
    // Response Code Model -------- data.commonModelList[0] is used for 'code Model'
    data.commonModelList[0] = {};
    data.commonModelList[0].title = 'Response Code';
    data.commonModelList[0].data = JSON.stringify({
      Success: 0,
      Warning: 1,
      Error: 2,
      Info: 3,
      Invalid: 401
    }, undefined, 1);
    this.apiData.push(data);
    // End Response Code Model
  }

  expandcollapse(i) {
    this.expandorCollapseList[i] = !this.expandorCollapseList[i];
    for (let n = 0; n < this.expandorCollapseList.length; n++) {
      if (i !== n) {
        this.expandorCollapseList[n] = false;
      }
    }
    if (this.expandorCollapseList[i]) {
      setTimeout(() => {
        const str: string = '#expand-card-' + i;
        const element = $(str);
        element.slideDown('slow');
      }, 0);
    }
  }

  copyToClipBoard(element) {
    const temp = $('<textarea />');
    $('body').append(temp);
    temp.val($(element).text()).select();
    document.execCommand('copy');
    temp.remove();
    this.tostr.success('Text Copied successfully');
  }
}
