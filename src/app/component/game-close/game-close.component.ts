import { Component, Input, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-game-close',
  templateUrl: './game-close.component.html',
  styleUrls: ['./game-close.component.scss']
})
export class GameCloseComponent implements OnInit {
  _ShowGameStatusOverlay = false;
  get ShowGameStatusOverlay(): boolean { return this._ShowGameStatusOverlay; }
  @Input('ShowGameStatusOverlay') set ShowGameStatusOverlay(value: boolean) { this._ShowGameStatusOverlay = value; }

  _GameWinner = '';
  get GameWinner(): string { return this._GameWinner; }
  @Input('GameWinner') set GameWinner(value: string) { this._GameWinner = value; }

  _GameStatus = 0;
  get GameStatus(): number { return this._GameStatus; }
  @Input('GameStatus') set GameStatus(value: number) { this._GameStatus = value; }

  constructor(public constant: ConstantService) { }
  ngOnInit(): void { }
}
