import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
})
export class ErrorComponent {
  @Input() errorMessage;
  // @Input() navigateLink;

  constructor() { }

  get lobbyLink() {
    return location.origin + '/lobby?Token=' + localStorage.AuthToken;
  }
}
