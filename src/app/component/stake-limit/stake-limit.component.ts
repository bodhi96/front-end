import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stake-limit',
  templateUrl: './stake-limit.component.html',
  styleUrls: ['./stake-limit.component.scss']
})
export class StakeLimitComponent implements OnInit {

  @Input() GameDetails;
  constructor() { }
  ngOnInit(): void {
  }

  MinStake(GameDetails){    
    return Math.ceil(GameDetails?.MinStakePerBet);
  }
  MaxStake(GameDetails){
    if(GameDetails.MaxStakePerBet==-1){
      return 'No limit';
    }else{
      return  Math.trunc(GameDetails.MaxStakePerBet);
    }
  }
  MaxProfit(GameDetails){
    if(GameDetails.MaxProfit==-1){
      return 'No limit';
    }else{
      return  Math.trunc(GameDetails.MaxProfit);
    }
  }
}
