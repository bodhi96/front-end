import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-bet-slip',
  templateUrl: './bet-slip.component.html',
  styleUrls: ['./bet-slip.component.scss']
})
export class BetSlipComponent implements OnInit {
  @Input() ShowBetSlip = false;
  @Output() ShowBetSlipChange = new EventEmitter<any>();

  @Input() GameBetDetails: any = { RoundSummaryID: 0, Rate: 0, Stake: 0, IsBack: 0, Runner: '', };;
  @Output() GameBetDetailsChange = new EventEmitter<any>();

  @Input() GameCode = '';
  @Input() PlayerPL = 0;
  @Input() ShowBetDelayOverlay = false;
  @Input() GameStatus = 0;
  @Input() GameID = 0;
  @Input() RoundSummaryID = 0;

  @Input() isThreeCard = false;
  @Input() selectCardList = { firstCard: '', secCard: '', thirdCard: '' };
  @Output() selectCardListChange = new EventEmitter<any>();
  @Input() selectedBetType = '';
  @Output() selectedBetTypeChange = new EventEmitter<any>();
  @Input() PlayerNum = 0;
  @Output() PlayerNumChange = new EventEmitter<any>();

  @Output() onCalculatePL: EventEmitter<any> = new EventEmitter<any>();
  @Output() onValidateBet: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public constant: ConstantService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void { }

  PlaceEnterBet(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      this.ValidateBet();
    }
    if (event.keyCode === 46) {
      event.preventDefault();
    }
  }

  AddStake(stake) {
    if (this.GameBetDetails.Rate && this.GameBetDetails.Rate !== '-' && this.GameStatus === 1) {
      this.GameBetDetails.Stake += +stake;
      this.GameBetDetailsChange.emit(this.GameBetDetails);
      this.CalculatePL(1);
    } else {
      this.toastr.error('You can not bet right now.');
    }
  }

  gameKeyBoardClick(key) {
    if (this.GameBetDetails.Rate && this.GameBetDetails.Rate !== '-' && this.GameStatus === 1) {
      if (!this.GameBetDetails.Stake) {
        this.GameBetDetails.Stake = 0;
        this.GameBetDetailsChange.emit(this.GameBetDetails);
      }
      if (this.GameBetDetails.Stake.toString().length < 6 || key === '<') {
        if (key === '<') {
          this.GameBetDetails.Stake = Number(this.GameBetDetails.Stake.toString().slice(0, -1));
          this.GameBetDetailsChange.emit(this.GameBetDetails);
          this.CalculatePL(1);
        } else {
          key = (this.GameBetDetails.Stake.toString().length === 5 && key === '00' ? '0' : key);
          this.GameBetDetails.Stake = Number(this.GameBetDetails.Stake.toString() + key);
          this.GameBetDetailsChange.emit(this.GameBetDetails);
        }
        this.CalculatePL(1);
      }

    } else {
      this.toastr.error('You can not bet right now.');
    }
  }

  CalculatePL(isCheck: number, CashOutBet: boolean = false) {
    this.onCalculatePL.emit({ isCheck, CashOutBet });
  }

  ValidateBet() {
    this.onValidateBet.emit();
  }

  ClearBet() {
    this.GameBetDetails = {
      GameID: this.GameID,
      RoundSummaryID: this.RoundSummaryID,
      Rate: 0,
      Stake: 0,
      IsBack: 0,
      Runner: '',
    };
    if (this.isThreeCard) {
      this.selectCardList = { firstCard: '', secCard: '', thirdCard: '' };
      this.selectCardListChange.emit(this.selectCardList);
      this.selectedBetType = null;
      this.selectedBetTypeChange.emit(this.selectedBetType);
      this.PlayerNum = null;
      this.PlayerNumChange.emit(this.PlayerNum);
    }

    this.ShowBetSlipChange.emit(false);
    this.GameBetDetailsChange.emit(this.GameBetDetails);
    this.CalculatePL(0);
    $('.betslip').removeClass('open');
    $('.overlay').css('display', 'none');
  }

}
