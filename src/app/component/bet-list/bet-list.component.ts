import { Component, Input } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-bet-list',
  templateUrl: './bet-list.component.html',
})
export class BetListComponent {

  @Input() isModal: boolean;
  @Input() gameCode: string;
  @Input() displayId: string;
  @Input() show: boolean;
  @Input() betList: any;

  constructor(public constant: ConstantService) { }
  runner(Runner: string) {
    return Runner.replace(/([A-Z])/g, ' $1').trim();
  }
  getNumber(gameCode: string, item: any) {
    if (gameCode == 'WM' || gameCode == 'VWM') {
      if (item.Number == 0 && item.Runner.trim() == 'TP') {
        return '( All )';
      } else {
        return item.Number;
      }
    } else if (gameCode == 'V3CJ' || gameCode == 'D3CJ') {
      return this.cardNumberToString(item);
    } else if (gameCode == 'VJKR' || gameCode == 'JKR') {
      if (item.Number == '14') { return '(JK)'; }
      else if (item.Number == '1') { return '(A)'; }
      else if (item.Number == '11') { return '(J)'; }
      else if (item.Number == '12') { return '(Q)'; }
      else if (item.Number == '13') { return '(K)'; }
      else if (item.Number == '0') { return ''; }
      else { return '('+item.Number+')'; }
    } else {
      return null;
    }

  }
  cardNumberToString(item) {
    let number = String(item.Number);
    number = number.substring(1);
    let splitArray = [];
    if (number) {
      splitArray = number.toString().match(/.{1,2}/g);
      if (splitArray.length == 3) {

        for (var i = 0; i < splitArray.length; i++) {
          // var a = splitArray[i];
          if (splitArray[i] === "01") {
            splitArray[i] = splitArray[i].replace(/01/g, 'A');
          } else if (splitArray[i] === "11") {
            splitArray[i] = splitArray[i].replace(/11/g, 'J');
          } else if (splitArray[i] === "12") {
            splitArray[i] = splitArray[i].replace(/12/g, 'Q');
          } else if (splitArray[i] === "13") {
            splitArray[i] = splitArray[i].replace(/13/g, 'K');
          } else if (splitArray[i] !== "10") {
            splitArray[i] = splitArray[i].replace(/0/g, '');
          }
        }
        return '( ' + splitArray[0] + ' ' + splitArray[1] + ' ' + splitArray[2] + ' )';
      } else {
        return '';
      }
    } else {
      return '';
    }

    // if (item.Number == 1) {
    //   return '(A)';
    // } else if (item.Number == 11) {
    //   return '(J)';
    // } else if (item.Number == 12) {
    //   return '(Q)';
    // } else if (item.Number == 13) {
    //   return '(K)';
    // } else {
    //   return '(' + item.Number + ')';
    // }
  }
}
