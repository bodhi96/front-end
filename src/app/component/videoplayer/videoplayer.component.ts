import { Component, Input } from '@angular/core';

declare var NanoPlayer;
declare var VideoCore_WS;
@Component({
  selector: 'app-videoplayer',
  templateUrl: './videoplayer.component.html',
  styleUrls: ['./videoplayer.component.scss']
})
export class VideoplayerComponent {
  @Input() GameCode;
  private _url: any = '';
  @Input('url') set url(value: string) {
    if (value) {
      this._url = value;
      if(this.GameCode == 'TP' || this.GameCode == 'AB'){
        this.tvVideoLoad();
      }else{
        this.videoLoad();
      }
    }
  }
  @Input('PlayerDes') set PlayerDes(value: string) {
    if (value) {
      if(this.GameCode == 'TP' || this.GameCode == 'AB'){
        this.tvPlayerDestroy();
      }else{
        this.playerDestroy();
      }
    }
  }
  get url(): string {
    return this._url;
  }
  Player: any;
  constructor() {
    document.addEventListener('visibilitychange', (event) => {
      if (!document.hidden) {
        if(this.GameCode != 'TP' && this.GameCode != 'AB'){
          this.Player.play();
        }
      }
    });
  }
  tvVideoLoad(){
    const Source = this.getParameterFromStringURL('Source', this._url);
    const config = {
      VideoEl: document.querySelector("#vplayer"),
      Source: Source
    }
    this.Player = new VideoCore_WS.VideoPlayer();
    this.PlayerSetupTvBet(config);
  }
  videoLoad() {
    this.Player = new NanoPlayer('playerDiv');
    const rtmpurl = this.getParameterFromStringURL('bintu.rtmpurl', this._url);
    const streamnamehd = this.getParameterFromStringURL('bintu.streamnamehd', this._url);
    const streamnamehigh = this.getParameterFromStringURL('bintu.streamnamehigh', this._url);
    const streamnamemedium = this.getParameterFromStringURL('bintu.streamnamemedium', this._url);
    const streamnamelow = this.getParameterFromStringURL('bintu.streamnamelow', this._url);

    const apiurl = 'rtmp://bintu-play.nanocosmos.de/play'; // this.getParameterFromStringURL('bintu.apiurl', this._url);
    // const streamid = this.getParameterFromStringURL('bintu.streamid', this._url);
    const streamname = 'AZyne-BB1nA';// this.getParameterFromStringURL('bintu.streamname', this._url);
    const label = 'stream 1';// this.getParameterFromStringURL('bintu.label', this._url);
    const config = {
      source: {
        entries: [
          {
            index: 0,
            label,
            tag: '',
            info: {
              bitrate: 0,
              width: 0,
              height: 0,
              framerate: 0
            },
            hls: '',
            h5live: {
              rtmp: {
                url: rtmpurl,
                streamname: streamnamehigh
              },
              server: {
                websocket: 'wss://bintu-play.nanocosmos.de:443/h5live/stream.mp4',
                hls: 'https://bintu-play.nanocosmos.de:443/h5live/http/playlist.m3u8',
                progressive: 'https://bintu-play.nanocosmos.de:443/h5live/http/stream.mp4'
              },
              token: '',
              security: {}
            },
            bintu: {}
          }
        ],
        options: {
          adaption: {
            rule: 'deviationOfMean2'
          },
          switch: {}
        },
        startIndex: 1
      },
      playback: {
        autoplay: true,
        automute: true,
        muted: true,
        flashplayer: '//demo.nanocosmos.de/nanoplayer/nano.player.swf'
      },
      style: {
        displayMutedAutoplay: false
      },
      metrics: {
        accountId: 'nanocosmos1',
        accountKey: 'nc1wj472649fkjah',
        userId: 'nanoplayer-demo',
        eventId: 'nanocosmos-demo',
        statsInterval: 10,
        customField1: 'demo',
        customField2: 'public',
        customField3: 'online resource'
      }
    };
    this.PlayerSetup(config);
  }

  PlayerSetup(config) {
    this.Player.setup(config).then(() => {
      console.log('setup success');
    }, (error) => {
      console.log(error);
    });
  }
  // flashplayer: '../../assets/js/nanocosmoplayer/nano.player.swf'
  getParameterFromStringURL(name, url) {
    const newURL = new URLSearchParams(url);
    const param = newURL.get(name);
    return param;
  }
  public playerDestroy() {
    const config = {
      source: {
        entries: [
          {
            index: 0,
            label: 'stream 1',
            tag: '',
            info: {
              bitrate: 0,
              width: 0,
              height: 0,
              framerate: 0
            },
            hls: '',
            h5live: {
              rtmp: {
                url: '',
                streamname: ''
              },
              server: {
                websocket: 'wss://bintu-play.nanocosmos.de:443/h5live/stream.mp4',
                hls: 'https://bintu-play.nanocosmos.de:443/h5live/http/playlist.m3u8',
                progressive: 'https://bintu-play.nanocosmos.de:443/h5live/http/stream.mp4'
              },
              token: '',
              security: {}
            },
            bintu: {}
          }
        ],
        options: {
          adaption: {
            rule: 'deviationOfMean2'
          },
          switch: {}
        },
        startIndex: 1
      },
      playback: {
        autoplay: true,
        automute: true,
        muted: true,
        flashplayer: '//demo.nanocosmos.de/nanoplayer/nano.player.swf'
      },
      style: {
        displayMutedAutoplay: true
      },
      metrics: {
        accountId: 'nanocosmos1',
        accountKey: 'nc1wj472649fkjah',
        userId: 'nanoplayer-demo',
        eventId: 'nanocosmos-demo',
        statsInterval: 10,
        customField1: 'demo',
        customField2: 'public',
        customField3: 'online resource'
      }
    };
    this.PlayerSetup(config);
  }
  PlayerSetupTvBet(config){
    this.Player.Initialize(config);
  }
  tvPlayerDestroy(){
    this.Player.Initialize('');
  }
}