import { Component, Input } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';
import { CasinoService } from 'src/app/services/casino.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-chips-setting',
  templateUrl: './chips-setting.component.html',
})
export class ChipsSettingComponent {

  IsCallMade = false;

  @Input() minStack: any;
  @Input() maxStack: any;

  constructor(public constant: ConstantService, private casinoService: CasinoService, private toastr: ToastrService) { }

  CheckChipsSetting() {
    for (const element of this.constant.chipsSetting) {
      if (element.name === '' || element.value === '') {
        this.toastr.error('Enter Proper Name or Value.');
        return;
      }
      if (!this.constant.CheckNumberFormat(element.value)) {
        this.toastr.error('Enter Proper Chip Value.');
        return;
      }
      if (this.minStack !== -1 && element.value < this.minStack) {
        this.toastr.error('Enter Chip Value above Min Value ' + this.minStack);
        return;
      }
      if (this.maxStack !== -1 && element.value > this.maxStack) {
        this.toastr.error('Enter Chip Value below Max Value ' + this.maxStack);
        return;
      }
      const name = this.constant.chipsSetting.filter((e) => e.name === element.name);
      const value = this.constant.chipsSetting.filter((e) => Number(e.value) === Number(element.value));
      if (name.length > 1 || value.length > 1) {
        this.toastr.error('Enter Unique Name and Value.');
        return;
      }
    }
    this.AddUpdateChipsSetting();
  }

  private async AddUpdateChipsSetting() {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      const chipsSetting = JSON.stringify(this.constant.chipsSetting);
      await this.casinoService.AddUpdateChipsSetting({ ChipsSetting: chipsSetting }).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === 0) {
          this.constant.GameChips = this.constant.chipsSetting;
          this.toastr.success('Chips setting updated successfully');
          $('#ChipsSettingPopup').modal('hide');
        } else {
          this.toastr.error(data.Status.returnMessage);
        }
      }, err => {
        this.IsCallMade = false;
        this.toastr.error('Something went wrong. Please try again.');
      });
    }
  }
}
