import { Component, Input, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {
  _GameDetails: any;
  get GameDetails(): any { return this._GameDetails; }
  @Input('GameDetails') set GameDetails(value: any) { this._GameDetails = value; }

  _GameCode: any;
  get GameCode(): any { return this._GameCode; }
  @Input('GameCode') set GameCode(value: any) { this._GameCode = value; }

  _DisplayID: any;
  get DisplayID(): any { return this._DisplayID; }
  @Input('DisplayID') set DisplayID(value: any) { this._DisplayID = value; }

  _isLay = false;
  get isLay(): boolean { return this._isLay; }
  @Input('isLay') set isLay(value: boolean) { this._isLay = value; }

  @Input() backLayName = { back: 'BACK', lay: 'LAY' }

  constructor(public constant: ConstantService) { }
  ngOnInit(): void { }

  MinStake(GameDetails) {
    return Math.ceil(GameDetails?.MinStakePerBet);
  }

  MaxStake(GameDetails) {
    if (GameDetails.MaxStakePerBet == -1) {
      return 'No limit';
    } else {
      return Math.trunc(GameDetails.MaxStakePerBet);
    }
  }

  MaxProfit(GameDetails) {
    if (GameDetails.MaxProfit == -1) {
      return 'No limit';
    } else {
      return Math.trunc(GameDetails.MaxProfit);
    }
  }
}
