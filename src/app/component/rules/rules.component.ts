import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
})
export class RulesComponent {

  @Input() code: any;
  @Input() rules: any;

  constructor() {
  }
}
