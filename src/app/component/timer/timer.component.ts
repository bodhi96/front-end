import { Component, Input } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';
declare var $: any;

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent {

  second = 0;

  @Input() time: any;

  @Input() set seconds(val) {
    this.second = val || 0;
    this.refreshTimer();
  }

  constructor(public constant: ConstantService) { }

  refreshTimer() {
    const initialOffset = 141;
    if (this.second > 0) {
      $('.countdown-animation').css('stroke-dashoffset', initialOffset - ((this.second - 1) * (initialOffset / this.time)));
    }
    if (this.second <= 0) {
      $('.countdown-animation').css('stroke-dashoffset', initialOffset);
      $('.countdown-animation').removeClass('ending');
      if (this.constant.IsTabActive && this.constant.IsSound) {
        this.constant.noBetAudio.play();
      }
    } else if (this.second <= 6) {
      $('.countdown-animation').addClass('ending');
      if (this.constant.IsTabActive && this.constant.IsSound) {
        this.constant.beepAudio.play();
      }
    }
  }
}
