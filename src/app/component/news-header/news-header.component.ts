import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { CasinoService } from 'src/app/services/casino.service';
import { ConstantService } from 'src/app/services/constant.service';
import { GamesService } from 'src/app/services/games.service';
import { Location } from '@angular/common';
import { GameList } from 'src/app/models/common.model';
declare var $: any;
@Component({
  selector: 'app-news-header',
  templateUrl: './news-header.component.html',
  styleUrls: ['./news-header.component.scss']
})
export class NewsHeaderComponent implements OnInit {

  @Input() News;
  @Input() GameCode;
  @Input() balance = 0;
  @Input() liability = 0;
  @Input() userName;
  GameList: any = [];

  @Input() gameId;
  @Input() IsOneClick;
  CurrentGame: GameList = {
    BetCount: null,
    Description: null,
    GameCode: null,
    GameID: null,
    ImagePath: null,
    IsIframe: null,
    IsLive: null,
    Name: null,
    Rules: null
  };
  Code: any;
  Token = '';
  IsCallMade = false;
  @Output() PlayerDes = new EventEmitter();
  SelectedGame: any = {};

  slideConfig = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 8,
    slidesToScroll: 1,
    draggable: true,
    waitForAnimate: false,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 361,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ],
    method: {}
  };
  @ViewChild('slickAModal', { static: true }) slickAModal: SlickCarouselComponent;

  constructor(
    private router: Router,
    public constant: ConstantService,
    private gamesService: GamesService,
    private casinoService: CasinoService,
    private location: Location
  ) {
    // this.GameList = this.constant.GameList;
  }

  ngOnInit() {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    this.Code = url.searchParams.get('Code') || '';
    if (Token !== '') {
      this.Token = Token;
      localStorage.setItem('AuthToken', this.Token);
      this.GetUserWiseGames(0);
    }
    $('#RulesPopup').modal('hide');
  }

  // GameSlider() {
  // if (this.GameList.length) {
  //   const slideNo = Number(this.GameList.length);
  //   // if (window.innerWidth <= 480 && window.innerWidth >= 320) {
  //   //   $('.carousel-a').slick('slickGoTo', slideNo + 2);
  //   // } else if (window.innerWidth <= 321) {
  //   //   $('.carousel-a').slick('slickGoTo', slideNo + 3);
  //   // } else {
  //   // }
  //   // $('.carousel-a').slick('slickGoTo', slideNo + 6);
  // }
  // }

  // private async GetUserWiseGames() {
  //   await this.gamesService.GetUserWiseGames().subscribe(data => {
  //     if (data.Status.code === this.constant.HttpResponceCode.Success) {
  //       if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
  //         this.GameList = data.UserGames;
  //       }
  //     }
  //   });
  // }

  async GetMarketHistory() {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    localStorage.setItem('SelectedGame', this.gameId.toString());
    window.open(location.origin + '/history?Token=' + Token, '_blank');
  }

  async OpenCurrentGameRule() {
    if (this.CurrentGame.Rules) {
      $('#RulesPopup').modal('show');
    } else {
      await this.gamesService.GetUserWiseGames({ GameID: this.CurrentGame.GameID }).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.LoginLink) {
            localStorage.setItem('LoginLink', data.LoginLink);
          }
          if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
            this.CurrentGame = data.UserGames[0];
            const gameIndex = this.constant.GameList.findIndex((e) => e.GameID === this.CurrentGame.GameID);
            let newGameList = [...this.constant.GameList];
            newGameList[gameIndex] = { ...newGameList[gameIndex], Rules: this.CurrentGame.Rules };
            this.GameList = newGameList;
            this.constant.GameList = newGameList;
            $('#RulesPopup').modal('show');
          }
        }
      });
    }
  }
  async Logout() {
    this.PlayerDes.emit(true);
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    await this.casinoService.GameLogOut().subscribe(data => {
      if (data.Status.code === this.constant.HttpResponceCode.Success) {
        localStorage.removeItem('Token');
        localStorage.removeItem('GameID');
        this.router.navigateByUrl('/lobby?Token=' + Token);
      } else {
        this.router.navigateByUrl('/lobby?Token=' + Token);
      }
    }, err => {
      this.router.navigateByUrl('/lobby?Token=' + Token);
    });
  }
  async OpenGameRule(item, index) {
    this.SelectedGame = item;
    if (this.SelectedGame.IsLive) {
      if (item.BetCount > 0) {
        this.NavigatoToGame();
      } else {
        if (this.SelectedGame.Rules) {
          $('#AgreeRulesPopup').modal('show');
        } else {
          await this.gamesService.GetUserWiseGames({ GameID: item.GameID }).subscribe(data => {
            this.IsCallMade = false;
            if (data.Status.code === this.constant.HttpResponceCode.Success) {
              if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
                this.SelectedGame = data.UserGames[0];
                const gameIndex = this.constant.GameList.findIndex((e) => e.GameID === this.SelectedGame.GameID);
                const newGameList = [...this.constant.GameList];
                newGameList[gameIndex] = { ...newGameList[gameIndex], Rules: this.SelectedGame.Rules };
                this.GameList = newGameList;
                this.constant.GameList = newGameList;
                $('#AgreeRulesPopup').modal('show');
                this.setCurrentSlider(index);
              }
            }
          });
        }
        // $('#AgreeRulesPopup').modal('show');
      }
    } else {
      this.PlayerDes.emit(true);
      this.router.navigate(['/coming-soon']);
    }
  }
  async NavigatoToGame() {
    this.PlayerDes.emit(true);
    if (this.SelectedGame.GameID && this.SelectedGame.GameCode) {
      localStorage.setItem(this.SelectedGame.GameCode.trim(), this.SelectedGame.GameID);
      this.location.go(location.pathname.replace(this.GameCode, this.SelectedGame.GameCode.trim()) + location.search);
      if (!this.IsCallMade) {
        this.IsCallMade = true;
        await this.gamesService.GetRefreshToken().subscribe(data => {
          this.IsCallMade = false;
          if (data.Status.code === this.constant.HttpResponceCode.Success) {
            this.router.navigateByUrl('/' + this.SelectedGame.GameCode.trim() + '?Token=' + this.Token);
          }
        }, err => {
          this.IsCallMade = false;
        });
      }
    }

    this.constant.beepAudio.load();
    this.constant.noBetAudio.load();
    this.constant.betPlaceAudio.load();
  }

  private async GetUserWiseGames(GameID) {
    if (!this.IsCallMade) {
      this.IsCallMade = true;
      await this.gamesService.GetUserWiseGames({ GameID }).subscribe(data => {
        this.IsCallMade = false;
        if (data.UserGames) {
          this.GameList = data.UserGames;
        } else {
          this.GameList = this.constant.GameList;
        }
        this.CurrentGame = this.GameList.find((e) => e.GameCode.trim() === this.GameCode.trim());
        const index = this.GameList.findIndex(game => game.GameCode.trim() === this.GameCode.trim());
        this.setCurrentSlider(index);
      });
    }
  }

  private setCurrentSlider(index) {
    // const totalGame = this.GameList.length;
    // if (window.innerWidth > 1200) {
    //   if (index + 7 > totalGame) {
    //     this.slideConfig.initialSlide = index - (8 - (totalGame - index));
    //   } else {
    //     this.slideConfig.initialSlide = index;
    //   }
    // } else if (window.innerWidth > 1024) {
    //   if (index + 5 > totalGame) {
    //     this.slideConfig.initialSlide = index - (6 - (totalGame - index));
    //   } else {
    //     this.slideConfig.initialSlide = index;
    //   }
    // } else if (window.innerWidth > 600) {
    //   if (index + 4 > totalGame) {
    //     this.slideConfig.initialSlide = index - (5 - (totalGame - index));
    //   } else {
    //     this.slideConfig.initialSlide = index;
    //   }
    // } else if (window.innerWidth > 480) {
    //   if (index + 3 > totalGame) {
    //     this.slideConfig.initialSlide = index - (4 - (totalGame - index));
    //   } else {
    //     this.slideConfig.initialSlide = index;
    //   }
    // } else {
    //   if (index + 1 > totalGame) {
    //     this.slideConfig.initialSlide = index - (2 - (totalGame - index));
    //   } else {
    //     this.slideConfig.initialSlide = index;
    //   }
    // }
    if (index !== 0) {
      this.slideConfig.initialSlide = index - 1;
    } else {
      this.slideConfig.initialSlide = index;
    }
    // setTimeout(() => {
    // console.log(index);
    // $('.carousel-a').slick('slickGoTo', index);
    // }, 1000);
  }
}


