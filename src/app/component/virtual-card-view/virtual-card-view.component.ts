import { Component, Input, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-virtual-card-view',
  templateUrl: './virtual-card-view.component.html',
  styleUrls: ['./virtual-card-view.component.scss']
})
export class VirtualCardViewComponent implements OnInit {

  @Input() GameCard: any;
  @Input() GameCode: any;
  @Input() GameID: any;
  constructor(public constant: ConstantService) { }

  ngOnInit(): void {
  }
  cardPostion(card) {
    if (card) {
      if (card.charAt(0) == 'A' || card.charAt(0) <= 6) {
        return 'float-left';
      } else if (card.charAt(0) == 'J' || card.charAt(0) == 'Q' || card.charAt(0) == 'K') {
        return 'float-right';
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

}
