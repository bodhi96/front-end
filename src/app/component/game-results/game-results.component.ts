import { Component, Input, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-game-results',
  templateUrl: './game-results.component.html',
  styleUrls: ['./game-results.component.scss']
})
export class GameResultsComponent implements OnInit {
  @Input() GameHistoryList: any;
  @Input() GameHistoryDetails: any;
  @Input() GameDetails: any;
  @Input() GameCode: string;

  constructor(
    public constant: ConstantService,
  ) { }

  ngOnInit(): void { }

  SetHistoryCards(item) {
    $('.historycard').removeClass('flip');
    if (this.GameCode === 'TP' || this.GameCode === 'TP20' || this.GameCode === 'VTP' || this.GameCode === 'VTP20') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''], PlayerBCard: ['', '', ''] };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerBCard, cards.PlayerB.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner ? item.Winner.trim() : '';
          this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'AB' || this.GameCode === 'AB20' || this.GameCode === 'VAB' || this.GameCode === 'VAB20') {
      this.GameHistoryDetails = {
        DisplayID: '', Winner: '', PlayerACard: [''], PlayerBCard: [''], PlayerTCard: [''],
        WinningHand: { Hand: '', JokerSuite: '', WinnerSuite: '' }
      };
      // if(item.WinningHand){
      //   const winHand = JSON.parse(item.WinningHand)
      //   this.GameHistoryDetails.WinningHand = { Hand: winHand.Hand, JokerSuite: winHand.JokerSuite, WinnerSuite: winHand.WinnerSuite };
      // }
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerBCard, cards.PlayerB.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerTCard, cards.PlayerT.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner.trim();
          // this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'DT' || this.GameCode === 'DT20' || this.GameCode === 'VDT' || this.GameCode === 'VDT20') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: [''], PlayerBCard: [''], WinningHand: { Dragon: '', DragonSuite: '', TigerSuite: '', Tiger: '' } };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          if (cards) {
            Object.assign(this.GameHistoryDetails.PlayerACard, cards.Dragon.trim().split(' '));
            Object.assign(this.GameHistoryDetails.PlayerBCard, cards.Tiger.trim().split(' '));
            this.GameHistoryDetails.Winner = item.Winner.trim();
            this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
            const winningHand = JSON.parse(item.WinningHand);
            if (winningHand) {
              this.GameHistoryDetails.WinningHand.Dragon = winningHand.Dragon;
              this.GameHistoryDetails.WinningHand.DragonSuite = winningHand.DragonSuite;
              this.GameHistoryDetails.WinningHand.TigerSuite = winningHand.TigerSuite;
              this.GameHistoryDetails.WinningHand.Tiger = winningHand.Tiger;
            } else {
              this.GameHistoryDetails.WinningHand = { Dragon: '', DragonSuite: '', TigerSuite: '', Tiger: '' }
            }
          }
        }, 100);
      }
    } else if (this.GameCode === 'WM' || this.GameCode === 'VWM') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''] };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner ? item.Winner.trim() : '';
          this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'ARW' || this.GameCode === 'VARW') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: [''], WinningHand: { Hand: '', Suite: '', Seven: '' } };
      if (item.WinningHand) {
        const winHand = JSON.parse(item.WinningHand)
        this.GameHistoryDetails.WinningHand = { Hand: winHand.Hand, Suite: winHand.Suite, Seven: winHand.Seven };
      }
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          if (cards) {
            Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
            this.GameHistoryDetails.Winner = item.Winner.trim();
            this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
          }
        }, 100);
      }
    } else if (this.GameCode === 'UD7' || this.GameCode === 'VUD7') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: [''], WinningHand: '' };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          if (cards) {
            Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
            this.GameHistoryDetails.Winner = item.Winner.trim();
            this.GameHistoryDetails.WinningHand = (item.WinningHand) ? item.WinningHand : '';
            this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
          }
        }, 100);
      }
    } else if (this.GameCode === 'C32' || this.GameCode === 'VC32') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', Card: {}, WinningHand: '' };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          cards.Player8.Cards = cards.Player8.Cards.trim().split(' ');
          cards.Player9.Cards = cards.Player9.Cards.trim().split(' ');
          cards.Player10.Cards = cards.Player10.Cards.trim().split(' ');
          cards.Player11.Cards = cards.Player11.Cards.trim().split(' ');
          Object.assign(this.GameHistoryDetails.Card, cards);
          this.GameHistoryDetails.Winner = item.Winner.trim();
          this.GameHistoryDetails.WinningHand = item.WinningHand.trim();
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'PK' || this.GameCode === 'PK20' || this.GameCode === 'VPK' || this.GameCode === 'VPK20') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', TableCard: ['', '', '', '', ''], PlayerACard: ['', ''], PlayerBCard: ['', ''] };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.TableCard, cards.Table.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerBCard, cards.PlayerB.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner.trim();
          this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'BAC' || this.GameCode === 'BAC20' || this.GameCode === 'VBAC' || this.GameCode === 'VBAC20') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''], PlayerBCard: ['', '', ''], PlayerAScore: 0, PlayerBScore: 0, WinningHand: '' };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.Player.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerBCard, cards.Banker.trim().split(' '));
          this.GameHistoryDetails.PlayerAScore = (cards.PlayerScore) ? Number(cards.PlayerScore) : 0;
          this.GameHistoryDetails.PlayerBScore = (cards.BankerScore) ? Number(cards.BankerScore) : 0;
          this.GameHistoryDetails.Winner = item.Winner.trim();
          this.GameHistoryDetails.WinningHand = item.WinningHand.trim();
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'V3CJ' || this.GameCode === 'D3CJ') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''] };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
          // item.Winner ? item.Winner.trim() : '';
          this.GameHistoryDetails.Winner = this.gameResult(item.Winner);
          this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'V3TP' || this.GameCode === 'D3TP') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''], PlayerBCard: ['', '', ''], PlayerCCard: ['', '', ''] };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerBCard, cards.PlayerB.trim().split(' '));
          Object.assign(this.GameHistoryDetails.PlayerCCard, cards.PlayerC.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner ? item.Winner.trim() : '';
          this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'VJKR' || this.GameCode === 'JKR') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', Cards: [''], WinningHand: { Number: '', Color: '', CardSuit: '' } };
      if (item.WinningHand) {
        const winHand = JSON.parse(item.WinningHand)
        this.GameHistoryDetails.WinningHand = { Number: winHand.Number, Color: winHand.Color, CardSuit: winHand.CardSuit };
      }
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          if (cards) {
            Object.assign(this.GameHistoryDetails.Cards, cards.Card.trim().split(' '));
            this.GameHistoryDetails.Winner = item.Winner.trim();
            this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
          }
        }, 100);
      }
    } else if (this.GameCode === 'VBJ' || this.GameCode === 'BJ') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', PlayerACard: ['', '', ''], PlayerBCard: ['', '', ''], PlayerCCard: ['', '', ''], PlayerDCard: ['', '', ''], PlayerECard: ['', '', ''], PlayerAScore: 0, PlayerBScore: 0, PlayerCScore: 0, PlayerDScore: 0, PlayerEScore: 0, WinningHand: '' };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          if (cards) {
            Object.assign(this.GameHistoryDetails.PlayerACard, cards.PlayerA.Cards.trim().split(' '));
            Object.assign(this.GameHistoryDetails.PlayerBCard, cards.PlayerB.Cards.trim().split(' '));
            Object.assign(this.GameHistoryDetails.PlayerCCard, cards.PlayerC.Cards.trim().split(' '));
            Object.assign(this.GameHistoryDetails.PlayerDCard, cards.PlayerD.Cards.trim().split(' '));
            Object.assign(this.GameHistoryDetails.PlayerECard, cards.Dealer.Cards.trim().split(' '));
            this.GameHistoryDetails.PlayerAScore = (cards.PlayerA.Score) ? Number(cards.PlayerA.Score) : 0;
            this.GameHistoryDetails.PlayerBScore = (cards.PlayerB.Score) ? Number(cards.PlayerB.Score) : 0;
            this.GameHistoryDetails.PlayerCScore = (cards.PlayerC.Score) ? Number(cards.PlayerC.Score) : 0;
            this.GameHistoryDetails.PlayerDScore = (cards.PlayerD.Score) ? Number(cards.PlayerD.Score) : 0;
            this.GameHistoryDetails.PlayerEScore = (cards.Dealer.Score) ? Number(cards.Dealer.Score) : 0;
            this.GameHistoryDetails.Winner = item.Winner.trim();
            // this.GameHistoryDetails.WinningHand = item.WinningHand.trim();
            this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
          }
        }, 100);
      }
    } else if (this.GameCode === 'VPK6' || this.GameCode === 'VPK620' || this.GameCode === 'PK620' || this.GameCode === 'PK6') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', TableCard: ['', '', '', '', ''], Player1Card: ['', ''], Player2Card: ['', ''], Player3Card: ['', ''], Player4Card: ['', ''], Player5Card: ['', ''], Player6Card: ['', ''] };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.TableCard, cards.Table.trim().split(' '));
          Object.assign(this.GameHistoryDetails.Player1Card, cards.Player1.trim().split(' '));
          Object.assign(this.GameHistoryDetails.Player2Card, cards.Player2.trim().split(' '));
          Object.assign(this.GameHistoryDetails.Player3Card, cards.Player3.trim().split(' '));
          Object.assign(this.GameHistoryDetails.Player4Card, cards.Player4.trim().split(' '));
          Object.assign(this.GameHistoryDetails.Player5Card, cards.Player5.trim().split(' '));
          Object.assign(this.GameHistoryDetails.Player6Card, cards.Player6.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner.trim();
          this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    } else if (this.GameCode === 'VCR' || this.GameCode === 'CR') {
      this.GameHistoryDetails = { DisplayID: '', Winner: '', SpadeCard: ['', '', '', '', '', ''], ClubCard: ['', '', '', '', '', ''], DiamondCard: ['', '', '', '', '', ''], HeartCard: ['', '', '', '', '', ''], WinningHand: { Hand: '', Color: '' } };
      if (item.Cards) {
        setTimeout(() => {
          const cards = JSON.parse(item.Cards);
          Object.assign(this.GameHistoryDetails.SpadeCard, cards.Spade.trim().split(' '));
          Object.assign(this.GameHistoryDetails.ClubCard, cards.Club.trim().split(' '));
          Object.assign(this.GameHistoryDetails.DiamondCard, cards.Diamond.trim().split(' '));
          Object.assign(this.GameHistoryDetails.HeartCard, cards.Heart.trim().split(' '));
          this.GameHistoryDetails.Winner = item.Winner ? item.Winner.trim() : '';
          // this.GameHistoryDetails.WinningHand = item.WinningHand ? item.WinningHand.trim() : '';
          if (item.WinningHand) {
            const winHand = JSON.parse(item.WinningHand)
            this.GameHistoryDetails.WinningHand = { Hand: winHand.Hand, Color: winHand.Color };
          }
          this.GameHistoryDetails.DisplayID = this.GameDetails.GameCode.trim() + '-' + item.RoundSummaryID;
        }, 100);
      }
    }
  }

  // V3CJ OR D3CJ
  gameResult(value) {
    const gameResult = [' ', ' ', ' ', ' ', ' '];
    // const gameResult = JSON.parse(value);
    Object.assign(gameResult, value.trim().split(' '));
    const fistCard = this.setResult(gameResult[1]);
    const secCard = this.setResult(gameResult[2]);
    const thirdCard = this.setResult(gameResult[3]);
    return fistCard + ' ' + secCard + ' ' + thirdCard;
  }
  setResult(value) {
    if (value > 10) {
      if (Number(value) === 11) { return 'J'; }
      else if (Number(value) === 12) { return 'Q'; }
      else if (Number(value) === 13) { return 'K'; }
      else { return null; }
    } else if (Number(value) === 1) {
      return 'A';
    } else {
      return value;
    }
  }
  jokerWinnerName(winner, num) {
    if (winner === '14' && num === 0) { return 'JK'; }
    else if (winner === '14' && num === 1) { return 'JOKER'; }
    else if (winner === '1') { return 'A'; }
    else if (winner === '11') { return 'J'; }
    else if (winner === '12') { return 'Q'; }
    else if (winner === '13') { return 'K'; }
    else { return winner; }
  }
  PK6WinnerNum(winner: string) {
    return winner.replace('Player ', '')[0];
  }
}
