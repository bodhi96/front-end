import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-one-click',
  templateUrl: './one-click.component.html',
  styleUrls: ['./one-click.component.scss']
})
export class OneClickComponent implements OnInit {

  @Input() isHeader: boolean;
  @Input() IsOneClick : boolean;
  @Output() oneClick = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  oneClickChange(){
    this.IsOneClick = !this.IsOneClick;
    this.oneClick.emit(this.IsOneClick);
  }
}
