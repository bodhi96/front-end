import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';
import { Router } from '@angular/router';
import { CasinoService } from 'src/app/services/casino.service';
import { GamesService } from 'src/app/services/games.service';
import { Location } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  @Input() GameCode;
  @Input() logoPath;
  @Input() userName;
  @Input() balance;
  @Input() gameId;
  @Input() IsOneClick;
  @Output() oneClick = new EventEmitter();
  @Output() PlayerDes = new EventEmitter();
  @Input() LoginLink;
  GameList: any = [];
  Code: any;
  Rules = null;
  CurrentGame = null;
  SelectedGame: any = {};
  IsCallMade = false;
  Token = '';
  constructor(
    private router: Router, public constant: ConstantService,
    private casinoService: CasinoService, private gamesService: GamesService,
    private location: Location
  ) { }

  ngOnInit() {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    this.Code = url.searchParams.get('Code') || '';
    if (Token !== '') {
      this.Token = Token;
      localStorage.setItem('AuthToken', this.Token);
      this.GameList = this.constant.GameList;
      this.CurrentGame = this.GameList.find((e) => e.GameCode.trim() === this.GameCode.trim());
    }
    $('#RulesPopup').modal('hide');
  }
  oneClickChange() {
    this.IsOneClick = !this.IsOneClick;
    this.oneClick.emit(this.IsOneClick);
  }
  async GetMarketHistory() {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    localStorage.setItem('SelectedGame', this.gameId.toString());
    window.open(location.origin + '/history?Token=' + Token, '_blank');
  }
  async GetRoundHistory() {
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    localStorage.setItem('SelectedGame', this.gameId.toString());
    window.open(location.origin + '/round?Token=' + Token, '_blank');
  }
  async Logout() {
    this.PlayerDes.emit(true);
    const url = new URL(window.location.href);
    const Token = url.searchParams.get('Token');
    await this.casinoService.GameLogOut().subscribe(data => {
      if (data.Status.code === this.constant.HttpResponceCode.Success) {
        localStorage.removeItem('Token');
        localStorage.removeItem('GameID');
        this.router.navigateByUrl('/lobby?Token=' + Token);
      } else {
        this.router.navigateByUrl('/lobby?Token=' + Token);
      }
    }, err => {
      this.router.navigateByUrl('/lobby?Token=' + Token);
    });
  }
  private async GetUserWiseGames() {
    await this.gamesService.GetUserWiseGames().subscribe(data => {
      if (data.Status.code === this.constant.HttpResponceCode.Success) {
        if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
          this.GameList = data.UserGames;
        }
      }
    });
  }

  async OpenCurrentGameRule() {
    if (this.CurrentGame.Rules) {
      $('#RulesPopup').modal('show');
    } else {
      await this.gamesService.GetUserWiseGames({ GameID: this.CurrentGame.GameID }).subscribe(data => {
        this.IsCallMade = false;
        if (data.Status.code === this.constant.HttpResponceCode.Success) {
          if (data.LoginLink) {
            localStorage.setItem('LoginLink', data.LoginLink);
          }
          if (data.UserName && data.Balance !== undefined && data.Balance != null && data.UserGames) {
            this.CurrentGame = data.UserGames[0];
            const gameIndex = this.constant.GameList.findIndex((e) => e.GameID === this.CurrentGame.GameID);
            let newGameList = [...this.constant.GameList];
            newGameList[gameIndex] = { ...newGameList[gameIndex], Rules: this.CurrentGame.Rules };
            this.GameList = newGameList;
            this.constant.GameList = newGameList;
            $('#RulesPopup').modal('show');
          }
        }
      });
    }
  }

  OpenGameRule(item) {
    this.SelectedGame = item;
    if (this.SelectedGame.IsLive) {
      if (item.BetCount > 0) {
        this.NavigatoToGame();
      } else {
        $('#AgreeRulesPopup').modal('show');
      }
    } else {
      this.router.navigate(['/coming-soon']);
    }
  }

  async NavigatoToGame() {
    if (this.SelectedGame.GameID && this.SelectedGame.GameCode) {
      localStorage.setItem(this.SelectedGame.GameCode.trim(), this.SelectedGame.GameID);
      this.location.go(location.pathname.replace(this.GameCode, this.SelectedGame.GameCode.trim()) + location.search);
      if (!this.IsCallMade) {
        this.IsCallMade = true;
        await this.gamesService.GetRefreshToken().subscribe(data => {
          this.IsCallMade = false;
          if (data.Status.code === this.constant.HttpResponceCode.Success) {
            this.router.navigateByUrl('/' + this.SelectedGame.GameCode.trim() + '?Token=' + this.Token);
          }
        }, err => {
          this.IsCallMade = false;
        });
      }
    }

    this.constant.beepAudio.load();
    this.constant.noBetAudio.load();
    this.constant.betPlaceAudio.load();
  }

  backToWebsite() {
    window.location.href = this.LoginLink;
  }
  LiveGame(GameCode) {
    const gameName = this.constant.GameName[GameCode.trim()];
    if (gameName.search(/Virtual/i) < 0) {
      return true;
    } else {
      return false;
    }
  }
}

