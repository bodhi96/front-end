import { Component, HostListener } from '@angular/core';
import { SpinnerService } from './services/spinner.service';
import { ConstantService } from './services/constant.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  HTTPActivity = false;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const scrollHeight = event.target.document.getElementsByTagName('body')[0].scrollHeight;
    if (window.parent && window.parent.postMessage) {
      window.parent.postMessage({ height: scrollHeight }, '*');
    }
  }
  constructor(
    public constant: ConstantService,
    private router: Router,
    private spinnerService: SpinnerService) {
    let oldHeight = 0;
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        const interval = setInterval(() => {
          const scrollHeight = document.getElementsByTagName('body')[0].scrollHeight;
          if (oldHeight !== scrollHeight && window.parent && window.parent.postMessage) {
            oldHeight = scrollHeight;
            window.parent.postMessage({ height: scrollHeight }, '*');
          }
        }, 250);
        setTimeout(() => {
          clearInterval(interval);
        }, 20000);
      }
    });
    this.spinnerService.change.subscribe((status: boolean) => {
      setTimeout(() => {
        this.HTTPActivity = status;
      }, 0);
    });
    const gameList = JSON.parse(localStorage.getItem('GameList'));
    const lastRefreshed = localStorage.getItem('lastRefreshed');
    let isLatestData = false;
    if (lastRefreshed) {
      const timeDiff = new Date().getTime() - new Date(lastRefreshed).getTime();
      const timeDiffMin = timeDiff / (1000 * 60);
      isLatestData = (timeDiffMin <= 60) ? true : false;
    }
    if (isLatestData && gameList) {
      this.constant.GameList = gameList;
    }
  }
}
